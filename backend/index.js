const mariadb = require('mariadb');
const pool = mariadb.createPool({
     host: '127.0.0.1', 
     port:'3306',
     user:'dannybombastic', 
     password: '606197854',
     connectionLimit: 2
});


async function asyncFunction() {

  let conn;
  try {
	conn = await pool.getConnection();
	const rows = await conn.query("SELECT * FROM insurance.policy_user; ");
	console.log(rows[0]); //[ {val: 1}, meta: ... ]

  } catch (err) {
	throw err;
  } finally {
	if (conn) return conn.end();
  }
}

asyncFunction();
