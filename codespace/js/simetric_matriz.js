

var matriz = [[1, 7, 7, 7],
              [7, 2, 7, 7],
              [7, 7, 3, 7],
              [7, 7, 7, 4]];


console.log("array", matriz[0].length);


function isSimetric(arr) {
 
    const col = arr.length;
    var simetric = true;

    for (let i = 0; i < arr.length; i++) {
        if (arr[i].length != col) {

            simetric = false;
        }

    }

    if (simetric) {

        for (let i = 0; i < arr.length; i++) {
            for (let j = 0; j < arr[i].length; j++) {
                if (i != j) {
                    if (arr[i][j] != arr[j][i]) {
                        simetric = false;

                    }
                }
            }

        }

        if (simetric) {
            return simetric;
        } else {

            return simetric;
        }
    } else {
        return simetric;

    }


}

if (isSimetric(matriz)) {

    console.log("ES SIMETRICA");

} else {
    console.log("NO ES SIMETRICA");

}
