#!/usr/bin/env node

class Edificio {
    /*
       Forma parte de un coche y cada vez que instanciamos el coche se heredan sus atributos
       
     */

    constructor(plantas) {
        this.plantas = plantas;
        if (this.constructor === Edificio) {
            throw new TypeError('Abstract class "Edificio" cannot be instantiated directly.');
        }
    }


}


class Chasis {
    /*
       Forma parte de un coche y cada vez que instanciamos el coche se heredan sus atributos
       
     */

    constructor(material) {
        if (this.constructor === Chasis) {
            throw new TypeError('Abstract class "Chasis" cannot be instantiated directly.');
        }

        this.material_chasis = material;
    }
}

class Coche extends Chasis { // herencia de chasis
    /*
       Clase que crea un parkin que extiende de la clase edificio a traves del super le pasamos el valos a edificio.
       
     */

    constructor(material, marca) {
        super(material);
        this.marca = marca;


    }
}

// Esta parte la hereda de la clase anterios : ) por eso se usa la palabra reservada extends
class Parkin extends Edificio {
    /*
      Clase que crea un parkin que extiende de la clase edificio a traves del super le pasamos el valos a edificio.
      
    */
    
    
    constructor(plantas, columnas) {
        super(plantas)
        this.columnas = columnas
        this.numero_plazas = 0;

        this.distribucion = new Array(plantas);
        for (let i = 0; i < this.distribucion.length; i++) {

            this.distribucion[i] = new Array(columnas);

        }
        this.vaciar_parking();  //  vaciamos antes 
        
    }

    aparcar_coche(coche) {

        /*
          Funcion que  busca una plaza libre donde aparcar un coche
        */

        for (let j = 0; j < this.distribucion.length; j++) {
            for (let x = 0; x < this.distribucion.length; x++) {

                if (this.distribucion[j][x] == 'vacio') {
                    console.log(` coche en Planta ${j} Columna ${x} aapracao  cuñaoooo, marca : ${coche.marca}, chasis ${coche.material_chasis}`);
                    this.distribucion[j][x] = coche;
                    this.numero_plazas = this.numero_plazas -1;
                    return true; // si lo aparcamos acabamos la tarea
                  

                } else {

                    console.log(`Planta ${j} Columna ${x}  esta plaza esta llena cuñaaaaoo`);

                }


            }

        }
    }

    busy_places() {

        for (let j = 0; j < this.distribucion.length; j++) {
            for (let x = 0; x < this.distribucion.length; x++) {

                if (this.distribucion[j][x] !== 'vacio') {
                    console.log(`Plazas ocupadas Planta ${j} Columna ${x}  esta plaza esta llena cuñaaaaoo por este coche ${this.distribucion[j][x].marca}`);
                }
            }
        }

    }



    vaciar_parking() {
        for (let j = 0; j < this.distribucion.length; j++) {
            for (let x = 0; x < this.distribucion.length; x++) {


                this.distribucion[j][x] = 'vacio';
                this.numero_plazas = this.numero_plazas + 1

            }

        }
    }

   get_plazas(){
    return this.numero_plazas;
   };
}
exports.Coche = Coche
exports.Parkin = new Parkin(4, 4);

// var coche_m = new Coche('aluminio', 'Maserati v11 +1 :)');
// var coche_f = new Coche('carbono', 'Ferrari v12 :)');
// var coche_p= new Coche('plastilina', 'Porche GT3s :)');
// var parking = new Parkin(4, 4);

// // parking.get_n_plazas_libres().then((numero_plazas) => {
// //     console.log(`numerode plazas antes ${numero_plazas} antes de abrir`);
// // });
 
// const plazas_before = parking.get_plazas();
// console.log(`numerode plazas antes ${plazas_before} antes de abrir`);
// parking.aparcar_coche(coche_f);
// parking.aparcar_coche(coche_m);
// parking.aparcar_coche(coche_p);
// parking.busy_places();
// const plazas_after = parking.get_plazas(); 
// console.log(`numerode plazas despues ${plazas_after} antes de abrir`);



