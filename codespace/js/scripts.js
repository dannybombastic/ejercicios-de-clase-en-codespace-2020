#!/usr/bin/env node

// main context here
// 1. Determinar cual de los dos elementos de texto es mayor
// 2. Utilizando exclusivamente los dos valores booleanos del array, determinar los operadores necesarios
// para obtener un resultado true y otro resultado false

// 3. Determinar el resultado de las cinco operaciones matemáticas realizadas con los dos elementos numéricos
const valores = [true, 5, false, "hola", "adios", 2];
var maxi = 0;
var palabra = '';

console.log(`Longitud  ${valores.length}`);

for (let i = 0; i <= valores.length - 1; i++) {

    if (typeof valores[i] == 'string') {

        if (valores[i].length > maxi) {
            maxi = valores[i].length;
            palabra = valores[i];

        }

    }


}


console.log(`La cadena mas larga es : ${palabra}`);
// task end

function empieza() {
    /*
          funciion que empieza e iterar los datos en pantalla y representa los numeros primos
    */

    var lista_numeros = new Array();
    var cnt = 0;
    var numero = document.getElementById('data-number').value;
    var J = numero;

    console.log(`Empezando ${numero}`);

    for (let i = 0; numero > 1; i++) {

        if (numero % J == 0) {

            cnt++;

        }
        if (J == 0) {


            if (cnt > 2) {

                console.log(`El numero no es primo: ${numero} , `, cnt);
                lista_numeros.push({ primo: false, numero: numero });
                cnt = 0;


            } else {


                console.log(`El numero  es primo : ${numero} , `, cnt);
                lista_numeros.push({ primo: true, numero: numero });
                cnt = 0;
            }
            numero = numero - 1;
            J = numero;
        } else {

            J = J - 1
        }

    }

    addTable('listado-numeros', lista_numeros); // creamos la tabla con la lista de enumeros

}


function addTable(nombre, numeros) {
    /*
          funciion que crea la tabla de una sola columna recibe el nombre del elemento y la lista de numeros
    */
    var myTableDiv = document.getElementById(nombre);
    var table = document.createElement('TABLE');
    table.border = '1';
    table.align = 'center';
    table.style.marginTop = '12px';

    var tableBody = document.createElement('TBODY');
    table.appendChild(tableBody);
    var N = numeros.length;

    for (var i = 0; i <= N - 1; i++) {

        var tr = document.createElement('TR');
        tableBody.appendChild(tr);
        var td = document.createElement('TD');
        td.width = '100';

        if (numeros[i].primo) {

            td.style.backgroundColor = 'yellow';
            td.appendChild(document.createTextNode("Numero: " + numeros[i].numero));
            tr.appendChild(td);

        } else {

            td.style.backgroundColor = 'red';
            td.appendChild(document.createTextNode("Numero :" + numeros[i].numero));
            tr.appendChild(td);
        }


        console.log(' cnt lista', i);

    }
    myTableDiv.appendChild(table);
}


function addTableUlam(nombre, numeros) {
    /*
      funciion que crea la tabla  de una sola columna recibe el nombre del elemento y la lista de numeros difiere de la anterior
    */
    var myTableDiv = document.getElementById(nombre);
    var table = document.createElement('TABLE');
    table.border = '1';
    table.align = 'center';
    table.style.marginTop = '12px';

    var tableBody = document.createElement('TBODY');
    table.appendChild(tableBody);
    var N = numeros.length;

    for (var i = 0; i <= N - 1; i++) {

        var tr = document.createElement('TR');
        tableBody.appendChild(tr);
        var td = document.createElement('TD');
        td.width = '100';

        if ((numeros[i] % 2) == 0) {

            td.style.backgroundColor = 'yellow';
            td.appendChild(document.createTextNode("Numero: " + numeros[i]));
            tr.appendChild(td);

        } else {

            td.style.backgroundColor = 'red';
            td.appendChild(document.createTextNode("Numero :" + numeros[i]));
            tr.appendChild(td);
        }


        console.log(' cnt lista', i);

    }
    myTableDiv.appendChild(table);
}



function ulam() {
    /*
    Calcula la conjetura de ULAM
    */
    numeros = [];

    var num = document.getElementById('data-ulam').value;
    if (num > 0) {

        while (num != 1) {
            if (calculaResto(num) == 0) {

                console.log('par', num);
                numeros.push(num);
                num = num / 2;


            } else {

                console.log('impar', num);
                numeros.push(num);
                num = (num * 3) + 1;


            }

        }
        numeros.push(num);
        console.log('impar', num);


    }
    addTableUlam('listado-ulam', numeros);

}





function calculaResto(num) {
    /*
       Calcula el resto de cualquier numero
    */

    return num % 2;
}




function getAllUrlParams(url) {

    /*
       parseando urlencode from the url
    */

    // get query string from url (optional) or window
    var queryString = url ? url.split('?')[1] : window.location.search.slice(1);

    // we'll store the parameters here
    var obj = {};

    // if query string exists
    if (queryString) {

        // stuff after # is not part of query string, so get rid of it
        queryString = queryString.split('#')[0];

        // split our query string into its component parts
        var arr = queryString.split('&');

        for (var i = 0; i < arr.length; i++) {
            // separate the keys and the values
            var a = arr[i].split('=');

            // set parameter name and value (use 'true' if empty)
            var paramName = a[0];
            var paramValue = typeof (a[1]) === 'undefined' ? true : a[1];

            // (optional) keep case consistent
            paramName = paramName.toLowerCase();
            if (typeof paramValue === 'string') paramValue = paramValue.toLowerCase();

            // if the paramName ends with square brackets, e.g. colors[] or colors[2]
            if (paramName.match(/\[(\d+)?\]$/)) {

                // create key if it doesn't exist
                var key = paramName.replace(/\[(\d+)?\]/, '');
                if (!obj[key]) obj[key] = [];

                // if it's an indexed array e.g. colors[2]
                if (paramName.match(/\[\d+\]$/)) {
                    // get the index value and add the entry at the appropriate position
                    var index = /\[(\d+)\]/.exec(paramName)[1];
                    obj[key][index] = paramValue;
                } else {
                    // otherwise add the value to the end of the array
                    obj[key].push(paramValue);
                }
            } else {
                // we're dealing with a string
                if (!obj[paramName]) {
                    // if it doesn't exist, create property
                    obj[paramName] = paramValue;
                } else if (obj[paramName] && typeof obj[paramName] === 'string') {
                    // if property does exist and it's a string, convert it to an array
                    obj[paramName] = [obj[paramName]];
                    obj[paramName].push(paramValue);
                } else {
                    // otherwise add the property
                    obj[paramName].push(paramValue);
                }
            }
        }
    }

    return obj;
}

//   const parametros = getAllUrlParams(window.location.href);
//   if( 'undefined' !== parametros){
//   console.log(`get parameters ${parametros.variable_name.split('%20').join(" ") }`);
//   }




function agregar_dos_pes() {
    /*
       esta funcion creara dos elementos P con algo de texto dentro
    */
    const container = document.getElementById('dos-pes');
    for (let i = 0; i <= 1; i++) {
        let elemento_creado = document.createElement('P');
        elemento_creado.innerHTML = `hola soy el texto ${i}`;
        container.appendChild(elemento_creado);

    }
}


function tres_fotos() {
    var lista_imagenes = document.getElementById('tres-fotos');
    const lista = lista_imagenes.firstChild;
    console.log('elementos', lista.children[0].firstChild);
    const imagenes = [
        './static/img/memes_dios.jpg',
        './static/img/cliente_se_da_cuenta.jpg',
        './static/img/press_f1.jpg'
    ];

    for (let i = 0; i < lista.children.length; i++) {

        lista.children[i].firstChild.addEventListener('click', function () {
            const imagen = document.createElement('DIV');
            imagen.style.border = 'none';
            imagen.style.backgroundImage = `url(${imagenes[i]})`;
            imagen.style.backgroundSize = '100% 100%';
            imagen.style.backgroundRepeat = 'no-repeat';
            imagen.style.backgroundPosition = 'center'; /* porcentaje | unidad de medida | left | centewr | rigth |  titne dos parametros*/
            imagen.style.height = '150px';
            imagen.style.width = '150px';
            this.insertBefore(imagen, this.nextsSibling);  // nextSiblin porxima caja  al padre le pasamos lo que queremos insertar y el elemento padre
            imagen.addEventListener('focusout', function () {
                console.log("abandonado");
                this.style.visibility = 'none';
            });
        });

    };
}
tres_fotos();




class Edificio {
    /*
       Forma parte de un coche y cada vez que instanciamos el coche se heredan sus atributos
       
     */

    constructor(plantas) {
        this.plantas = plantas;
        if (this.constructor === Edificio) {
            throw new TypeError('Abstract class "Edificio" cannot be instantiated directly.');
        }
    }


}


class Chasis {
    /*
       Forma parte de un coche y cada vez que instanciamos el coche se heredan sus atributos
       
     */

    constructor(material) {
        if (this.constructor === Chasis) {
            throw new TypeError('Abstract class "Chasis" cannot be instantiated directly.');
        }

        this.material_chasis = material;
    }
}

class Coche extends Chasis { // herencia de chasis
    /*
       Clase que crea un parkin que extiende de la clase edificio a traves del super le pasamos el valos a edificio.
       
     */

    constructor(material, marca) {
        super(material);
        this.marca = marca;


    }
}

// Esta parte la hereda de la clase anterios : ) por eso se usa la palabra reservada extends
class Parkin extends Edificio {
    /*
      Clase que crea un parkin que extiende de la clase edificio a traves del super le pasamos el valos a edificio.
      
    */

    constructor(plantas, columnas) {
        super(plantas)
        this.columnas = columnas
        this.numero_plazas = 0;

        this.distribucion = new Array(plantas);
        for (let i = 0; i < this.distribucion.length; i++) {

            this.distribucion[i] = new Array(columnas);

        }
    }

    aparcar_coche(coche) {

        /*
          Funcion que  busca una plaza libre donde aparcar un coche
        */

        for (let j = 0; j < this.distribucion.length; j++) {
            for (let x = 0; x < this.distribucion.length; x++) {

                if (this.distribucion[j][x] == 'vacio') {
                    console.log(` coche en Planta ${j} Columna ${x}  aaapracao  cuñaoooo`);
                    this.distribucion[j][x] = coche;
                    return true; // si lo aparcamos acabamos la tarea

                } else {

                    console.log(`Planta ${j} Columna ${x}  esta plaza esta llena cuñaaaaoo`);

                }


            }

        }
    }

    busy_places() {

        for (let j = 0; j < this.distribucion.length; j++) {
            for (let x = 0; x < this.distribucion.length; x++) {

                if (this.distribucion[j][x] !== 'vacio') {
                    console.log(`Plazas ocupadas Planta ${j} Columna ${x}  esta plaza esta llena cuñaaaaooS`);
                }
            }
        }

    }



    vaciar_parking() {
        for (let j = 0; j < this.distribucion.length; j++) {
            for (let x = 0; x < this.distribucion.length; x++) {


                this.distribucion[j][x] = 'vacio';

            }

        }
    }

    get_n_plazas_libres() {

        for (let j = 0; j < this.distribucion.length; j++) {
            for (let x = 0; x < this.distribucion.length; x++) {

                if (this.distribucion[j][x] == 'vacio') {
                    this.numero_plazas = this.numero_plazas + 1
                } else {
                    this.numero_plazas = this.numero_plazas - 1
                }
            }
        }

        return new Promise((resolve, reject) => {
            resolve(this.numero_plazas);

        });
    }
}



var coche = new Coche('aluminio', 'Maserati v11 :)');
var parking = new Parkin(4, 4);

get_n_plazas_libres().then((numero_plazas) => {
    console.log(`numerode plazas ${numero_plazas} antes de abrir`);
});
parking.vaciar_parking();
parking.aparcar_coche(coche);
parking.busy_places();
get_n_plazas_libres().then((numero_plazas) => {
    console.log(`numerode plazas ${numero_plazas} antes de abrir`);
});;




var numeros = [];

for (let i = 2000; i > 3400; i++) {

    numeros.push(i);
}

const N = 2700;
var initial_plus = numeros.length / 2;
var initial_mines = numeros.length / 2;
var initial = numeros.length / 2;
var init = true;

while (init) {

    if (N < numeros[initial]) {
        initial_plus = initial_plus / 2;
     
    }else{
        console.log('numero encontrado <', initial_plus );
        for (let i = 0; i < initial_plus * 2; i++) {
              if(N == numeros[i]){
                  console.log('numero encontrado', numeros[i]);
                  init = false;
              }
        }


    }

    if (N > numeros[initial_mines]) {
        initial_mines = initial_mines + ( initial_mines / 2 );


    }else{
        console.log('numero encontrado >', initial_mines );
        for (let i = initial_mines; i < initial_mines * 2; i++) {
            if(N == numeros[i]){
                console.log('numero encontrado', numeros[i]);
                init = false;
            }
      }

    }


}

