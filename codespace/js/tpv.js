var ul_content = document.getElementById("main-operations");
var ul = document.getElementsByTagName("UL")[0];
var calculated = true;
var total_popup = 0;
var subtotal_popup = 0;
var iva_popup = 0;

ul.style = "list-style: none";


/**
 * 
 * evento que calcula el total y pone a la escucha al ultimo hijo de la lista y borra el anterior;
 * 
 */


const evento = function (callback) {
   
        if (calculated) {
            
                bild_total();
                myFunction();
                genera_fila(1);
    
        } else {
                genera_fila(1);
                calculated = !calculated;
        }
   
    console.log("index DESPUES", ul.children.length - 2);
};

const hidePop = function hidePopup() {
    if(document.getElementById("myPopup").className.includes("show")){
        document.getElementById("myPopup").classList.remove("show");
        console.log("texto off", document.getElementById("myPopup").className.includes("show"));
    }
    console.log("texto off");
};


genera_fila(4);
last_child_listener();
/**
 * 
 *   funcion que pone a la escucha al ultimo elemento;
 * 
 */
function last_child_listener() {
    
        document.getElementsByClassName("total")[ul.children.length - 1].addEventListener("focus", evento);
        document.getElementsByClassName("total")[Number(ul.children.length - 2)].removeEventListener("focus", evento);
        document.getElementsByClassName("cantidad")[ul.children.length - 1].addEventListener("focus", hidePop);
        document.getElementsByClassName("cantidad")[Number(ul.children.length - 2)].removeEventListener("focus", hidePop);
}
/**
 * funcion calcular  funcion que muestra el resultado;
 * 
 */

function bild_total() {
    var totalElement = get_list_item("total");
    var subTotalElement = get_list_item("subtotal");
    var ivaElement = get_list_item("iva");
    ivaElement.innerHTML = 0;
    totalElement.innerHTML = 0;
    subTotalElement.innerHTML = 0;

    for (let i = 0; i < ul.children.length; i++) {

        let iva = ul.children[i].getElementsByTagName("SELECT")[0].value;
        let total_articulo = ul.children[i].getElementsByTagName("INPUT")[0];

        console.log("supuesto elemento ", (iva / 1000) * total_articulo.value);
        ivaElement.innerHTML = Number(ivaElement.innerHTML) + Number(((iva / 100) * total_articulo.value));
        subTotalElement.innerHTML = parseFloat(Number(subTotalElement.innerHTML) + Number(total_articulo.value));
        totalElement.innerHTML = parseFloat(Number(subTotalElement.innerHTML) + Number(ivaElement.innerHTML));
    }
    total_popup = totalElement.innerHTML;
    subtotal_popup = subTotalElement.innerHTML;
    iva_popup = ivaElement.innerHTML;
}
/**
 * 
 * funcion por que me aburre porner document.....
 * 
 */
function get_list_item(id) {
    return document.getElementById(id);
}
/***
 * 
 * funcion que genera las cuatro primeras filas, la estructura es la de un ul y sus
 * elemnetos, en cada li estan todos los elemntos internos el input cantidad, el iva,
 * y el input total;
 * 
 */

function element_list(num) {

    var li = document.createElement("LI");
    var columna_total = document.createElement("INPUT");
    var option_10 = document.createElement("OPTION");
    var option_21 = document.createElement("OPTION");
    var select = document.createElement("SELECT");
    var columna_cantidad = document.createElement("INPUT");

    columna_cantidad.type = "number";
    columna_cantidad.placeholder = "Introduzca un articulo";
    columna_cantidad.className = "cantidad";
    columna_total.type = "number";
    columna_total.className = "total";

    columna_total.placeholder = "TOTAL";
    columna_total.id = num;
    columna_total.style = "display: inline-block; margin-left: 15px; padding: 15px"

    /**
     * 
     * con este evento doy el resto del calculo del iva sobre la cantidad
     * 
     */
    const hover = (event) => {

        let iva = ul.children[columna_total.id].getElementsByTagName("SELECT")[0].value;
        let cantidad = ul.children[columna_total.id].getElementsByTagName("INPUT");

        if (cantidad[0].value > 0) {
            columna_total.value = Number(cantidad[0].value) + parseFloat((cantidad[0].value * iva) / 100);

            console.log("iva", parseFloat(cantidad[0].value * iva) / 100);
            console.log(columna_total);
        }
        
   
        
    };

    columna_total.addEventListener("mouseover", hover);

    /***
     * 
     * inicializo las opciones y atributos necesarios
    */
    option_10.value = 21;
    option_10.innerText = 21;

    select.appendChild(option_10);
    option_21.value = 10;
    option_21.innerText = 10;
    option_21.selected = false;
    option_10.selected = true;
    select.appendChild(option_21);
    select.style = "display: inline-block; margin-left: 15px;";
    select.className = "iva";
    columna_cantidad.id = `${num}-id`;
    columna_cantidad.className = "cantidad"
    columna_cantidad.style = "display: inline-block; margin-left: 15px; padding: 15px";

    var fila = document.createElement("DIV");
    fila.style = "margin-top:15px;";
    fila.className = "col-12";
    fila.appendChild(columna_cantidad);
    fila.appendChild(select);
    fila.appendChild(columna_total);
    li.appendChild(fila);
    ul.appendChild(li);

}


/**
 * 
 * con esta funcion genero las filas que se necesiten;
 * 
 */

function genera_fila(num) {

    var i = ul.children.length;
    num = num + ul.children.length
    for (; i < num; i++) {
        element_list(`${i}`);
    }
   
   last_child_listener();

}



function myFunction() {

    var popup = document.getElementById("myPopup");
    document.getElementsByClassName("popuptotal")[0].innerHTML =  total_popup;
    document.getElementsByClassName("popupsubtotal")[0].innerHTML = subtotal_popup;
    document.getElementsByClassName("popupiva")[0].innerHTML = iva_popup;
    document.getElementById("nombre").innerHTML = document.getElementById("nombre_usuario").value;
    popup.classList.toggle("show");
    return popup;
}


