
document.addEventListener("DOMContentLoaded", function () {
    // Handler when the DOM is fully loaded

    const boton = document.getElementById("enviar");
    boton.addEventListener("click", (event) => {

        let frase = document.getElementById("frase");
        if (frase.value == 0) {
            let empty = document.createElement("P");
            empty.innerText = "Introduca algun texto"
            empty.className = "error"
            document.getElementById("ejercicio-1").appendChild(empty);

        } else {
            let p = document.createElement("P");
            p.innerText = frase.value;
            document.getElementById("ejercicio-1").appendChild(p);
            frase.value = '';

        }


    });


    /**
     * Ejercicio 2 de js, se nos pide que conrtrolemos cuando un numero es introducido
     * y sea mayor de 10, mediante un evento vamos a leer lo que el usuario pulsa .
     */
    const numero_field = document.getElementById("numero");
    var cnt_number = 0;
    numero_field.addEventListener("keyup", (event) => {
        console.log(event.key);
        cnt_number++;
        if (event.key !== "Backspace") {
            if (!Number.isInteger(Number(numero_field.value)) & cnt_number > 1) {
                let empty = document.createElement("P");
                empty.innerText = "Introduca solo numeros";
                empty.className = "error";
                document.getElementById("ejercicio-2").appendChild(empty);
                numero_field.value = '';
                cnt_number = 0;

            } else {

                if (numero_field.value < 10) {
                    let empty = document.createElement("P");
                    empty.innerText = "Introduca numero superior a 10"
                    empty.className = "error"
                    document.getElementById("ejercicio-2").appendChild(empty);

                } else {

                    let empty = document.createElement("P");
                    empty.innerText = "numero superior a 10 ;) ";
                    document.getElementById("ejercicio-2").appendChild(empty);

                }

            }

        }






    });

    /**
    * Ejercicio 3 detectar cunado la frase tiene mas de 10 caracteres.
    */
    const mas_field = document.getElementById("mas-diez");
    var cnt = 0;

    mas_field.addEventListener("keyup", (event) => {

        cnt++;

        if (cnt > 10) {
            let empty = document.createElement("P");
            empty.innerText = "Tiene mas de 10 caracteres";
            document.getElementById("ejercicio-3").appendChild(empty);
            mas_field.value = '';
            cnt = 0;

        }


    });



    /**
     * Ejecrcicio 4 se nos pide que encontremos un apalabra compuesta de dos palabras y que la remplacemos por otra "Codespace2020"
     */
    const textarea = document.getElementById("textarea");
    textarea.value = `Lorem Ipsum is simply dummy text of the printing and typesetting industry.
 Lorem Ipsum has been the industrys standard dummy text ever since the 1500s,
 when an unknown printer took a galley of type and scrambled it to make a 
 type specimen book. It has survived not only five centuries, but also the 
 leap into electronic typesetting, remaining essentially unchanged. It was 
 popularised in the 1960s with the release of Letraset sheets containing  
 Lorem Ipsum passages, and more recently with desktop publishing software 
 like Aldus  PageMaker including versions of Lorem Ipsum .`;


    const boton_buscar = document.getElementById("buscar");

    boton_buscar.addEventListener("click", (event) => {

        try {

            var longitud = textarea.value.match(/(Lorem Ipsum)/gi).length;
            textarea.value = textarea.value.replace(/(Lorem Ipsum)/gi, "Codespace 2020");
            let empty = document.createElement("P");
            empty.innerText = `La palabra etsaba presente ${longitud} veces`;
            document.getElementById("ejercicio-4").appendChild(empty);

        } catch (error) {

            let empty = document.createElement("P");
            empty.innerText = `Ya cambiaste todo no hay mas coincidencias`;
            document.getElementById("ejercicio-4").appendChild(empty);

        }

    });


    /**
     * Ejecrcicio 5 se nos pide que encontremos un apalabra compuesta de dos palabras y que la remplacemos por otra "Codespace2020"
     */
    const ventana = document.getElementById("ventana");
    ventana.addEventListener("click", (envent) => {

        document.getElementById("link").target = document.getElementById("link").target == "" ? "_blank" : "";


    });



    /**
    * Ejecrcicio 6 se nos pide introducir un numero y disponerlo en orden en una lista ordenada
    */

    boton_lista = document.getElementById("boton-lista");
    var list = [];
   
    var ols = document.getElementById("lista-numeros");

    // for (let i = 0; i < ols.children.length; i++) {

    //     if (list[i] !== ols.children[i].innerHTML) {
    //         list.push(Number(ols.children[i].innerHTML));
    //     }
    // }


    boton_lista.addEventListener("click", (event) => {




          // var bandera = false;
           const cifra = document.getElementById("lista-numero");
 
           var i = 0;
      
           // mientras la i sea menor a la longitud devolvera true, mientras el valor de la cifra introducida se mayor a correpondiente hijo
           while( i < ols.children.length && Number(cifra.value) > Number(ols.children[i].innerHTML) ){
        
            i++;
           }

         

           var li = document.createElement("LI");
           li.innerText = cifra.value;
           ols.appendChild(li);
           ols.insertBefore(li, ols.children[i]);
           cifra.value = "";



       


        // for (let i = 0; i < list.length; i++) {
        //     console.log(Number(cifra.value) +" "+ Number(list[i]));
        //     if (Number(cifra.value) === Number(list[i])) {
        //         bandera = true;
        //     }


        // }

        // if (bandera == false) {
        //     list.push(Number(cifra.value));
        //     var li = document.createElement("LI");
        //     li.innerText = cifra.value;
        //     ols.appendChild(li);
        //     cifra.value = '';
          
        //     let numeros = bubbleSort(list);
        //     console.log(numeros);
        //     for (let i = 0; i <  numeros.length; i++) {
        //         ols.children[i].innerText = numeros[i];
        //     }

        // }

    });

    // function bubbleSortElements(arr, length = arr.length) {
    //     console.log(length);
    //     while (length) {
    //         for (let i = 0; i < list.length; i++) {
    //             if (Number(ols.children[i].innerText) > Number(ols.children[i + 1].innerText)) {
    //                 let temp = ols.children[i].innerText;
    //                 ols.children[i].innerText = ols.children[i + 1].innerText;
    //                 ols.children[i + 1].innerText = temp
    //             }
    //         }
    //         length--;
    //     }
    //     console.log("pinta", lista.length);

    //     return arr;
    // }

    function bubbleSort(arr, length = arr.length) {
        while (length) {
            for (let i = 0; i < arr.length; i++) {
                if (arr[i] > arr[i + 1]) {
                    let temp = arr[i];
                    arr[i] = arr[i + 1];
                    arr[i + 1] = temp
                }
            }
            length--;
        }
        console.log("pinta", lista.length);
        
        return arr;
    }






    /**
    * Ejecrcicio 7 se nos pide cambiar de color el parrafo
    */

    console.log(document.getElementById("sel").children[0].nodeName);
    const seleccion = document.getElementById("sel");
    seleccion.addEventListener("change", (event) => {
        seleccion.style = `color:orange; background-color:white;`;
        seleccion.options[seleccion.selectedIndex].style = `color:orange; background-color:white;`;
    });
    for (let i = 0; i < seleccion.children.length; i++) {
        if (seleccion.children[i].nodeName === "OPTGROUP") {

            for (let j = 0; j < seleccion.children[i].children.length; j++) {
                seleccion.children[i].children[j].style = `background-color: ${getRandomColor()};`;

            }
        }
    }

    function getRandomColor() {
        var letters = '0123456789ABCDEF';
        var color = '#';
        for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }



    /**
    * Ejecrcicio 8 se nos pide cambiar de color el parrafo
    */

    const impar = document.getElementById("impares");
    const par = document.getElementById("pares");
    const boton_agrega = document.getElementById("agrega-numero");

    boton_agrega.addEventListener("click", (event) => {
        let numero = document.getElementById("pares_id").value;
        let rest = numero % 2;
        if (rest != 0) {
            let element = document.createElement("LI");
            element.innerHTML = numero;
            impar.appendChild(element);
            document.getElementById("pares_id").value = '';
        } else {
            let element = document.createElement("LI");
            element.innerHTML = numero;
            par.appendChild(element);
            document.getElementById("pares_id").value = '';
        }
    });

    const boton_genera = document.getElementById("genera-numero");

    boton_genera.addEventListener("click", (event) => {
        let numero = Math.floor((Math.random() * 100) + 1);
        let rest = numero % 2;
        if (rest != 0) {
            let element = document.createElement("LI");
            element.innerHTML = numero;
            impar.appendChild(element);
        } else {
            let element = document.createElement("LI");
            element.innerHTML = numero;
            par.appendChild(element);
        }

    });

    /**
    * Ejecrcicio 9 se nos pide ir agregando numeros
    * no repetidos de lo contrario  avisaarlo
    */

    const boton_ejercicio9 = document.getElementById("boton-ejercicio-9");
    var lista = []

    const ol = document.getElementById("lista-ejercicio-9");
    boton_ejercicio9.addEventListener("click", (event) => {

        let numero = document.getElementById("ejercicio-9-id");
        let bandera = false;
        console.log("numero", numero);
        console.log(bandera == false);


        for (let i = 0; i < lista.length; i++) {

            if (numero.value == lista[i]) {
                bandera = true;
            }
        }

        if (bandera == false) {

            let li = document.createElement("LI");
            li.innerHTML = numero.value;
            ol.appendChild(li);
            lista.push(numero.value);
            numero.value = "";


        } else {

            let li = document.createElement("LI");
            li.innerHTML = `Ese numero ya esta ${numero.value}`
            ol.insertBefore(li, ol.lastChild);
            lista.push(numero.value);
            numero.value = "";


        }

    });

    /**
    * Ejecrcicio 10 se nos pide usar una etiqueta video 
    * para controlar la reproduccion del video y hacer lo inverso
    */

    const video = document.getElementById("video");
    const volumeup = document.getElementById("volume-up");
    const volumedown = document.getElementById("volume-down");
    const boton_reproducir = document.getElementById("boton-ejercicio-10");
    boton_reproducir.addEventListener("click", (event) => {
        console.log("video ", video.volume);
        if (video.paused) {
            video.play();
            boton_reproducir.innerText = "";
            boton_reproducir.innerText = "pause"

        } else {
            video.pause();
            boton_reproducir.innerText = "";
            boton_reproducir.innerText = "Reproducir"
        }


    });
    volumeup.addEventListener("click", (event) => {
        if (video.volume < 0.9)
            video.volume += 0.1;

    });

    volumedown.addEventListener("click", (event) => {
        if (video.volume > 0.1) {
            video.volume -= 0.1;

        }


    });








});





 