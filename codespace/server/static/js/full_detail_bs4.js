const DATOS = [

    {
        id: 0,
        sht_name: "Ferrari 575M Maranello",
        categoria: 2,
        desc: `Comfort: da prioridad a la seguridad y es el programa más indicado para condiciones de baja adherencia. Según Ferrari, es la mejor elección para una utilización diaria y cotidiana.
                Sport: da prioridad a las máximas prestaciones y estabilidad en condiciones de buena adherencia. Es recomendable para una conducción muy rápida.`,
        sku: 45,
        img: "./static/media/ferrari.jpg",
        srcset: ["./static/media/ferrari_competizione.jpg", "./static/media/ferrari_street.jpg", "./static/media/ferrari.jpg"],
        color: ["#cdb5ff", "#3b1740", "#f53f67"],
        tax: 5,
        sub: 12.5,
        total: 3.40,
        star: 3
    }, {
        id: 1,
        sht_name: "Ferrari F430",
        categoria: 5,
        desc: `Comfort: da prioridad a la seguridad y es el programa más indicado para condiciones de baja adherencia. Según Ferrari, es la mejor elección para una utilización diaria y cotidiana.
        Sport: da prioridad a las máximas prestaciones y estabilidad en condiciones de buena adherencia. Es recomendable para una conducción muy rápida.`,
        sku: 45,
        img: "./static/media/ferrari_cabriolet.jpg",
        srcset: ["./static/media/ferrari_competizione.jpg", "./static/media/ferrari_street.jpg", "./static/media/ferrari.jpg"],
        color: ["#cdb5ff", "#3b1740", "#f53f67"],
        tax: 5,
        sub: 12.5,
        total: 10.25,
        star: 1
    }, {
        id: 2,
        sht_name: "Ferrari Enzo",
        categoria: 5,
        desc: `Comfort: da prioridad a la seguridad y es el programa más indicado para condiciones de baja adherencia. Según Ferrari, es la mejor elección para una utilización diaria y cotidiana.
        Sport: da prioridad a las máximas prestaciones y estabilidad en condiciones de buena adherencia. Es recomendable para una conducción muy rápida.`,
        sku: 45,
        img: "./static/media/ferrari_calle.jpg",
        srcset: ["./static/media/ferrari_competizione.jpg", "./static/media/ferrari_street.jpg", "./static/media/ferrari.jpg"],
        color: ["#cdb5ff", "#3b1740", "#f53f67"],
        tax: 5,
        sub: 12.5,
        total: 4.25,
        star: 2
    }, {
        id: 3,
        sht_name: "Ferrari Testarossa",
        categoria: 5,
        desc: `Comfort: da prioridad a la seguridad y es el programa más indicado para condiciones de baja adherencia. Según Ferrari, es la mejor elección para una utilización diaria y cotidiana.
        Sport: da prioridad a las máximas prestaciones y estabilidad en condiciones de buena adherencia. Es recomendable para una conducción muy rápida.`,
        sku: 45,
        img: "./static/media/ferrari_street.jpg",
        srcset: ["./static/media/ferrari_competizione.jpg", "./static/media/ferrari_street.jpg", "./static/media/ferrari.jpg"],
        color: ["#cdb5ff", "#3b1740", "#f53f67"],
        tax: 5,
        sub: 12.5,
        total: 44.25,
        star: 5
    }, {
        id: 4,
        sht_name: "Ferrari 288 GTO",
        categoria: 2,
        desc: `Comfort: da prioridad a la seguridad y es el programa más indicado para condiciones de baja adherencia. Según Ferrari, es la mejor elección para una utilización diaria y cotidiana.
        Sport: da prioridad a las máximas prestaciones y estabilidad en condiciones de buena adherencia. Es recomendable para una conducción muy rápida.`,
        sku: 45,
        img: "./static/media/ferrari_competizione.jpg",
        srcset: ["./static/media/ferrari_competizione.jpg", "./static/media/ferrari_street.jpg", "./static/media/ferrari.jpg"],
        color: ["#cdb5ff", "#3b1740", "#f53f67"],
        tax: 5,
        sub: 12.5,
        total: 24.25,
        star: 3
    },
    {
        id: 5,
        sht_name: "FerrariGTB Fiorano",
        categoria: 2,
        desc: `Comfort: da prioridad a la seguridad y es el programa más indicado para condiciones de baja adherencia. Según Ferrari, es la mejor elección para una utilización diaria y cotidiana.
        Sport: da prioridad a las máximas prestaciones y estabilidad en condiciones de buena adherencia. Es recomendable para una conducción muy rápida.`,
        sku: 45,
        img: "./static/media/ferrari_clasico_calle.jpg",
        srcset: ["./static/media/ferrari_competizione.jpg", "./static/media/ferrari_street.jpg", "./static/media/ferrari.jpg"],
        color: ["#cdb5ff", "#3b1740", "#f53f67"],
        tax: 5,
        sub: 12.5,
        total: 11.25,
        star: 4
    },
    {
        id: 6,
        sht_name: "Ferrari 458 Italia",
        categoria: 2,
        desc: `Comfort: da prioridad a la seguridad y es el programa más indicado para condiciones de baja adherencia. Según Ferrari, es la mejor elección para una utilización diaria y cotidiana.
        Sport: da prioridad a las máximas prestaciones y estabilidad en condiciones de buena adherencia. Es recomendable para una conducción muy rápida.`,
        sku: 45,
        img: "./static/media/ferrari_clasico_pista.jpg",
        srcset: ["./static/media/ferrari_clasico.jpg", "./static/media/ferrari_clasico_pista.jpg", "./static/media/ferrari.jpg"],
        color: ["#cdb5ff", "#3b1740", "#f53f67"],
        tax: 5,
        sub: 12.5,
        total: 11.25,
        star: 4
    },
    {
        id: 7,
        sht_name: "Ferrari 458 Speciale",
        categoria: 2,
        desc: `Comfort: da prioridad a la seguridad y es el programa más indicado para condiciones de baja adherencia. Según Ferrari, es la mejor elección para una utilización diaria y cotidiana.
        Sport: da prioridad a las máximas prestaciones y estabilidad en condiciones de buena adherencia. Es recomendable para una conducción muy rápida.`,
        sku: 45,
        img: "./static/media/ferrari_clasico.jpg",
        srcset: ["./static/media/ferrari_clasico.jpg", "./static/media/ferrari_clasico_pista.jpg", "./static/media/ferrari.jpg"],
        color: ["#cdb5ff", "#3b1740", "#f53f67"],
        tax: 5,
        sub: 12.5,
        total: 11.25,
        star: 4
    }
];

const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);
const id = urlParams.get('q');

$(document).ready(function () {
    $('.alert-success').hide();
    $('.alert-warning').hide();

    $("#main-list").append(buildElement(get_data(id)));

});



function get_data(id) {
    let elemento = "";

    DATOS.forEach(element => {

        if (element.id == id) {
            elemento = element;
            return elemento;
        }

    });

    return elemento;
}

function buildElement(elemento) {

    let item = ` 
    <li class="main col-sm-12 col-md-6 col-lg-6">
    <img src="${elemento.img}" class="img-fluid" alt="">
</li>

<li id="info-mine" class="col-sm-12 col-md-6 col-lg-6">

    <div class="col-xs-12 col-sm-12 pt-5 col-md-12 col-lg-12">
        <p class="text-white">${elemento.desc}</p>
    </div>

    <div class="row justify-content-center">
        <div class="heart col-xs-2 col-sm-2 col-md-2 col-lg-2 text-center">
            <button type="button" onclick="counter(0)" class="heart btn btn-dark btn-default">-</button>
                
        </div>
    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 text-center">
        <span id="quantity"> 0 </span>             
    </div>

    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 text-center">
            <button type="button" onclick="counter(1)" class="heart btn btn-dark btn-default">+</button>
   </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <button type="button" onclick="buy()" class="btn btn-block btn-dark btn-default">button</button>
        </div>
    </div>

</li>
<li class="fotos">
    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-2 gallery ">
        <img src="${elemento.srcset[0]}" class="img-fluid ft" alt="">
    </div>
    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-2 gallery ">
        <img src="${elemento.srcset[1]}" class="img-fluid sc" alt="">
    </div>
    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-2 gallery">
        <img src="${elemento.srcset[2]}" class="img-fluid thr" alt="">
    </div>
</li>`;
    return item;
}

function buy() {

    if($("#quantity").text() > 0){
        let carry = document.getElementsByTagName("my-list")[0];
        let carry_model = get_data(id);
        carry.art = { id: carry_model.id, total: carry_model.total ,sht_name: carry_model.sht_name, img: carry_model.img, quantity: document.getElementById("quantity").innerText };
        $('.alert-success').show();
    }else{
        $('.alert-warning').show();
    }
   
}

function counter(choice) {
    if ($("#quantity").text() >= 0) {
        if (choice == 1) {
            $('.alert-warning').hide();
            let cnt = $("#quantity").text();
            console.log("cnt", cnt);
            cnt = Number(cnt);
            cnt = cnt + 1;
            $("#quantity").text("");
            $("#quantity").text(cnt);
        } else {

            let cnt = $("#quantity").text();
            cnt = Number(cnt);
            if($("#quantity").text() != 0){
                cnt = cnt - 1;
            }
           
            $("#quantity").text("");
            $("#quantity").text(cnt);
        }

    }

}

