(function () {

    const template = document.createElement('template');
    template.innerHTML = `   
    
    <link rel="stylesheet" href="./static/css/carry.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
     integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
     crossorigin="anonymous">
  

     <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
     <span id="title"></span>
    
     </div>
    <div id="main" class="row">
       
    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <button type="button" class="btn btn-primary" list>
            Notifications <span class="noti badge badge-light text-white">4</span>
        </button>
       
    </div>
    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <span id="total"></span>
    </div>
    <div id="listado" class=" col-xs-2 col-sm-2 col-md-2 col-lg-2">
        <ul id="list" class="row">
        </ul>
        </div>
    </div>`;


    class Carry extends HTMLElement {
        constructor() {
            super();

            this.list = this.list.bind(this);
            this.delete = this.delete.bind(this);
            this.lista = [];
            this.attachShadow({ mode: 'open' });
            this.shadowRoot.appendChild(template.content.cloneNode(true));
            this.listBtn = this.shadowRoot.querySelector('[list]');
            this.total = this.shadowRoot.querySelector('#total');
            this._title = this.shadowRoot.querySelector('#title');
            this.liste_items = this.shadowRoot.querySelector('UL');
            this.notification = this.shadowRoot.querySelector('.noti');



            this._remakeList();

            this.deleteBtn = this.shadowRoot.querySelectorAll('[delete]');
            this.notify();
        }
        connectedCallback() {
            this.listBtn.addEventListener('click', this.list);
            for (let index = 0; index < this.liste_items.children.length; index++) {
                const element = this.liste_items.children[index];
                element.addEventListener('click', this.delete);
            }
        }

        get name() {
            return this.getItem("name");
        }

        set name(title) {
            this._title.innerHTML = title;
            this.setAttribute('name', title);
        }

        get art() {

            return JSON.parse(localStorage.getItem("carry_shop"));
        }

        set art(articulo) {
            if (!localStorage.getItem("carry_shop")) {
                this.lista.push(articulo);
                this._insert_item(articulo);
                
                console.log("LcsTg vacio");
            } else {
        var flag = false;
                if (this.lista.length >= 0) {
                    this.lista.forEach(element => {
                        if (element.id == articulo.id) {
                            element.quantity  = Number(   element.quantity ) + Number(articulo.quantity);
                            let q = this.shadowRoot.querySelector("#q-" + articulo.id);
                            console.log(this.shadowRoot.querySelector("#q-" + articulo.id));
                            this.shadowRoot.querySelector("#q-" + articulo.id).innerHTML = "";
                            this.shadowRoot.querySelector("#q-" + articulo.id).innerHTML =  Number(  element.quantity );
                          
                            flag = true;
                            console.log("existo",  Number(q.innerText));
                            
                        } 
                    });
                    if(!flag){
                        flag = !flag;
                        this.lista.push(articulo);
                        this._insert_item(articulo);
                    }
                    
                }


            }



            localStorage.setItem("carry_shop", JSON.stringify(this.lista));
         
            this.notify();
     
          
           

            this.total.innerText = "";
            for (let index = 0; index < this.liste_items.children.length; index++) {
                const element = this.liste_items.children[index];
                element.addEventListener('click', this.delete);
            }
            this.sumTotal();
        }

        static get observedAttributes() {
            return ['name'];
        }

        attributeChangedCallback(name, oldValue, newValue) {
            this._title.innerHTML = this.getAttribute('name');
        }
        /**
         * hide carry list
         */
        list() {
            this.toggle(this.liste_items);

        }
        /**
         * remove item from carry (LI)
         */

        _remakeList() {
          
                if (JSON.parse(localStorage.getItem("carry_shop"))) {
                    this.lista = JSON.parse(localStorage.getItem("carry_shop"));
                    this.notification.innerText = this.lista.length;
                    for (let index = 0; index < this.lista.length; index++) {
                        const element = this.lista[index];

                        let elemento = '<li id="' + element.id
                            + '" class="row justify-content-center">'
                            + '<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">'
                            + '<span> <img src="' + element.img + '" class="img-fluid" ></span>'
                            + '<p class="h6 text-white">' + element.sht_name + '</p>'
                            + '</div>'
                            + '<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">'
                            + '<span id="' + 'q-' + element.id + '" class="h6">' + element.quantity + '</span>' + '<span>' + element.total + '</span>'
                            + '</div>'
                            + ' <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">'
                            + '<p class="delete_item"  id="' + element.id + '" delete>X </p>'
                            + ' </div></li>';


                        this.liste_items.innerHTML += elemento;

                    }

                    this.sumTotal();

                } else {
                    this.lista = [];
                }
   

        }
        _insert_item(articulo){
            let elemento = '<li id="' + articulo.id
            + '" class="row justify-content-center">'
            + '<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">'
            + '<span> <img src="' + articulo.img + '" class="img-fluid" ></span>'
            + '<p class="h6 text-white">' + articulo.sht_name + '</p>'
            + '</div>'
            + '<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">'
            + '<span id="' + 'q-' + articulo.id + '" class="h6">' + articulo.quantity + '</span>' + '<span>' + articulo.total + '</span>'
            + '</div>'
            + ' <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">'
            + '<p class="delete_item"  id="' + articulo.id + '" delete>X </p>'
            + ' </div></li>';
            this.liste_items.innerHTML += elemento;

        }
        delete(event) {

            const id = event.target.id;
               
              if(this.shadowRoot.querySelector("#q-" + id).innerText != null){
                let q = this.shadowRoot.querySelector("#q-" + id).innerText;
                console.log("qqq", q);
                if(q > 0 ){

                    console.log(this.shadowRoot.querySelector("#q-" + id));
                    this.shadowRoot.querySelector("#q-" + id).innerHTML = "";
                    this.shadowRoot.querySelector("#q-" + id).innerHTML =  Number(  q ) - 1; 
                    this.dleteFromLocal(id);
                    return
             
                } 

              }
                    
                   

        }

        /**
         * recalculate the carry list
         */
        sumTotal() {
            this.total.innerText = '';
            let prix = JSON.parse(localStorage.getItem("carry_shop"));
            let total = 0;
            for (let index = 0; index < prix.length; index++) {
                const element = prix[index];
                total = Number(element.total) * Number(element.quantity);
            }
            this.total.innerText = total.toFixed(2);
            this.notify();
        }

        /**
         * 
         * remove from localstorage
         */
        dleteFromLocal(id) {
            var local = JSON.parse(localStorage.getItem("carry_shop"));

            for (let index = 0; index < local.length; index++) {
                const element = local[index];

                if (id == element.id) {
                    if(this.shadowRoot.querySelector("#q-" + id)){
                        if(this.shadowRoot.querySelector("#q-" + id).innerText == "0"){
                            local.splice(index,0);
                            for (let index = 0; index < this.liste_items.children.length; index++) {
                                const elemento = this.liste_items.children[index];
                
                                if (id == elemento.id) {
                                    this.liste_items.removeChild(elemento);
                                    local.splice(index,1)
                                    this.dleteFromLocal(id);
                                    this.sumTotal();
                                    break;
                                }
                            }
                        }
                      
                    }else{
                        element.quantity =  element.quantity -1 ;
                    }
              
                    

                }
            }
            console.log(local);
            this.lista = local;
            this.notify();
            localStorage.setItem("carry_shop", JSON.stringify(this.lista));
        }

        notify() {

            if (this.lista.length > 0) {

                this.notification.innerText = this.lista.length;
                this.notification.style.background = "red";
                this.notification.style.display = "inline-block";
            } else {
                this.notification.style.display = "none";

            }
        }


        /**
         * 
         * hide an element , timing of transition
         */
        toggle(elem, timing) {

            // If the element is visible, hide it
            if (elem.classList.contains('is-visible')) {
                this.hide(elem);
                return;
            }

            // Otherwise, show it
            this.show(elem);

        };

        show(elem) {

            // Get the natural height of the element
            var getHeight = function () {
                elem.style.display = 'inline-block'; // Make it visible
                var height = elem.scrollHeight + 'px'; // Get it's height
                elem.style.display = ''; //  Hide it again
                return height;
            };

            var height = getHeight(); // Get the natural height
            elem.classList.add('is-visible'); // Make the element visible
            elem.style.height = height; // Update the max-height

            // Once the transition is complete, remove the inline max-height so the content can scale responsively
            window.setTimeout(function () {
                elem.style.height = '';
            }, 350);

        };

        hide(elem) {

            // Give the element a height to change from
            elem.style.height = elem.scrollHeight + 'px';

            // Set the height back to 0
            window.setTimeout(function () {
                elem.style.height = '0';
            }, 1);

            // When the transition is complete, hide it
            window.setTimeout(function () {
                elem.classList.remove('is-visible');
            }, 350);

        };




        disconnectedCallback() {
            this.listBtn.removeEventListener('click', this.list);
            for (let index = 0; index < this.liste_items.children.length; index++) {
                const element = this.liste_items.children[index];
                element.removeEventListener('click', this.delete);

            }
        }

    }

    window.customElements.define('my-list', Carry);


})();