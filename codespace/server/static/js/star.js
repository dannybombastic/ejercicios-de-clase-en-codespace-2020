const sheet = new CSSStyleSheet();
const sky = new CSSStyleSheet();
sky.replace(`@import url('./static/css/star.css'`);
sheet.replace(`@import url('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'`);
const template = document.createElement('template');
template.innerHTML = `   

<div id="star">

</div>`;

export class Star extends HTMLElement {
    constructor(max = 0, star = 0) {
        super();

        this.attachShadow({mode:"open"});
        this.shadowRoot.appendChild(template.content.cloneNode(true));
        this.shadowRoot.adoptedStyleSheets = [sheet,sky];
        this.estarellas = this.shadowRoot.querySelector("#star");
        this.setAttribute("star", star);
        this.setAttribute("max", max);
        this._generateStart();
    }

    get max(){

        return this.getAttribute("max");
    }

    set max(numero){

        this.setAttribute("max", numero);
    }
    
    get star(){
        return this.getAttribute("star");
    }
    set star(star){
        this.setAttribute("star", star);
    }

    _generateStart(){
        let lngt = this.getAttribute("max");
        let limit = this.getAttribute("star");
     
        for (let index = 0; index < lngt; index++) {
           let span =document.createElement("SPAN");
           span.classList += " fa fa-star";
            if(limit > index){
                span.classList.add("checked");
            }
            this.estarellas.appendChild(span);

            
        }
    }



}

window.customElements.define("x-start", Star);