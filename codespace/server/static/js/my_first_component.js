class Button extends HTMLElement {
    constructor() {
        super();


        const shadow = this.attachShadow({ mode: 'open' });
        this.btn = this.shadowRoot.querySelector(".btn-custom");

        shadow.appendChild(this.customCss());
        shadow.appendChild(this.component(this));

    }

    get altura() {
        return this.getAttribute("altura");
    }

    set altura(alturas) {
        this.setAttribute("altura", alturas);
    }

    get ancho() {
        return this.getAttribute("altura");
    }

    set ancho(anchos) {
        this.setAttribute("altura", anchos);
    }
    static get observedAttributes() {
        return ['altura'];
    }

    attributeChangedCallback(name, oldValue, newValue) {

        console.log(this.getAttribute('altura'));
    }
    connectedCallback() { }

    disconnectedCallback() {

    }

    customCss() {

        let link = document.createElement("LINK");
        link.setAttribute("rel", "stylesHeet");
        link.setAttribute("href", "./static/css/my_first_component.css");
        return link;
    }

    alerta(value) {
        alert(value);

    }
    component() {
        let _this = this;
        const btn = document.createElement("BUTTON");
        btn.innerText = this.getAttribute("value");
        btn.setAttribute("class", "btn-custom");
        btn.style.width = this.getAttribute("ancho");
        btn.style.height = this.getAttribute("altura");
        btn.style.background = this.getAttribute("bg")
        btn.onclick = function () {
            _this.alerta("value");
        }

        return btn;

    }


}

customElements.define('button-hola', Button);



