export function datos_tienda(){
    return [

        {
            id: 0,
            categoria: 2,
            desc: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy  ",
            sku: 45,
            img: "./static/media/code_1.jpg",
            color: ["rojo", "negro", "blanco"],
            tax: 5,
            sub: 12.5,
            total: 14.25
        }, {
            id: 1,
            categoria: 5,
            desc: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy  ",
            sku: 45,
            img: "./static/media/code_1.jpg",
            color: ["rojo", "negro", "blanco"],
            tax: 5,
            sub: 12.5,
            total: 14.25
        }, {
            id: 2,
            categoria: 5,
            desc: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy  ",
            sku: 45,
            img: "./static/media/code_1.jpg",
            color: ["rojo", "negro", "blanco"],
            tax: 5,
            sub: 12.5,
            total: 14.25
        }, {
            id: 3,
            categoria: 5,
            desc: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy  ",
            sku: 45,
            img: "./static/media/code_1.jpg",
            color: ["rojo", "negro", "blanco"],
            tax: 5,
            sub: 12.5,
            total: 14.25
        }, {
            id: 4,
            categoria: 2,
            desc: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy  ",
            sku: 45,
            img: "./static/media/code_1.jpg",
            color: ["rojo", "negro", "blanco"],
            tax: 5,
            sub: 12.5,
            total: 14.25
        },
        {
            id: 5,
            categoria: 2,
            desc: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy  ",
            sku: 45,
            img: "./static/media/code_1.jpg",
            color: ["rojo", "negro", "blanco"],
            tax: 5,
            sub: 12.5,
            total: 14.25
        }
    ];
}