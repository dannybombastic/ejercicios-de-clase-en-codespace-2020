$(document).ready(function () {


    if(sessionStorage.getItem("visited") !== "ok" ){
        $("#welcom-layer").click(function (e) {
            e.preventDefault();
    
    
    
            $("#welcom-layer").slideUp(3000, function () {
                sessionStorage.setItem("visited", "ok");
            });
        });

    }else{
        $("#welcom-layer").hide();
    }

   



    $([document.documentElement, document.body]).animate({
        scrollTop: $("#welcom-layer").offset().top
    }, 500);




});

//  var b = 6;
//  var obj = {id:2, cantidad:2};
//  localStorage.setItem("papaya", JSON.stringify(obj) );
//  obj.cantidad = 3;
//  localStorage.setItem("papaya", JSON.stringify(obj));
//  obj.cantidad = 10;
//  localStorage.setItem("papaya", JSON.stringify(obj))
// if(localStorage.getItem("papaya")){
//     console.log("b nulo", null);
// }


console.log("estamos por aqui :)");
const DATOS = [

    {
        id: 0,
        sht_name: "Ferrari 575M Maranello",
        categoria: 2,
        desc: `Comfort: da prioridad a la seguridad y es el programa más indicado para condiciones de baja adherencia. Según Ferrari, es la mejor elección para una utilización diaria y cotidiana.
                Sport: da prioridad a las máximas prestaciones y estabilidad en condiciones de buena adherencia. Es recomendable para una conducción muy rápida.`,
        sku: 45,
        img: "./static/media/ferrari.jpg",
        color: ["#cdb5ff", "#3b1740", "#f53f67"],
        tax: 5,
        sub: 12.5,
        total: 3.40,
        star: 3
    }, {
        id: 1,
        sht_name: "Ferrari F430",
        categoria: 5,
        desc: `Comfort: da prioridad a la seguridad y es el programa más indicado para condiciones de baja adherencia. Según Ferrari, es la mejor elección para una utilización diaria y cotidiana.
        Sport: da prioridad a las máximas prestaciones y estabilidad en condiciones de buena adherencia. Es recomendable para una conducción muy rápida.`,
        sku: 45,
        img: "./static/media/ferrari_cabriolet.jpg",
        color: ["#cdb5ff", "#3b1740", "#f53f67"],
        tax: 5,
        sub: 12.5,
        total: 10.25,
        star: 1
    }, {
        id: 2,
        sht_name: "Ferrari Enzo",
        categoria: 5,
        desc: `Comfort: da prioridad a la seguridad y es el programa más indicado para condiciones de baja adherencia. Según Ferrari, es la mejor elección para una utilización diaria y cotidiana.
        Sport: da prioridad a las máximas prestaciones y estabilidad en condiciones de buena adherencia. Es recomendable para una conducción muy rápida.`,
        sku: 45,
        img: "./static/media/ferrari_calle.jpg",
        color: ["#cdb5ff", "#3b1740", "#f53f67"],
        tax: 5,
        sub: 12.5,
        total: 4.25,
        star: 2
    }, {
        id: 3,
        sht_name: "Ferrari Testarossa",
        categoria: 5,
        desc: `Comfort: da prioridad a la seguridad y es el programa más indicado para condiciones de baja adherencia. Según Ferrari, es la mejor elección para una utilización diaria y cotidiana.
        Sport: da prioridad a las máximas prestaciones y estabilidad en condiciones de buena adherencia. Es recomendable para una conducción muy rápida.`,
        sku: 45,
        img: "./static/media/ferrari_street.jpg",
        color: ["#cdb5ff", "#3b1740", "#f53f67"],
        tax: 5,
        sub: 12.5,
        total: 44.25,
        star: 5
    }, {
        id: 4,
        sht_name: "Ferrari 288 GTO",
        categoria: 2,
        desc: `Comfort: da prioridad a la seguridad y es el programa más indicado para condiciones de baja adherencia. Según Ferrari, es la mejor elección para una utilización diaria y cotidiana.
        Sport: da prioridad a las máximas prestaciones y estabilidad en condiciones de buena adherencia. Es recomendable para una conducción muy rápida.`,
        sku: 45,
        img: "./static/media/ferrari_competizione.jpg",
        color: ["#cdb5ff", "#3b1740", "#f53f67"],
        tax: 5,
        sub: 12.5,
        total: 24.25,
        star: 3
    },
    {
        id: 5,
        sht_name: "FerrariGTB Fiorano",
        categoria: 2,
        desc: `Comfort: da prioridad a la seguridad y es el programa más indicado para condiciones de baja adherencia. Según Ferrari, es la mejor elección para una utilización diaria y cotidiana.
        Sport: da prioridad a las máximas prestaciones y estabilidad en condiciones de buena adherencia. Es recomendable para una conducción muy rápida.`,
        sku: 45,
        img: "./static/media/ferrari_clasico_calle.jpg",
        color: ["#cdb5ff", "#3b1740", "#f53f67"],
        tax: 5,
        sub: 12.5,
        total: 11.25,
        star: 4
    },
    {
        id: 6,
        sht_name: "Ferrari 458 Italia",
        categoria: 2,
        desc: `Comfort: da prioridad a la seguridad y es el programa más indicado para condiciones de baja adherencia. Según Ferrari, es la mejor elección para una utilización diaria y cotidiana.
        Sport: da prioridad a las máximas prestaciones y estabilidad en condiciones de buena adherencia. Es recomendable para una conducción muy rápida.`,
        sku: 45,
        img: "./static/media/ferrari_clasico_pista.jpg",
        color: ["#cdb5ff", "#3b1740", "#f53f67"],
        tax: 5,
        sub: 12.5,
        total: 11.25,
        star: 4
    },
    {
        id: 7,
        sht_name: "Ferrari 458 Speciale",
        categoria: 2,
        desc: `Comfort: da prioridad a la seguridad y es el programa más indicado para condiciones de baja adherencia. Según Ferrari, es la mejor elección para una utilización diaria y cotidiana.
        Sport: da prioridad a las máximas prestaciones y estabilidad en condiciones de buena adherencia. Es recomendable para una conducción muy rápida.`,
        sku: 45,
        img: "./static/media/ferrari_clasico.jpg",
        color: ["#cdb5ff", "#3b1740", "#f53f67"],
        tax: 5,
        sub: 12.5,
        total: 11.25,
        star: 4
    },{
        id: 8,
        sht_name: "Ferrari 288 GTO",
        categoria: 2,
        desc: `Comfort: da prioridad a la seguridad y es el programa más indicado para condiciones de baja adherencia. Según Ferrari, es la mejor elección para una utilización diaria y cotidiana.
        Sport: da prioridad a las máximas prestaciones y estabilidad en condiciones de buena adherencia. Es recomendable para una conducción muy rápida.`,
        sku: 45,
        img: "./static/media/ferrari_competizione.jpg",
        color: ["#cdb5ff", "#3b1740", "#f53f67"],
        tax: 5,
        sub: 12.5,
        total: 24.25,
        star: 3
    },
    {
        id: 9,
        sht_name: "FerrariGTB Fiorano",
        categoria: 2,
        desc: `Comfort: da prioridad a la seguridad y es el programa más indicado para condiciones de baja adherencia. Según Ferrari, es la mejor elección para una utilización diaria y cotidiana.
        Sport: da prioridad a las máximas prestaciones y estabilidad en condiciones de buena adherencia. Es recomendable para una conducción muy rápida.`,
        sku: 45,
        img: "./static/media/ferrari_clasico_calle.jpg",
        color: ["#cdb5ff", "#3b1740", "#f53f67"],
        tax: 5,
        sub: 12.5,
        total: 11.25,
        star: 4
    },
    {
        id: 10,
        sht_name: "Ferrari 458 Italia",
        categoria: 2,
        desc: `Comfort: da prioridad a la seguridad y es el programa más indicado para condiciones de baja adherencia. Según Ferrari, es la mejor elección para una utilización diaria y cotidiana.
        Sport: da prioridad a las máximas prestaciones y estabilidad en condiciones de buena adherencia. Es recomendable para una conducción muy rápida.`,
        sku: 45,
        img: "./static/media/ferrari_clasico_pista.jpg",
        color: ["#cdb5ff", "#3b1740", "#f53f67"],
        tax: 5,
        sub: 12.5,
        total: 11.25,
        star: 4
    },
    {
        id: 11,
        sht_name: "Ferrari 458 Speciale",
        categoria: 2,
        desc: `Comfort: da prioridad a la seguridad y es el programa más indicado para condiciones de baja adherencia. Según Ferrari, es la mejor elección para una utilización diaria y cotidiana.
        Sport: da prioridad a las máximas prestaciones y estabilidad en condiciones de buena adherencia. Es recomendable para una conducción muy rápida.`,
        sku: 45,
        img: "./static/media/ferrari_clasico.jpg",
        color: ["#cdb5ff", "#3b1740", "#f53f67"],
        tax: 5,
        sub: 12.5,
        total: 11.25,
        star: 4
    }
];

var sandwiches = [
	'tuna',
	'ham',
	'turkey',
	'pb&j'
];

sandwiches.forEach((sandwich, index)=> {

	// If the sandwich is turkey, skip it

     sandwiches.splice(0, 1)
	// Otherwise log it to the console


});

console.log(sandwiches);
function cargar_productos_jquery() {
    var grilla = $("#row-main-list");
    console.log("entrando");
    $.each(DATOS, function (indexInArray, valueOfElement) {

        let li = `
        <li class="col-sm-12 col-md-6 col-lg-3 pt-2"  data-id="${valueOfElement.id}" data-total="${valueOfElement.total}" data-sku="${valueOfElement.sku}>
        <!-- Card for items  -->
       
        <div class="card bg-dark border-dark bg-dark" style="width: 98%;">
        <h5 class="card-title rounded bg-dark p-2 text-center text-white">${valueOfElement.sht_name}</h5>
          <img class="sign" src="./static/media/ferrai_logo.png"><span class="italia-g"></span>
          <span class="italia-w"></span><span class="italia-r"></span>
           <div class="row">
           <div class="col-12">
           <img src="${valueOfElement.img}" class="card-img-top" alt="...">
           </div>
           </div>
          <div class="card-body container bgb-button border-left border-right border-danger">
      
            <div class="row pl-2 pr-2 align-content-start">
            <span></span>
              <div class="col-6 h-100">
                <button value="${valueOfElement.id}" onclick="modal(this)" class="btn btn-dark btn-block border-dark" type="button">
                History
              </button>
              <div class="row">
              <div class="col-12 d-flex justify-content-center">
              <x-modal class="card" id="x-modal-${valueOfElement.id}" max="5" star="${valueOfElement.star}" title="Important!"</x-modal>
              <p> ${valueOfElement.desc}</p>
              </xmodal>
              </div>
           </div>
             
              </div>
              <div class="col-6 h-100">
              <a href="./full_detail_bs4.html?q=${valueOfElement.id}" class="btn btn-dark btn-block border-dark" type="button">Details</a>
              </div>
            </div>
           
             
            </div>
          </div>
        </div>
        </li>`

        grilla.append($(li));
        console.log("lista string ", grilla);

        // solo se puede asignas obj jquery a otro jquery
        //  let arr =  $("<p>", { text: valueOfElement.total }).appendTo("main");
        // console.log("renderizando", arr);

    });

}

function modal(element) {
    console.log("pulsando", $(element).val());
    $("#x-modal-" + $(element).val()).attr("visible", true);
}

cargar_productos_jquery();

