class InputCustom extends HTMLElement {
    constructor() {
        super();
        this.labeled = document.createElement("LABEL");
        const shadow = this.attachShadow({ mode: 'open' });
        shadow.appendChild(this.css());
        shadow.appendChild(this.labeled);
        shadow.appendChild(this.creator());
       
    }



    creator() {

        var input = document.createElement("INPUT");

        this.labeled.setAttribute("for", this.getAttribute("id"));
        this.labeled.innerText = this.getAttribute("label");
        input.style.border = this.getAttribute("border");
        input.style.height = this.getAttribute("height");
        input.style.width = this.getAttribute("width");
        input.setAttribute("id", this.getAttribute("id"));
        input.style.display = "block";


        switch (this.getAttribute("type")) {

            case "tel":
                this.telefono(input, this);

                break;
            case "texto":

                this.texto(input, this);

                break;
            case "email":

                this.email(input, this);

                break;
            case "numero":
                this.numero(input, this);
                
                break;
            default:
                break;
        }
        return input;
    }

    max(input) {
        if (this.getAttribute("maxi")) {

            input.maxLength = this.getAttribute("maxi");
        }
    }

    css() {

        let css = document.createElement("LINK");
        css.setAttribute("rel", "stylesheet");
        css.setAttribute("href", "./static/css/input_component.css");
        return css;
    }

    telefono(input, ctx) {

        this.max(input);
        input.onblur = function () {
            let sufix = this.value.slice(0, 3);
            if (sufix === "952" && this.value.length > 8) {
                ctx.labeled.style.color ="green";
                ctx.labeled.innerText = "El numero de telefono es valido nen";
            } else {
                ctx.labeled.style.color ="red";
                ctx.labeled.innerText = "El numero de telefono no es valido nen"
            }
        }
    }

    texto(input, ctx) {
        this.max(input);
        input.onkeypress = function (event) {
            console.log(event.key);
            ctx.labeled.innerText = "Texto ok";
            if (!isNaN(Number(event.key))) {
                event.preventDefault();
                ctx.labeled.innerText = "meta solo texto";
            }
        }
    }
    
    numero(input, ctx) {
        this.max(input);
        input.onkeypress = function (event) {
            console.log(event.key);
            ctx.labeled.style.color ="green";
            ctx.labeled.innerText = "Numeros ok";
            if (isNaN(Number(event.key))) {
                event.preventDefault();
                ctx.labeled.style.color ="red";
                ctx.labeled.innerText = "Meta solo Numeros";
               
            }
        }
    }


    email(input, ctx) {
        console.log("holaaa");  input.onblur = function () {

            let email = this.value;

            var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
            if (!expr.test(email)) {
                ctx.labeled.style.color ="red";
                ctx.labeled.innerText = "Mal formato";
           
            } else {
                ctx.labeled.style.color ="green";
                ctx.labeled.innerText = "Todo ok ";

            }
        }

    }

    static get observedAttributes() {
        return ['label'];
    }

    attributeChangedCallback(name, oldValue, newValue) {
        this.labeled.innerText = this.getAttribute('label');


    }
    connectedCallback() { }

    disconnectedCallback() {

    }

}
customElements.define("custom-input", InputCustom);



