-- phpMyAdmin SQL Dump
-- version 4.9.1deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 26, 2020 at 10:35 PM
-- Server version: 10.3.20-MariaDB-0ubuntu0.19.10.1
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `insurance`
--

-- --------------------------------------------------------

--
-- Stand-in structure for view `ALL CRASH REPORT BY MONTH`
-- (See below for the actual view)
--
CREATE TABLE `ALL CRASH REPORT BY MONTH` (
`mes_accidente` varchar(9)
,`fecha_accidente` date
,`estado` enum('BROKEN','FIXED')
,`cif` varchar(24)
,`comercial` varchar(24)
,`entida` varchar(24)
,`polici_price` decimal(15,2)
,`dni` varchar(24)
,`nombre` varchar(24)
,`fecha` date
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `ALL FROM ONE USER`
-- (See below for the actual view)
--
CREATE TABLE `ALL FROM ONE USER` (
`cif` varchar(24)
,`comercial` varchar(24)
,`entida` varchar(24)
,`polici_price` decimal(15,2)
,`dni` varchar(24)
,`nombre` varchar(24)
);

-- --------------------------------------------------------

--
-- Table structure for table `car_crash`
--

CREATE TABLE `car_crash` (
  `car_id` int(11) NOT NULL,
  `pay_policy` int(11) NOT NULL,
  `car_state` enum('BROKEN','FIXED') NOT NULL,
  `car_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `car_crash`
--

INSERT INTO `car_crash` (`car_id`, `pay_policy`, `car_state`, `car_date`) VALUES
(1, 1, 'BROKEN', '2020-01-23'),
(2, 1, 'FIXED', '2020-01-29'),
(3, 2, 'BROKEN', '2019-10-08'),
(4, 2, 'FIXED', '2020-01-01'),
(5, 2, 'BROKEN', '2020-01-23'),
(6, 2, 'FIXED', '2020-01-25'),
(7, 5, 'BROKEN', '2020-01-23');

-- --------------------------------------------------------

--
-- Stand-in structure for view `HISTORIC FROM ONE POLICY`
-- (See below for the actual view)
--
CREATE TABLE `HISTORIC FROM ONE POLICY` (
`mes_accidente` varchar(9)
,`fecha_accidente` date
,`estado` enum('BROKEN','FIXED')
,`cif` varchar(24)
,`comercial` varchar(24)
,`entida` varchar(24)
,`polici_price` decimal(15,2)
,`dni` varchar(24)
,`nombre` varchar(24)
,`fecha` date
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `OLD POLICY AND NOT RENEW`
-- (See below for the actual view)
--
CREATE TABLE `OLD POLICY AND NOT RENEW` (
`cif` varchar(24)
,`comercial` varchar(24)
,`entida` varchar(24)
,`polici_price` decimal(15,2)
,`dni` varchar(24)
,`nombre` varchar(24)
,`fecha` date
);

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `pay_id` int(11) NOT NULL,
  `pay_policy` int(11) NOT NULL,
  `pay_state` enum('PENDING','REJECT','INPROGRESS','FINISH') NOT NULL,
  `pay_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`pay_id`, `pay_policy`, `pay_state`, `pay_date`) VALUES
(1, 2, 'PENDING', '2020-01-31'),
(2, 1, 'INPROGRESS', '2020-01-29'),
(3, 3, 'FINISH', '2020-01-23');

-- --------------------------------------------------------

--
-- Table structure for table `policy`
--

CREATE TABLE `policy` (
  `policy_id` int(11) NOT NULL,
  `polici_price` decimal(15,2) DEFAULT NULL,
  `policy_subject` varchar(24) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `policy`
--

INSERT INTO `policy` (`policy_id`, `polici_price`, `policy_subject`) VALUES
(1, '1000.00', 'Home'),
(2, '300.00', 'MotorBike'),
(3, '450.00', 'Car'),
(4, '650.00', 'Dual vehicule');

-- --------------------------------------------------------

--
-- Table structure for table `policy_user`
--

CREATE TABLE `policy_user` (
  `polici_user_id` int(11) NOT NULL,
  `policy_id` int(11) NOT NULL,
  `portfolio_id` int(11) NOT NULL,
  `policy_start` date NOT NULL,
  `policy_end` date NOT NULL,
  `policy_gateway` enum('TRANSFER','CARD','BANKACCOUNT') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `policy_user`
--

INSERT INTO `policy_user` (`polici_user_id`, `policy_id`, `portfolio_id`, `policy_start`, `policy_end`, `policy_gateway`) VALUES
(1, 1, 7, '2020-01-24', '2020-12-31', 'CARD'),
(2, 2, 8, '2018-05-08', '2020-01-08', 'BANKACCOUNT'),
(3, 2, 9, '2020-03-20', '2020-09-18', 'TRANSFER'),
(4, 2, 9, '2017-10-03', '2019-10-16', 'CARD'),
(5, 1, 8, '2020-01-25', '2020-07-31', 'BANKACCOUNT'),
(6, 4, 9, '2020-01-01', '2020-04-30', 'CARD'),
(7, 4, 10, '2019-07-09', '2020-04-30', 'CARD');

-- --------------------------------------------------------

--
-- Table structure for table `portfolio`
--

CREATE TABLE `portfolio` (
  `id` int(11) NOT NULL,
  `seller_id` int(11) NOT NULL,
  `user_dni` varchar(24) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `portfolio`
--

INSERT INTO `portfolio` (`id`, `seller_id`, `user_dni`) VALUES
(6, 3, '89258574z'),
(7, 3, '9342525p'),
(8, 4, '79020208b'),
(9, 4, '79020208m'),
(10, 3, '9865657w');

-- --------------------------------------------------------

--
-- Stand-in structure for view `RENEW CRITERIUM`
-- (See below for the actual view)
--
CREATE TABLE `RENEW CRITERIUM` (
`contador` varbinary(52)
,`ano_accidente` varchar(11)
,`fecha_accidente` varchar(11)
,`estado` varchar(8)
,`cif` varchar(24)
,`comercial` varchar(24)
,`entida` varchar(24)
,`polici_price` decimal(15,2)
,`dni` varchar(24)
,`nombre` varchar(24)
,`fecha_fin` date
,`penalizacion` decimal(16,2)
,`total` decimal(17,2)
,`tipo` varchar(26)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `SELLER POTFOLIO`
-- (See below for the actual view)
--
CREATE TABLE `SELLER POTFOLIO` (
`vendedor` varchar(24)
,`cif` varchar(24)
,`entidad` varchar(24)
,`precio` decimal(15,2)
,`ganancias` decimal(16,2)
);

-- --------------------------------------------------------

--
-- Table structure for table `seller_profile`
--

CREATE TABLE `seller_profile` (
  `seller_id` int(11) NOT NULL,
  `user_cif` varchar(24) NOT NULL,
  `user_name` varchar(24) NOT NULL,
  `user_addr` varchar(60) NOT NULL,
  `gender` enum('M','F') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `seller_profile`
--

INSERT INTO `seller_profile` (`seller_id`, `user_cif`, `user_name`, `user_addr`, `gender`) VALUES
(3, '2524334NHNG', 'Daniel', 'Juan ramon jimenez nº 15', 'M'),
(4, '6548777HLN', 'Antonio', 'Juan ramon jimenez nº 5', 'M');

-- --------------------------------------------------------

--
-- Table structure for table `user_profile`
--

CREATE TABLE `user_profile` (
  `user_dni` varchar(24) NOT NULL,
  `user_name` varchar(24) NOT NULL,
  `user_addr` varchar(60) NOT NULL,
  `gender` enum('M','F') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_profile`
--

INSERT INTO `user_profile` (`user_dni`, `user_name`, `user_addr`, `gender`) VALUES
('79020208b', 'Gamora Thanos', 'Tanoslandia n5', 'F'),
('79020208m', 'Thor Asgardiano', 'Asgard palacio n 4', 'M'),
('89258574z', 'Nebula Tanos', 'Tanoslandia n 5', 'F'),
('9342525p', 'start lord', 'Universe no place ', 'M'),
('9865657w', 'Indiana jhones', 'Juan ramon jimenez nº 3', 'M');

-- --------------------------------------------------------

--
-- Structure for view `ALL CRASH REPORT BY MONTH`
--
DROP TABLE IF EXISTS `ALL CRASH REPORT BY MONTH`;

CREATE ALGORITHM=UNDEFINED DEFINER=`dannybombastic`@`localhost` SQL SECURITY DEFINER VIEW `ALL CRASH REPORT BY MONTH`  AS  select monthname(`cr`.`car_date`) AS `mes_accidente`,`cr`.`car_date` AS `fecha_accidente`,`cr`.`car_state` AS `estado`,`se`.`user_cif` AS `cif`,`se`.`user_name` AS `comercial`,`po`.`policy_subject` AS `entida`,`po`.`polici_price` AS `polici_price`,`up`.`user_dni` AS `dni`,`up`.`user_name` AS `nombre`,`us`.`policy_end` AS `fecha` from (((((`policy_user` `us` join `car_crash` `cr` on(`us`.`polici_user_id` = `cr`.`pay_policy`)) join `portfolio` `pt` on(`us`.`portfolio_id` = `pt`.`id`)) join `seller_profile` `se` on(`pt`.`seller_id` = `se`.`seller_id`)) join `user_profile` `up` on(`pt`.`user_dni` = `up`.`user_dni`)) join `policy` `po` on(`us`.`policy_id` = `po`.`policy_id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `ALL FROM ONE USER`
--
DROP TABLE IF EXISTS `ALL FROM ONE USER`;

CREATE ALGORITHM=UNDEFINED DEFINER=`dannybombastic`@`localhost` SQL SECURITY DEFINER VIEW `ALL FROM ONE USER`  AS  select `se`.`user_cif` AS `cif`,`se`.`user_name` AS `comercial`,`po`.`policy_subject` AS `entida`,`po`.`polici_price` AS `polici_price`,`up`.`user_dni` AS `dni`,`up`.`user_name` AS `nombre` from ((((`policy_user` `us` join `portfolio` `pt` on(`us`.`portfolio_id` = `pt`.`id`)) join `seller_profile` `se` on(`pt`.`seller_id` = `se`.`seller_id`)) join `user_profile` `up` on(`pt`.`user_dni` = `up`.`user_dni`)) join `policy` `po` on(`us`.`policy_id` = `po`.`policy_id`)) where `up`.`user_name` = 'Gamora Thanos' ;

-- --------------------------------------------------------

--
-- Structure for view `HISTORIC FROM ONE POLICY`
--
DROP TABLE IF EXISTS `HISTORIC FROM ONE POLICY`;

CREATE ALGORITHM=UNDEFINED DEFINER=`dannybombastic`@`localhost` SQL SECURITY DEFINER VIEW `HISTORIC FROM ONE POLICY`  AS  select monthname(`cr`.`car_date`) AS `mes_accidente`,`cr`.`car_date` AS `fecha_accidente`,`cr`.`car_state` AS `estado`,`se`.`user_cif` AS `cif`,`se`.`user_name` AS `comercial`,`po`.`policy_subject` AS `entida`,`po`.`polici_price` AS `polici_price`,`up`.`user_dni` AS `dni`,`up`.`user_name` AS `nombre`,`us`.`policy_end` AS `fecha` from (((((`policy_user` `us` join `car_crash` `cr` on(`us`.`polici_user_id` = `cr`.`pay_policy`)) join `portfolio` `pt` on(`us`.`portfolio_id` = `pt`.`id`)) join `seller_profile` `se` on(`pt`.`seller_id` = `se`.`seller_id`)) join `user_profile` `up` on(`pt`.`user_dni` = `up`.`user_dni`)) join `policy` `po` on(`us`.`policy_id` = `po`.`policy_id`)) where `us`.`polici_user_id` = '2' order by `cr`.`car_date` ;

-- --------------------------------------------------------

--
-- Structure for view `OLD POLICY AND NOT RENEW`
--
DROP TABLE IF EXISTS `OLD POLICY AND NOT RENEW`;

CREATE ALGORITHM=UNDEFINED DEFINER=`dannybombastic`@`localhost` SQL SECURITY DEFINER VIEW `OLD POLICY AND NOT RENEW`  AS  select `se`.`user_cif` AS `cif`,`se`.`user_name` AS `comercial`,`po`.`policy_subject` AS `entida`,`po`.`polici_price` AS `polici_price`,`up`.`user_dni` AS `dni`,`up`.`user_name` AS `nombre`,`us`.`policy_end` AS `fecha` from ((((`policy_user` `us` join `portfolio` `pt` on(`us`.`portfolio_id` = `pt`.`id`)) join `seller_profile` `se` on(`pt`.`seller_id` = `se`.`seller_id`)) join `user_profile` `up` on(`pt`.`user_dni` = `up`.`user_dni`)) join `policy` `po` on(`us`.`policy_id` = `po`.`policy_id`)) where `us`.`policy_end` < current_timestamp() ;

-- --------------------------------------------------------

--
-- Structure for view `RENEW CRITERIUM`
--
DROP TABLE IF EXISTS `RENEW CRITERIUM`;

CREATE ALGORITHM=UNDEFINED DEFINER=`dannybombastic`@`localhost` SQL SECURITY DEFINER VIEW `RENEW CRITERIUM`  AS  select count(0) AS `contador`,year(`cr`.`car_date`) AS `ano_accidente`,`cr`.`car_date` AS `fecha_accidente`,`cr`.`car_state` AS `estado`,`se`.`user_cif` AS `cif`,`se`.`user_name` AS `comercial`,`po`.`policy_subject` AS `entida`,`po`.`polici_price` AS `polici_price`,`up`.`user_dni` AS `dni`,`up`.`user_name` AS `nombre`,`us`.`policy_end` AS `fecha_fin`,cast(`po`.`polici_price` * if(count(0) > 1,15,'0') / 100 as decimal(16,2)) AS `penalizacion`,`po`.`polici_price` + cast(`po`.`polici_price` * if(count(0) > 1,15,'0') / 100 as decimal(16,2)) AS `total`,if(count(0) > 1,'mas de un parte por año','un parte por año') AS `tipo` from (((((`policy_user` `us` join `car_crash` `cr` on(`us`.`polici_user_id` = `cr`.`pay_policy`)) join `portfolio` `pt` on(`us`.`portfolio_id` = `pt`.`id`)) join `seller_profile` `se` on(`pt`.`seller_id` = `se`.`seller_id`)) join `user_profile` `up` on(`pt`.`user_dni` = `up`.`user_dni`)) join `policy` `po` on(`us`.`policy_id` = `po`.`policy_id`)) group by `up`.`user_name`,`us`.`policy_end` union select if(year(current_timestamp()) - year(`us`.`policy_start`) > 0,'Un año o mas','Su primer año') AS `contador`,'NO ACCIDENT' AS `ano_accidente`,'NO ACCIDENT' AS `fecha_accidente`,'NO STATE' AS `estado`,`se`.`user_cif` AS `cif`,`se`.`user_name` AS `comercial`,`po`.`policy_subject` AS `entida`,`po`.`polici_price` AS `polici_price`,`up`.`user_dni` AS `dni`,`up`.`user_name` AS `nombre`,`us`.`policy_end` AS `fecha_fin`,cast(`po`.`polici_price` * if(year(current_timestamp()) - year(`us`.`policy_start`) > 1,-20,-7) / 100 as decimal(16,2)) AS `penalizacion`,cast(`po`.`polici_price` + `po`.`polici_price` * if(year(current_timestamp()) - year(`us`.`policy_start`) > 1,-20,-7) / 100 as decimal(16,2)) AS `total`,if(year(current_timestamp()) - year(`us`.`policy_start`) > 1,'mas de dos años sin partes','un año sin partes') AS `tipo` from (((((`policy_user` `us` join `car_crash` `cr` on(!(`us`.`polici_user_id` in (select `car_crash`.`pay_policy` from `car_crash`)))) join `portfolio` `pt` on(`us`.`portfolio_id` = `pt`.`id`)) join `seller_profile` `se` on(`pt`.`seller_id` = `se`.`seller_id`)) join `user_profile` `up` on(`pt`.`user_dni` = `up`.`user_dni`)) join `policy` `po` on(`us`.`policy_id` = `po`.`policy_id`)) group by `up`.`user_name`,`us`.`policy_end` order by `contador`,`fecha_accidente` ;

-- --------------------------------------------------------

--
-- Structure for view `SELLER POTFOLIO`
--
DROP TABLE IF EXISTS `SELLER POTFOLIO`;

CREATE ALGORITHM=UNDEFINED DEFINER=`dannybombastic`@`localhost` SQL SECURITY DEFINER VIEW `SELLER POTFOLIO`  AS  select `se`.`user_name` AS `vendedor`,`se`.`user_cif` AS `cif`,`pl`.`policy_subject` AS `entidad`,`pl`.`polici_price` AS `precio`,cast(`pl`.`polici_price` * 5 / 100 as decimal(16,2)) AS `ganancias` from ((((`portfolio` `po` join `seller_profile` `se` on(`po`.`seller_id` = `se`.`seller_id`)) join `user_profile` `us` on(`po`.`user_dni` = `us`.`user_dni`)) join `policy_user` `pu` on(`po`.`id` = `pu`.`portfolio_id`)) join `policy` `pl` on(`pu`.`policy_id` = `pl`.`policy_id`)) where `po`.`seller_id` = '4' ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `car_crash`
--
ALTER TABLE `car_crash`
  ADD PRIMARY KEY (`car_id`),
  ADD KEY `pay_policy` (`pay_policy`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`pay_id`),
  ADD KEY `pay_id` (`pay_id`),
  ADD KEY `pay_policy` (`pay_policy`);

--
-- Indexes for table `policy`
--
ALTER TABLE `policy`
  ADD PRIMARY KEY (`policy_id`),
  ADD KEY `policy_id` (`policy_id`);

--
-- Indexes for table `policy_user`
--
ALTER TABLE `policy_user`
  ADD PRIMARY KEY (`polici_user_id`),
  ADD KEY `policy_id` (`policy_id`),
  ADD KEY `portfolio_id` (`portfolio_id`);

--
-- Indexes for table `portfolio`
--
ALTER TABLE `portfolio`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `seller_id` (`seller_id`),
  ADD KEY `user_dni` (`user_dni`);

--
-- Indexes for table `seller_profile`
--
ALTER TABLE `seller_profile`
  ADD PRIMARY KEY (`seller_id`);

--
-- Indexes for table `user_profile`
--
ALTER TABLE `user_profile`
  ADD PRIMARY KEY (`user_dni`),
  ADD KEY `user_dni` (`user_dni`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `car_crash`
--
ALTER TABLE `car_crash`
  MODIFY `car_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `pay_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `policy`
--
ALTER TABLE `policy`
  MODIFY `policy_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `policy_user`
--
ALTER TABLE `policy_user`
  MODIFY `polici_user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `portfolio`
--
ALTER TABLE `portfolio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `seller_profile`
--
ALTER TABLE `seller_profile`
  MODIFY `seller_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `car_crash`
--
ALTER TABLE `car_crash`
  ADD CONSTRAINT `car_crash_relation` FOREIGN KEY (`pay_policy`) REFERENCES `policy_user` (`polici_user_id`);

--
-- Constraints for table `payments`
--
ALTER TABLE `payments`
  ADD CONSTRAINT `payments_relation` FOREIGN KEY (`pay_policy`) REFERENCES `policy_user` (`polici_user_id`);

--
-- Constraints for table `policy_user`
--
ALTER TABLE `policy_user`
  ADD CONSTRAINT `polici_policy_relation` FOREIGN KEY (`policy_id`) REFERENCES `policy` (`policy_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `policy_relation` FOREIGN KEY (`portfolio_id`) REFERENCES `portfolio` (`id`);

--
-- Constraints for table `portfolio`
--
ALTER TABLE `portfolio`
  ADD CONSTRAINT `seller_relation` FOREIGN KEY (`seller_id`) REFERENCES `seller_profile` (`seller_id`),
  ADD CONSTRAINT `user_relation` FOREIGN KEY (`user_dni`) REFERENCES `user_profile` (`user_dni`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
