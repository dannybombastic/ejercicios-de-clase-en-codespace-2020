-- departments
-- ('d009','Customer Service')

-- dept_emp
-- (10001,'d005','1986-06-26','9999-01-01')

-- dept_manager
-- (110022,'d001','1985-01-01','1991-10-01')

-- employees
-- (10001,'1953-09-02','Georgi','Facello','M','1986-06-26')

-- salaries
-- (10001,60117,'1986-06-26','1987-06-26')

-- titles
-- (10001,'Senior Engineer','1986-06-26','9999-01-01')



-- (10001,'1953-09-02','Georgi','Facello','M','1986-06-26')
CREATE TABLE employees
(
    emp_no INT NOT NULL
    AUTO_INCREMENT,
    birth_date DATE NOT NULL,
    first_name VARCHAR (14),
    last_name VARCHAR  (16),
    gender ENUM   ('M','F'),
    hire_date DATE NOT NULL,
    PRIMARY KEY (emp_no)

);


CREATE TABLE employees_copia
(
    emp_no INT NOT NULL
    AUTO_INCREMENT,
    birth_date DATE NOT NULL,
    first_name VARCHAR (14),
    last_name VARCHAR  (16),
    gender ENUM   ('M','F'),
    hire_date DATE NOT NULL,
    PRIMARY KEY (emp_no)

);


-- ('d009','Customer Service')
CREATE TABLE departments
(
    dept_no CHAR(4) NOT NULL,
    dept_name CHAR(40)

);


-- (10001,'d005','1986-06-26','9999-01-01')
CREATE TABLE dept_emp
(
    emp_no INT NOT NULL,
    dept_no CHAR(4),
    from_date DATE NOT NULL,
    to_date DATE NOT NULL

);



-- (10001,'Senior Engineer','1986-06-26','9999-01-01')
CREATE TABLE titles (
  emp_no INT NOT NULL,
  title VARCHAR (80),
  from_date DATE NOT NULL,
  to_date DATE

);



-- (10001,60117,'1986-06-26','1987-06-26')
CREATE TABLE salaries (
    emp_no INT NOT NULL,
    salary INT NOT NULL,
    from_date DATE NOT NULL,
    to_date DATE NOT NULL
);


-- (110022,'d001','1985-01-01','1991-10-01')
CREATE TABLE dept_manager (
 emp_no INT NOT NULL,
 dept_no CHAR(4),
 from_date DATE NOT NULL,
 to_date DATE NOT NULL

)

SELECT  ep.first_name as nombre 
FROM titles as tt 
LEFT JOIN employees as ep on tt.emp_no = ep.emp_no GROUP BY nombre;

SELECT sa.salary as salario, ep.first_name as nombre
FROM titles as tt
LEFT JOIN employees as ep on tt.emp_no = ep.emp_no
LEFT JOIN salaries as sa on ep.emp_no = sa.emp_no;



SELECT sa.salary as salario, first_name as name FROM employees as ee
JOIN salaries as sa on ee.emp_no = sa.emp_no
ORDER BY salario ASC LIMIT 100;


SELECT * FROM salaries 
JOIN employees ON salaries.employees = employees.emp_no

SELECT * FROM dept_manager AS dep
JOIN employees AS empl ON dep.emp_no = empl.emp_no
JOIN departments AS deps ON dep.dept_no = deps.dept_no;


SELECT * FROM dept_manager AS dep
LEFT JOIN employees AS empl ON dep.emp_no = empl.emp_no;

SELECT * FROM employees AS dep
LEFT JOIN dept_manager AS empl ON dep.emp_no = empl.emp_no;

SELECT COUNT(*) AS cnt, ep.first_name AS nombre  
FROM titles AS tt  
LEFT JOIN employees AS ep on tt.emp_no = ep.emp_no WHERE nombre = 'Zvonko'  GROUP BY nombre;



CREATE DATABASE videoclub;

USE videoclub;

--( titulo, categoria, duracion, nacionalidad, director, puntuacion, pegi )
-- pelicula
CREATE TABLE IF NOT EXISTS  films(

    id INT NOT NULL AUTO_INCREMENT,
    title VARCHAR(50) NOT NULL,
    cate ENUM ('XXX','ACTION','ADVENTURE','SCIFY'),
    duracion INT NOT NULL,
    nacionalidad VARCHAR(40) NOT NULL,
    director  VARCHAR(40),
    puntuacion ENUM ('MALISIMA','MALA','BUENA','BUENISIMA', 'RECOMENDABLE'),
    PRIMARY KEY (id)

); 


INSERT INTO films (title, cate, duracion, nacionalidad, director, puntuacion ) 
VALUES ("Resacon el las Vegas", "ACTION", 120, "FR", "Activision", "MALA");

--( DNI, nombre, apellidos, fecha nacimiento )
-- socio

CREATE TABLE IF NOT EXISTS client (

   dni INT NOT NULL,
   client_name VARCHAR(30) NOT NULL,
   client_surname VARCHAR(30) NOT NULL,
   birth_day DATE NOT NULL,
   PRIMARY KEY (dni)

);

INSERT INTO client (dni, client_name, client_surname, birth_day)
VALUES ('79020209m', 'Pablo', 'Motos', '1987-02-06'); 


--( id, id_cliente, from, to )
-- alquileres

CREATE TABLE IF NOT EXISTS rent (

film_id INT NOT NULL,
client_id INT NOT NULL,
from_date DATE NOT NULL,
to_date DATE NULL

);

INSERT INTO rent (film_id, client_id, from_date, to_date)
VALUES (2, '79020208m', '2020-01-16', '2020-01-25'); 


SELECT *, cl.client_name, cl.dni FROM films AS fl 
LEFT JOIN rent AS rr ON rr.film_id = fl.id
LEFT JOIN client AS cl ON cl.dni = rr.client_id;


SELECT fl.title,  IFNULL(cl.dni, 'dni') AS 'dni cliente', IFNULL(cl.client_name, 'En alquiler') as 'nombre cliente'  FROM films AS fl 
LEFT JOIN rent AS rr ON rr.film_id = fl.id
LEFT JOIN client AS cl ON cl.dni = rr.client_id;


SELECT * FROM rent where  DATEDIFF(CURDATE(), from_date ) > 2;

SELECT * FROM client;
SELECT * FROM films;
SELECT * FROM rent;

--DATE_SUB()
SELECT * FROM rent where DATE_SUB( from_date, INTERVAL 2 DAY );

--DATE_FORMAT(date, format) ej, SELECT DATE_SUB("2017-06-15 09:34:21", INTERVAL 15 MINUTE);
SELECT film_id, client_id, DATE_FORMAT(from_date, '%D %y %a %d %m %b %j') AS de, DATE_FORMAT(to_date, '%D %M %Y') AS 'hasta'  
FROM rent 
where  DATEDIFF(CURDATE(), from_date ) > 2;



--LAST_DAY('2003-02-05');  inner DATE_FORMAT(date, format)
SELECT film_id, client_id, LAST_DAY(DATE_FORMAT(from_date, '%D %M %Y')) AS de, LAST_DAY(to_date) AS 'hasta'  
FROM rent 
where  DATEDIFF(CURDATE(), from_date ) > 2;







CREATE DATABASE dbexample;

USE dbexample;

CREATE TABLE left_table (

    id_left INT NOT NULL AUTO_INCREMENT,
    nombre VARCHAR(24) NOT NULL,
    loqsea VARCHAR(5) NOT NULL,
    desde DATE NOT NULL,
    PRIMARY KEY (id_left)


);

INSERT INTO left_table (nombre, loqsea, desde) VALUES ('Panocho','AADV5','2020-01-15'),('Pancho villa','ADDV5','2020-01-14'),('Pablo Escobar','AKK47','2020-01-19');

CREATE TABLE right_table (

    id_right INT NOT NULL AUTO_INCREMENT,
    info VARCHAR(24) NOT NULL,
    otracosa VARCHAR(5) NOT NULL,
    desde DATE NOT NULL,
    PRIMARY KEY (id_right)


)
INSERT INTO right_table (info, otracosa, desde) VALUES ('Panocha la chamaca','ADV5','2020-01-17'),('Pancha la pandilla','ADV5','2020-01-14'),('Pabla Escobar a barrer','AK47','2020-01-19');


-- MANY TO MANY
CREATE TABLE full_body (

    id_left INT NOT NULL,
    id_right INT NOT NULL,
    desde DATE NOT NULL,
    hasta DATE NULL
)
INSERT INTO full_body  VALUES (1, 7, '2020-01-15', '2020-01-15'),(1, 7, '2020-01-15', '2020-01-15'),(8, 1, '2020-01-15', '2020-01-15');


--car example

CREATE TABLE car (

    id_car INT NOT NULL AUTO_INCREMENT,
    dorrs INT NOT NULL,
    desde DATE NOT NULL,
    PRIMARY KEY(id_car)
);

INSERT INTO car (dorrs, desde) VALUES (4,'2019-11-02'),(2,'2019-11-02');
INSERT INTO car (dorrs, desde) VALUES (2,'2019-11-02');
CREATE TABLE driver (

    id_driver INT NOT NULL AUTO_INCREMENT,
    driver_name VARCHAR(24) NOT NULL,
    license VARCHAR(5) NOT NULL,
    desde DATE NOT NULL,
    PRIMARY KEY (id_driver)
);

INSERT INTO car (driver_name, license, desde) VALUES ('Hector','853ef','2019-11-02'),('Alberto','8w3df','2018-05-07');

-- ONE TO ONE
CREATE TABLE car_user (

    id_car INT NOT NULL,
    id_driver INT NOT NULL,
    desde DATE NOT NULL,
    PRIMARY KEY(id_car)
);

INSERT INTO car_user (id_car, id_driver, desde) VALUES (2, 2, '2020-01-14');




-- 202015-01 DATE_ADD(fieldate, format)
 SELECT MAX(YEAR(hire_date)) from employees;
 UPDATE employees SET hire_date= DATE_ADD(hire_date, INTERVAL 17 YEAR);


 -- example view
 SELECT emp_no, first_name, last_name,DATE_FORMAT(hire_date, '%d/%m/%Y') AS 'hire_date_ES', DATE_FORMAT(DATE_ADD(hire_date, INTERVAL 10 YEAR), '%d/%m/%Y') as 'fecha'
 FROM employees WHERE hire_date >= DATE_SUB(CURDATE(), INTERVAL 5 YEAR);

 -- view views
 SHOW FULL TABLES;
 SHOW CREATE VIEW celebracion;

 INSERT INTO employees_copia ()

 --change column name
 -- ALTER TABLE  table_name CHANGE COLUNM old_field new_field redefinition and options;  

 -- ALTER TABLE name ADD INDEX(ATTRIB_name)





 -- Ejercicio Tienda Virtual #######################################################################################################################

 -- manual add CREATE INDEX product_category_index ON product_category (id_category);


 CREATE TABLE category (

     id_category INT NOT NULL AUTO_INCREMENT,
     name  VARCHAR(25),
     slug VARCHAR(25),
     decription VARCHAR(255), 
     CONSTRAINT categori_pk PRIMARY KEY (id_category)

 );

 
 
 
 CREATE INDEX category_index ON product_category (product_id);


 CREATE TABLE product (

     product_id INT NOT NULL AUTO_INCREMENT,
     name_pr  VARCHAR(25),
     price DECIMAL(15, 2) NOT NULL,
     subtotal  DECIMAL(15, 2) NULL,
     rates  DECIMAL(15, 2) NULL,
     weight_pr  DECIMAL(15, 2) NOT NULL,
     decription VARCHAR(255), 
     CONSTRAINT product_pk PRIMARY KEY (product_id),
     CONSTRAINT product_relation FOREIGN KEY(product_id) REFERENCES product_category(product_id) 
    
 );

 

  
 CREATE TABLE product_category (

     id INT NOT NULL AUTO_INCREMENT,
     product_id INT NOT NULL,
     id_category INT NOT NULL,
     CONSTRAINT product_categori_pk PRIMARY KEY (id),
     CONSTRAINT categori_relation FOREIGN KEY (id_category) REFERENCES category(id_category)


 );

  ALTER TABLE `product_category`
  ADD CONSTRAINT `FK_product_cat` FOREIGN KEY (product_id) REFERENCES produc(product_id) ON DELETE CASCADE ON UPDATE CASCADE;


 CREATE TABLE user_profile (
     
     user_name VARCHAR(24) NOT NULL,
     user_email VARCHAR(68) NOT NULL,
     gender ENUM ('M','F') NOT NULL,
     CONSTRAINT user_pk PRIMARY KEY(user_email),
  
 );

  CREATE INDEX user_or ON user_profile(user_email);

    CREATE TABLE user_addres (

     id INT NOT NULL AUTO_INCREMENT,
     user_email VARCHAR(68) NOT NULL,
     addres VARCHAR(68) NOT NULL,
     CONSTRAINT user_addres_pk PRIMARY KEY (id),
     CONSTRAINT uer_addres_relation FOREIGN KEY(user_email) REFERENCES user_profile(user_email)


 );

 CREATE TABLE user_order(

     order_id INT NOT NULL AUTO_INCREMENT,
     order_num INT NOT NULL,
     succes BOOLEAN  NOT NULL, 
     user_email VARCHAR(68) NOT NULL,
     CONSTRAINT user_email PRIMARY KEY(order_id),
     CONSTRAINT order_relation FOREIGN KEY(user_email) REFERENCES user_profile(user_email) 
  );

 CREATE INDEX order ON user_order(order_num);


 CREATE TABLE trolley_basket (
     
     id INT NOT NULL AUTO_INCREMENT,
     product_id  INT NOT NULL,
     price DECIMAL  UNSIGNED ZEROFILL,
     order_num INT NOT NULL,
     weight_pr DECIMAL(15, 2) NULL,
     subtotal DECIMAL(15, 2) NULL,
     iva DECIMAL(15, 2) NULL,
     total DECIMAL(15, 2) NULL,
     sold BOOLEAN  NOT NULL, 
     CONSTRAINT basket_pk PRIMARY KEY(id),
     CONSTRAINT basket_relation FOREIGN KEY(order_num) REFERENCES user_order(order_num) 

 );

 CREATE INDEX product_index ON trolley_basket(product_id);

 ALTER TABLE `trolley_basket`
  ADD CONSTRAINT `FK_product` FOREIGN KEY (product_id) REFERENCES produc(product_id) ON DELETE CASCADE ON UPDATE CASCADE;


  CREATE TABLE wish_list (

     id INT NOT NULL AUTO_INCREMENT,
     product_id INT NOT NULL,
     user_email VARCHAR(68) NOT NULL,
     CONSTRAINT product_categori_pk PRIMARY KEY (id),
     CONSTRAINT wish_relation FOREIGN KEY (product_id) REFERENCES product(product_id)

 );

 CREATE INDEX wish ON wish_list(user_email);

 ALTER TABLE `wish_list`
  ADD CONSTRAINT `FK_user` FOREIGN KEY (user_email) REFERENCES user_profile (user_email) ON DELETE CASCADE ON UPDATE CASCADE;




-- by client
SELECT uo.user_email AS 'email', SUM(pr.total) AS 'total', SUM(( pr.total - pr.subtotal )) AS 'iva_pagado',  COUNT(*) as numero_articulos
FROM `trolley_basket` AS bk JOIN user_order AS uo ON bk.order_num = uo.order_num JOIN product as pr ON bk.product_id = pr.product_id WHERE bk.sold = TRUE
GROUP BY email; 
 
-- by orden de usuario
SELECT uo.user_email AS 'email', SUM(pr.total) AS 'total', SUM(( (pr.iva/100) * total )) AS 'iva_pagado',  uo.order_num AS order_numero, COUNT(*) as numero_articulos
FROM `trolley_basket` AS bk JOIN user_order AS uo ON bk.order_num = uo.order_num JOIN product as pr ON bk.product_id = pr.product_id WHERE bk.sold = TRUE
GROUP BY order_numero; 


-- how many articles by quantity
SELECT pr.name_pr as producto, SUM(pr.total) AS 'total', SUM(( pr.total - pr.subtotal )) AS 'iva_pagado', COUNT(*) as numero_ventas, COUNT(wl.user_email) as wltime
FROM `trolley_basket` AS bk 
JOIN user_order AS uo ON bk.order_num = uo.order_num 
JOIN product as pr ON bk.product_id = pr.product_id 
LEFT JOIN wish_list as wl ON wl.product_id = pr.product_id 
GROUP BY pr.name_pr 
ORDER BY  numero_ventas DESC; 


-- all products and whislist and how any times are on bascket

SELECT pr.name_pr as producto, COUNT(*) as cantidad, 'ventas' as ventas
FROM `trolley_basket` AS bk 
JOIN user_order AS uo ON bk.order_num = uo.order_num 
JOIN product as pr ON bk.product_id = pr.product_id 
GROUP BY pr.name_pr
UNION 
SELECT pr.name_pr as producto, COUNT(*) as cantidad, 'wish list' as ventas
FROM `wish_list` AS wl 
JOIN product as pr ON pr.product_id = wl.product_id 
WHERE wl.product_id IN (SELECT product_id FROM trolley_basket)
GROUP BY pr.name_pr
ORDER BY ventas;
 


-- all products and whislist and how any times are on bascket from one user 
 SELECT pr.name_pr as producto, COUNT(*) as cantidad, 'ventas' as ventas
FROM `trolley_basket` AS bk 
JOIN user_order AS uo ON bk.order_num = uo.order_num 
JOIN product as pr ON bk.product_id = pr.product_id 
WHERE uo.user_email = 'Dannybombastic@gmail.com'
GROUP BY pr.name_pr
UNION 
SELECT pr.name_pr as producto, COUNT(*) as cantidad, 'wish list' as ventas
FROM `wish_list` AS wl 
JOIN product as pr ON pr.product_id = wl.product_id 
WHERE wl.product_id IN (SELECT product_id FROM trolley_basket) AND wl.user_email = 'Dannybombastic@gmail.com'
GROUP BY pr.name_pr
ORDER BY ventas;
 


-- case D last order by user 
 SELECT ur.user_name, ur.user_email, ur.gender, uo.order_num, uo.succes , pr.name_pr, pr.total
 FROM user_profile AS ur 
 JOIN user_order AS uo ON uo.user_email = ur.user_email 
 JOIN trolley_basket AS tr ON tr.order_num = uo.order_num
 JOIN product AS pr ON pr.product_id = tr.product_id 
 WHERE ur.user_email = 'Dannybombastic@gmail.com' 
 ORDER BY uo.order_num DESC LIMIT 1;
