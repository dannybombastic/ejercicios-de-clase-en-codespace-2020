-- MySQL dump 10.17  Distrib 10.3.20-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: insurance
-- ------------------------------------------------------
-- Server version	10.3.20-MariaDB-0ubuntu0.19.10.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `car_crash`
--

DROP TABLE IF EXISTS `car_crash`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `car_crash` (
  `car_id` int(11) NOT NULL AUTO_INCREMENT,
  `pay_policy` int(11) NOT NULL,
  `car_state` enum('BROKEN','FIXED') NOT NULL,
  `car_date` date NOT NULL,
  PRIMARY KEY (`car_id`),
  KEY `pay_policy` (`pay_policy`),
  CONSTRAINT `car_crash_relation` FOREIGN KEY (`pay_policy`) REFERENCES `policy_user` (`polici_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `car_crash`
--

LOCK TABLES `car_crash` WRITE;
/*!40000 ALTER TABLE `car_crash` DISABLE KEYS */;
/*!40000 ALTER TABLE `car_crash` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payments`
--

DROP TABLE IF EXISTS `payments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payments` (
  `pay_id` int(11) NOT NULL AUTO_INCREMENT,
  `pay_policy` int(11) NOT NULL,
  `pay_amount` decimal(15,2) DEFAULT NULL,
  `pay_state` enum('PENDING','REJECT','INPROGRESS','FINISH') NOT NULL,
  `pay_date` date NOT NULL,
  PRIMARY KEY (`pay_id`),
  KEY `pay_id` (`pay_id`),
  KEY `pay_policy` (`pay_policy`),
  CONSTRAINT `payments_relation` FOREIGN KEY (`pay_policy`) REFERENCES `policy_user` (`polici_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payments`
--

LOCK TABLES `payments` WRITE;
/*!40000 ALTER TABLE `payments` DISABLE KEYS */;
/*!40000 ALTER TABLE `payments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `policy`
--

DROP TABLE IF EXISTS `policy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `policy` (
  `policy_id` int(11) NOT NULL AUTO_INCREMENT,
  `polici_price` decimal(15,2) DEFAULT NULL,
  `policy_subject` varchar(24) NOT NULL,
  `policy_start` date NOT NULL,
  `policy_end` date NOT NULL,
  `policy_gateway` enum('TRANSFER','CARD','BANKACCOUNT') NOT NULL,
  PRIMARY KEY (`policy_id`),
  KEY `policy_id` (`policy_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `policy`
--

LOCK TABLES `policy` WRITE;
/*!40000 ALTER TABLE `policy` DISABLE KEYS */;
/*!40000 ALTER TABLE `policy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `policy_user`
--

DROP TABLE IF EXISTS `policy_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `policy_user` (
  `polici_user_id` int(11) NOT NULL AUTO_INCREMENT,
  `policy_id` int(11) NOT NULL,
  `portfolio_id` int(11) NOT NULL,
  PRIMARY KEY (`polici_user_id`),
  KEY `policy_id` (`policy_id`),
  KEY `portfolio_id` (`portfolio_id`),
  CONSTRAINT `polici_policy_relation` FOREIGN KEY (`policy_id`) REFERENCES `policy` (`policy_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `policy_relation` FOREIGN KEY (`portfolio_id`) REFERENCES `portfolio` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `policy_user`
--

LOCK TABLES `policy_user` WRITE;
/*!40000 ALTER TABLE `policy_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `policy_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `portfolio`
--

DROP TABLE IF EXISTS `portfolio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `portfolio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `seller_id` int(11) NOT NULL,
  `user_dni` varchar(24) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `seller_id` (`seller_id`),
  KEY `user_dni` (`user_dni`),
  CONSTRAINT `seller_relation` FOREIGN KEY (`seller_id`) REFERENCES `seller_profile` (`seller_id`),
  CONSTRAINT `user_relation` FOREIGN KEY (`user_dni`) REFERENCES `user_profile` (`user_dni`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `portfolio`
--

LOCK TABLES `portfolio` WRITE;
/*!40000 ALTER TABLE `portfolio` DISABLE KEYS */;
/*!40000 ALTER TABLE `portfolio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seller_profile`
--

DROP TABLE IF EXISTS `seller_profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seller_profile` (
  `seller_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_cif` varchar(24) NOT NULL,
  `user_name` varchar(24) NOT NULL,
  `user_addr` varchar(60) NOT NULL,
  `gender` enum('M','F') NOT NULL,
  PRIMARY KEY (`seller_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seller_profile`
--

LOCK TABLES `seller_profile` WRITE;
/*!40000 ALTER TABLE `seller_profile` DISABLE KEYS */;
/*!40000 ALTER TABLE `seller_profile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_profile`
--

DROP TABLE IF EXISTS `user_profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_profile` (
  `user_dni` varchar(24) NOT NULL,
  `user_name` varchar(24) NOT NULL,
  `user_addr` varchar(60) NOT NULL,
  `gender` enum('M','F') NOT NULL,
  PRIMARY KEY (`user_dni`),
  KEY `user_dni` (`user_dni`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_profile`
--

LOCK TABLES `user_profile` WRITE;
/*!40000 ALTER TABLE `user_profile` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_profile` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-01-24  0:17:23
