-- MariaDB dump 10.17  Distrib 10.4.6-MariaDB, for Win64 (AMD64)
--
-- Host: localhost    Database: onlineshop
-- ------------------------------------------------------
-- Server version	10.4.6-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `id_category` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) DEFAULT NULL,
  `slug` varchar(25) DEFAULT NULL,
  `decription` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_category`),
  KEY `category_inde` (`id_category`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,'jugetes','juegetes','Juguetes para niños'),(2,'Deporte','Deporte','Articulos deportivos'),(3,'Comida sana','comida-sana','Articulos de comida baja en calorias'),(4,'Ocio Entretenimiento','Ocio-Entretenimiento','Articulos para el entretenimiento');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `name_pr` varchar(25) NOT NULL,
  `price` decimal(15,2) NOT NULL,
  `subtotal` decimal(15,2) NOT NULL,
  `iva` decimal(15,2) DEFAULT NULL,
  `weight_pr` decimal(15,2) DEFAULT NULL,
  `decription` varchar(255) DEFAULT NULL,
  `total` decimal(15,2) NOT NULL,
  PRIMARY KEY (`product_id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (1,'Bicicleta',125.70,120.50,8.20,14.00,'Bicicleta de montaña',127.70),(2,'Netflix',34.50,25.00,8.20,0.50,'Tarjeta Nextflix 1 año',35.50),(3,'Muñeco Batman',31.00,29.50,8.60,0.70,'Muñeco de batman DC',32.00),(4,'Pure de calabazin',4.50,2.30,8.20,0.50,'Creama de calabazin',5.00);
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_category`
--

DROP TABLE IF EXISTS `product_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `id_category` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `categori_relation` (`id_category`),
  KEY `product_index` (`product_id`),
  CONSTRAINT `categori_relation` FOREIGN KEY (`id_category`) REFERENCES `category` (`id_category`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `product_relation_pr` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_category`
--

LOCK TABLES `product_category` WRITE;
/*!40000 ALTER TABLE `product_category` DISABLE KEYS */;
INSERT INTO `product_category` VALUES (1,1,2),(2,3,1),(3,3,2),(4,2,4),(5,4,3);
/*!40000 ALTER TABLE `product_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `total_by_client`
--

DROP TABLE IF EXISTS `total_by_client`;
/*!50001 DROP VIEW IF EXISTS `total_by_client`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `total_by_client` (
  `email` tinyint NOT NULL,
  `total` tinyint NOT NULL,
  `iva_pagado` tinyint NOT NULL,
  `succes` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `trolley_basket`
--

DROP TABLE IF EXISTS `trolley_basket`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trolley_basket` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `sold` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `basket_relation` (`order_id`),
  KEY `product_id` (`product_id`),
  CONSTRAINT `basket_relation` FOREIGN KEY (`order_id`) REFERENCES `user_order` (`order_id`),
  CONSTRAINT `product_relation_basket` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trolley_basket`
--

LOCK TABLES `trolley_basket` WRITE;
/*!40000 ALTER TABLE `trolley_basket` DISABLE KEYS */;
INSERT INTO `trolley_basket` VALUES (1,1,1,1),(2,3,1,1),(3,2,2,1),(4,4,2,1),(5,1,2,1),(6,4,1,1);
/*!40000 ALTER TABLE `trolley_basket` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_addres`
--

DROP TABLE IF EXISTS `user_addres`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_addres` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_email` varchar(68) NOT NULL,
  `addres` varchar(68) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `uer_addres_relation` (`user_email`),
  CONSTRAINT `uer_addres_relation` FOREIGN KEY (`user_email`) REFERENCES `user_profile` (`user_email`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_addres`
--

LOCK TABLES `user_addres` WRITE;
/*!40000 ALTER TABLE `user_addres` DISABLE KEYS */;
INSERT INTO `user_addres` VALUES (1,'dannybombastic@gmail.com','juan ramon jimenez n3 5 m'),(2,'pabloescobar@gmail.com','Estado de guerrero Acapulco, las brisas'),(3,'dannybombastic@gmail.com','Estado de Puebla, San Adres Cholula 5th poniente'),(4,'panocha@gmail.com','Veracruz, la parroquia n 10');
/*!40000 ALTER TABLE `user_addres` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_order`
--

DROP TABLE IF EXISTS `user_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_order` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_date` date NOT NULL,
  `succes` tinyint(1) NOT NULL,
  `user_email` varchar(68) NOT NULL,
  PRIMARY KEY (`order_id`),
  KEY `order_relation` (`user_email`),
  CONSTRAINT `order_relation` FOREIGN KEY (`user_email`) REFERENCES `user_profile` (`user_email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_order`
--

LOCK TABLES `user_order` WRITE;
/*!40000 ALTER TABLE `user_order` DISABLE KEYS */;
INSERT INTO `user_order` VALUES (1,'2020-01-18',1,'dannybombastic@gmail.com'),(2,'2020-01-16',0,'pabloescobar@gmail.com');
/*!40000 ALTER TABLE `user_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_profile`
--

DROP TABLE IF EXISTS `user_profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_profile` (
  `user_name` varchar(24) NOT NULL,
  `user_email` varchar(68) NOT NULL,
  `gender` enum('M','F') NOT NULL,
  PRIMARY KEY (`user_email`),
  KEY `product` (`user_email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_profile`
--

LOCK TABLES `user_profile` WRITE;
/*!40000 ALTER TABLE `user_profile` DISABLE KEYS */;
INSERT INTO `user_profile` VALUES ('Daniel','dannybombastic@gmail.com','M'),('Pablo Escobar','pabloescobar@gmail.com','M'),('Panocha morales','panocha@gmail.com','F');
/*!40000 ALTER TABLE `user_profile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `ventas_by_product`
--

DROP TABLE IF EXISTS `ventas_by_product`;
/*!50001 DROP VIEW IF EXISTS `ventas_by_product`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `ventas_by_product` (
  `user_email` tinyint NOT NULL,
  `name_pr` tinyint NOT NULL,
  `total` tinyint NOT NULL,
  `iva` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `wish_list`
--

DROP TABLE IF EXISTS `wish_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wish_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `user_email` varchar(68) CHARACTER SET utf8mb4 NOT NULL,
  PRIMARY KEY (`id`),
  KEY `product` (`user_email`),
  KEY `wish_relation` (`product_id`),
  KEY `user_email` (`user_email`),
  CONSTRAINT `whis_relation_user` FOREIGN KEY (`user_email`) REFERENCES `user_profile` (`user_email`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `wish_relation` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wish_list`
--

LOCK TABLES `wish_list` WRITE;
/*!40000 ALTER TABLE `wish_list` DISABLE KEYS */;
INSERT INTO `wish_list` VALUES (1,1,'dannybombastic@gmail.com'),(2,4,'pabloescobar@gmail.com'),(3,3,'pabloescobar@gmail.com');
/*!40000 ALTER TABLE `wish_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Final view structure for view `total_by_client`
--

/*!50001 DROP TABLE IF EXISTS `total_by_client`*/;
/*!50001 DROP VIEW IF EXISTS `total_by_client`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_unicode_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `total_by_client` AS select `uo`.`user_email` AS `email`,sum(`pr`.`total`) AS `total`,sum(`pr`.`total` - `pr`.`subtotal`) AS `iva_pagado`,`uo`.`succes` AS `succes` from ((`trolley_basket` `bk` join `user_order` `uo` on(`bk`.`order_id` = `uo`.`order_id`)) join `product` `pr` on(`bk`.`product_id` = `pr`.`product_id`)) where `bk`.`sold` = 1 and `uo`.`succes` = 1 group by `uo`.`user_email` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `ventas_by_product`
--

/*!50001 DROP TABLE IF EXISTS `ventas_by_product`*/;
/*!50001 DROP VIEW IF EXISTS `ventas_by_product`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_unicode_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `ventas_by_product` AS select `uo`.`user_email` AS `user_email`,`pr`.`name_pr` AS `name_pr`,`pr`.`total` AS `total`,`pr`.`iva` AS `iva` from ((`trolley_basket` `bk` join `user_order` `uo` on(`bk`.`order_id` = `uo`.`order_id`)) join `product` `pr` on(`bk`.`product_id` = `pr`.`product_id`)) where `bk`.`sold` = 1 */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-01-16 17:22:18
