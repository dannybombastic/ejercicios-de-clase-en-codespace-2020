CREATE DATABASE shoponline CHARACTER SET = 'utf8' COLLATE = 'utf8_general_ci';

CREATE TABLE user_profile (
  user_dni VARCHAR(24) NOT NULL,
  user_name VARCHAR(24) NOT NULL,
  user_addr VARCHAR(60) NOT NULL,
  user_email VARCHAR(60) NOT NULL,
  user_password BINARY NOT NULL,
  gender ENUM ('M', 'F') NOT NULL,
  create_at DATETIME NOT NULL,
  is_active TINYINT NOT NULL DEFAULT 0,
  CONSTRAINT user_pk PRIMARY KEY(user_dni),
  KEY(user_dni)
);

CREATE TABLE articulos (
  id INT NOT NULL AUTO_INCREMENT,
  art_title VARCHAR(80) NOT NULL,
  art_desc VARCHAR(400) NOT NULL,
  art_price VARCHAR(60) NOT NULL,
  art_evalu CHAR(5) NOT NULL,
  art_sku INT NOT NULL DEFAULT 0,
  id_cat INT NOT NULL DEFAULT 0,
  create_at DATETIME NOT NULL,
  is_active TINYINT NOT NULL DEFAULT 0,
  KEY(id_cat),
  PRIMARY KEY(id)
);

CREATE TABLE trolleybascket (
  id INT NOT NULL AUTO_INCREMENT,
  id_art INT NOT NULL,
  quantity INT NOT NULL,
  price VARCHAR(60) NOT NULL,
  id_user INT NOT NULL,
  create_at DATETIME NOT NULL,
  KEY(id_user),
  PRIMARY KEY(id)
);

CREATE TABLE categories (
  id INT NOT NULL AUTO_INCREMENT,
  name VARCHAR(80) NOT NULL,
  is_active TINYINT NOT NULL DEFAULT 0,
  PRIMARY KEY (id)
) CREATE TABLE comments (
  id INT NOT NULL AUTO_INCREMENT,
  art_comment VARCHAR(250) NOT NULL,
  id_art NOT NULL,
  id_user NOT NULL,
  is_active TINYINT NOT NULL DEFAULT 0,
  KEY(id_art),
  KEY(id_user),
  PRIMARY KEY(id)
);

CREATE TABLE multimedia (
  id INT NOT NULL AUTO_INCREMENT,
  id_art INT NOT NULL,
  id_tipo INT NOT NULL,
  is_active TINYINT NOT NULL DEFAULT 0,
  PRIMARY KEY(id),
  KEY(id_art),
  KEY(id_tipo)
  );

CREATE TABLE multimedia_type (
  id INT NOT NULL AUTO_INCREMENT,
  m_type VARCHAR(80),
  PRIMARY KEY(id)
)

CREATE TABLE seller_profile (
  seller_id INT NOT NULL AUTO_INCREMENT,
  user_cif VARCHAR(24) NOT NULL,
  user_name VARCHAR(24) NOT NULL,
  user_addr VARCHAR(60) NOT NULL,
  gender ENUM ('M', 'F') NOT NULL,
  CONSTRAINT user_pk PRIMARY KEY(seller_id)
);

CREATE TABLE portfolio (
  id INT NOT NULL AUTO_INCREMENT,
  seller_id INT NOT NULL,
  user_dni VARCHAR(24) NOT NULL,
  PRIMARY KEY (id),
  KEY(id),
  KEY(seller_id),
  KEY(user_dni),
  CONSTRAINT seller_relation FOREIGN KEY (seller_id) REFERENCES seller_profile(seller_id),
);

-- ALTER TABLE `portfolio` ADD UNIQUE(`seller_id`);
ALTER TABLE portfolio ADD   CONSTRAINT user_relation FOREIGN KEY (user_dni) REFERENCES user_profile (user_dni) ON DELETE CASCADE ON UPDATE CASCADE;

CREATE INDEX user_id ON portfolio(user_dni);

CREATE TABLE policy (
  policy_id INT NOT NULL AUTO_INCREMENT,
  policy_price DECIMAL(15, 2) NULL,
  policy_subject VARCHAR(24) NOT NULL,
  PRIMARY KEY (policy_id),
  KEY(policy_id)
);

CREATE TABLE policy_user (
  policy_user_id INT NOT NULL AUTO_INCREMENT,
  policy_id INT NOT NULL,
  portfolio_id INT NOT NULL,
  policy_gateway ENUM ('TRANSFER', 'CARD', 'BANKACCOUNT') NOT NULL,
  policy_start DATE NOT NULL,
  policy_end DATE NOT NULL,
  PRIMARY KEY(polici_user_id),
  KEY(policy_id),
  KEY(portfolio_id),
  CONSTRAINT policy_relation FOREIGN KEY (portfolio_id) REFERENCES portfolio(id)
);

ALTER TABLE
  policy_user
ADD
  CONSTRAINT polici_policy_relation FOREIGN KEY(policy_id) REFERENCES policy(policy_id) ON DELETE CASCADE ON UPDATE CASCADE;

CREATE TABLE payments (
  pay_id INT NOT NULL AUTO_INCREMENT,
  pay_policy INT NOT NULL,
  pay_state ENUM ('PENDING', 'REJECT', 'INPROGRESS', 'FINISH') NOT NULL,
  pay_date DATE NOT NULL,
  PRIMARY KEY (pay_id),
  KEY(pay_id),
  KEY(pay_policy),
  CONSTRAINT payments_relation FOREIGN KEY (pay_policy) REFERENCES policy_user(policy_user_id)
);

CREATE TABLE car_crash (
  car_id INT NOT NULL AUTO_INCREMENT,
  pay_policy INT NOT NULL,
  car_state ENUM ('BROKEN', 'FIXED') NOT NULL,
  car_date DATE NOT NULL,
  PRIMARY KEY (car_id),
  KEY(pay_policy),
  CONSTRAINT car_crash_relation FOREIGN KEY (pay_policy) REFERENCES policy_user(policy_user_id)
);

-- by user
SELECT
  se.user_cif as cif,
  se.user_name as comercial,
  po.policy_subject as entida,
  po.polici_price,
  up.user_dni as dni,
  up.user_name as nombre
FROM
  policy_user as us
  JOIN portfolio as pt on us.portfolio_id = pt.id
  JOIN seller_profile as se on pt.seller_id = se.seller_id
  JOIN user_profile as up on pt.user_dni = up.user_dni
  JOIN policy as po on us.policy_id = po.policy_id
WHERE
  up.user_name = 'Nebula Tanos';

-- 	OLD POLICY AND NOT RENEW
SELECT
  se.user_cif as cif,
  se.user_name as comercial,
  po.policy_subject as entida,
  po.polici_price,
  up.user_dni as dni,
  up.user_name as nombre,
  us.policy_end as fecha
FROM
  policy_user as us
  JOIN portfolio as pt on us.portfolio_id = pt.id
  JOIN seller_profile as se on pt.seller_id = se.seller_id
  JOIN user_profile as up on pt.user_dni = up.user_dni
  JOIN policy as po on us.policy_id = po.policy_id
WHERE
  us.policy_end < NOW();

-- ALL CRASH REPORT BY MONTH
SELECT
  MONTHNAME(cr.car_date) as mes_accidente,
  cr.car_date as fecha_accidente,
  cr.car_state as estado,
  se.user_cif as cif,
  se.user_name as comercial,
  po.policy_subject as entida,
  po.polici_price,
  up.user_dni as dni,
  up.user_name as nombre,
  us.policy_end as fecha
FROM
  policy_user as us
  JOIN car_crash as cr on us.polici_user_id = cr.pay_policy
  JOIN portfolio as pt on us.portfolio_id = pt.id
  JOIN seller_profile as se on pt.seller_id = se.seller_id
  JOIN user_profile as up on pt.user_dni = up.user_dni
  JOIN policy as po on us.policy_id = po.policy_id;

-- HISTORIC FROM ONE POLICY
SELECT
  MONTHNAME(cr.car_date) as mes_accidente,
  cr.car_date as fecha_accidente,
  cr.car_state as estado,
  se.user_cif as cif,
  se.user_name as comercial,
  po.policy_subject as entida,
  po.polici_price,
  up.user_dni as dni,
  up.user_name as nombre,
  us.policy_end as fecha
FROM
  policy_user as us
  JOIN car_crash as cr on us.polici_user_id = cr.pay_policy
  JOIN portfolio as pt on us.portfolio_id = pt.id
  JOIN seller_profile as se on pt.seller_id = se.seller_id
  JOIN user_profile as up on pt.user_dni = up.user_dni
  JOIN policy as po on us.policy_id = po.policy_id
WHERE
  us.polici_user_id = '2'
ORDER BY
  fecha_accidente ASC;

-- - Obtener el listado de los 10 clientes que menos siniestros han tenido.
SELECT
  count(*) as contador,
  MONTHNAME(cr.car_date) as mes_accidente,
  cr.car_date as fecha_accidente,
  cr.car_state as estado,
  se.user_cif as cif,
  se.user_name as comercial,
  po.policy_subject as entida,
  po.polici_price,
  up.user_dni as dni,
  up.user_name as nombre,
  us.policy_end as fecha
FROM
  policy_user as us
  JOIN car_crash as cr on us.polici_user_id = cr.pay_policy
  JOIN portfolio as pt on us.portfolio_id = pt.id
  JOIN seller_profile as se on pt.seller_id = se.seller_id
  JOIN user_profile as up on pt.user_dni = up.user_dni
  JOIN policy as po on us.policy_id = po.policy_id
GROUP BY
  nombre
ORDER BY
  contador ASC
LIMIT
  10;

-- Obtener el listado de los 10 clientes que mas siniestros han tenido.
SELECT
  count(*) as contador,
  MONTHNAME(us.policy_start) as mes_accidente,
  us.policy_start as fecha_accidente,
  se.user_cif as cif,
  se.user_name as comercial,
  po.policy_subject as entida,
  po.polici_price,
  up.user_dni as dni,
  up.user_name as nombre,
  us.policy_end as fecha
FROM
  policy_user as us
  JOIN portfolio as pt on us.portfolio_id NOT IN (
    SELECT
      pay_policy
    FROM
      car_crash
  )
  JOIN seller_profile as se on pt.seller_id = se.seller_id
  JOIN user_profile as up on pt.user_dni = up.user_dni
  JOIN policy as po on us.policy_id = us.policy_id
GROUP BY
  nombre
ORDER BY
  contador DESC
LIMIT
  10;

-- Obtener toda la cartera de un comercial, calculando el total de las pólizas generadas. Un comercial comisiona el 5% de la póliza.
SELECT
  se.user_name as vendedor,
  se.user_cif as cif,
  pl.policy_subject as entidad,
  pl.polici_price as precio,
  CAST(
    ((pl.polici_price * 5) / 100) AS DECIMAL(16, 2)
  ) as ganancias
FROM
  portfolio as po
  JOIN seller_profile as se ON po.seller_id = se.seller_id
  JOIN user_profile as us ON po.user_dni = us.user_dni
  JOIN policy_user as pu ON po.id = pu.portfolio_id
  JOIN policy as pl ON pu.policy_id = pl.policy_id
WHERE
  po.seller_id = '4';

SELECT
  count(*) as contador,
  IF(
    YEAR(cr.car_date) IS NULL,
    'vacio',
    YEAR(cr.car_date)
  ) as ano_accidente,
  IF(
    cr.car_date IS NULL,
    'vacio',
    cr.car_date
  ) as fecha_accidente,
  IF(
    cr.car_state IS NULL,
    'vacio',
    cr.car_state
  ) as estado,
  se.user_cif as cif,
  se.user_name as comercial,
  po.policy_subject as entida,
  po.polici_price,
  up.user_dni as dni,
  up.user_name as nombre,
  us.policy_end as fecha,
  IF(
    CAST(
      (
        (
          po.polici_price * IF(
            count(*) > 0,
            15,
            -7
          )
        ) / 100
      ) AS DECIMAL(16, 2)
    ) < 0,
    'un parte por año %',
    CAST(
      (
        (
          po.polici_price * IF(
            count(*) > 0,
            15,
            -7
          )
        ) / 100
      ) AS DECIMAL(16, 2)
    )
  ) as penalizacion,
  po.polici_price + Cast(
    (
      (
        po.polici_price * IF(
          Count(*) > 0,
          15,
          -7
        )
      ) / 100
    ) AS DECIMAL(16, 2)
  ) AS total
FROM
  policy_user as us
  LEFT JOIN car_crash as cr on us.polici_user_id = cr.pay_policy
  LEFT JOIN portfolio as pt on us.portfolio_id = pt.id
  LEFT JOIN seller_profile as se on pt.seller_id = se.seller_id
  LEFT JOIN user_profile as up on pt.user_dni = up.user_dni
  JOIN policy as po on us.policy_id = po.policy_id
GROUP BY
  nombre,
  ano_accidente
ORDER by
  contador DESC;

-- Para renovar una póliza se aplica el siguiente criterios:
-- Si esa póliza no ha tenido un siniestro en un año: -7% del valor actual
-- Si esa póliza no ha tenido un siniestro en en 2 años: -20% del valor actual.
-- Si ha tenido más de 1 siniestros en un año: +15% 
-- Generar la vista que permite calcular el precio de renovación de una póliza
SELECT
  Count(*) AS contador,
  Year(cr.car_date) AS ano_accidente,
  cr.car_date AS fecha_accidente,
  cr.car_state AS estado,
  se.user_cif AS cif,
  se.user_name AS comercial,
  po.policy_subject AS entida,
  po.polici_price,
  up.user_dni AS dni,
  up.user_name AS nombre,
  us.policy_end AS fecha_fin,
  Cast(
    (
      (
        po.polici_price * IF(
          Count(*) > 1,
          15,
          '0'
        )
      ) / 100
    ) AS DECIMAL(16, 2)
  ) AS penalizacion,
  po.polici_price + Cast(
    (
      (
        po.polici_price * IF(
          Count(*) > 1,
          15,
          '0'
        )
      ) / 100
    ) AS DECIMAL(16, 2)
  ) AS total,
  IF(
    Count(*) > 1,
    "mas de un parte por año",
    "un parte por año"
  ) AS tipo
FROM
  policy_user AS us
  JOIN car_crash AS cr ON us.polici_user_id = cr.pay_policy
  JOIN portfolio AS pt ON us.portfolio_id = pt.id
  JOIN seller_profile AS se ON pt.seller_id = se.seller_id
  JOIN user_profile AS up ON pt.user_dni = up.user_dni
  JOIN policy AS po ON us.policy_id = po.policy_id
GROUP BY
  nombre,
  fecha_fin
UNION
SELECT
  IF(
    (YEAR(NOW()) - YEAR(us.policy_start)) > 0,
    "Un año o mas",
    "Su primer año"
  ) AS contador,
  ('NO ACCIDENT') AS ano_accidente,
  ('NO ACCIDENT') AS fecha_accidente,
  ('NO STATE') AS estado,
  se.user_cif AS cif,
  se.user_name AS comercial,
  po.policy_subject AS entida,
  po.polici_price,
  up.user_dni AS dni,
  up.user_name AS nombre,
  us.policy_end AS fecha_fin,
  Cast(
    (
      (
        po.polici_price * IF(
          (YEAR(NOW()) - YEAR(us.policy_start)) > 1,
          -20,
          -7
        )
      ) / 100
    ) AS DECIMAL (16, 2)
  ) AS penalizacion,
  Cast(
    (
      po.polici_price + (
        po.polici_price * IF(
          (YEAR(NOW()) - YEAR(us.policy_start)) > 1,
          -20,
          -7
        ) / 100
      )
    ) AS DECIMAL(16, 2)
  ) AS total,
  IF(
    (YEAR(NOW()) - YEAR(us.policy_start)) > 1,
    "mas de dos años sin partes",
    "un año sin partes"
  ) AS tipo
FROM
  policy_user AS us
  JOIN car_crash as cr ON us.polici_user_id NOT IN (
    SELECT
      pay_policy
    FROM
      car_crash
  )
  JOIN portfolio AS pt ON us.portfolio_id = pt.id
  JOIN seller_profile AS se ON pt.seller_id = se.seller_id
  JOIN user_profile AS up ON pt.user_dni = up.user_dni
  JOIN policy AS po ON us.policy_id = po.policy_id
GROUP BY
  nombre,
  fecha_fin
ORDER BY
  contador,
  fecha_accidente ASC;

SELECT
  Count(*) AS contador,
  Year(cr.car_date) AS ano_accidente,
  cr.car_date AS fecha_accidente,
  cr.car_state AS estado,
  se.user_cif AS cif,
  se.user_name AS comercial,
  po.policy_subject AS entida,
  po.polici_price,
  up.user_dni AS dni,
  up.user_name AS nombre,
  us.policy_end AS fecha_fin,
  Cast(
    (
      (
        po.polici_price * IF(
          Count(*) > 1,
          15,
          '0'
        )
      ) / 100
    ) AS DECIMAL(16, 2)
  ) AS penalizacion,
  po.polici_price + Cast(
    (
      (
        po.polici_price * IF(
          Count(*) > 1,
          15,
          '0'
        )
      ) / 100
    ) AS DECIMAL(16, 2)
  ) AS total,
  IF(
    Count(*) > 1,
    "mas de un parte por año",
    "un parte por año"
  ) AS tipo
FROM
  policy_user AS us
  JOIN car_crash AS cr ON us.polici_user_id = cr.pay_policy
  JOIN portfolio AS pt ON us.portfolio_id = pt.id
  JOIN seller_profile AS se ON pt.seller_id = se.seller_id
  JOIN user_profile AS up ON pt.user_dni = up.user_dni
  JOIN policy AS po ON us.policy_id = po.policy_id
GROUP BY
  nombre,
  fecha_fin
UNION
SELECT
  IF(
    (YEAR(NOW()) - YEAR(us.policy_start)) > 0,
    "Un año o mas",
    "Su primer año"
  ) AS contador,
  ('NO ACCIDENT') AS ano_accidente,
  ('NO ACCIDENT') AS fecha_accidente,
  ('NO STATE') AS estado,
  se.user_cif AS cif,
  se.user_name AS comercial,
  po.policy_subject AS entida,
  po.polici_price,
  up.user_dni AS dni,
  up.user_name AS nombre,
  us.policy_end AS fecha_fin,
  Cast(
    (
      (
        po.polici_price * IF(
          (YEAR(NOW()) - YEAR(us.policy_start)) > 1,
          -20,
          -7
        )
      ) / 100
    ) AS DECIMAL (16, 2)
  ) AS penalizacion,
  Cast(
    (
      po.polici_price + (
        po.polici_price * IF(
          (YEAR(NOW()) - YEAR(us.policy_start)) > 1,
          -20,
          -7
        ) / 100
      )
    ) AS DECIMAL(16, 2)
  ) AS total,
  IF(
    (YEAR(NOW()) - YEAR(us.policy_start)) > 1,
    "mas de dos años sin partes",
    "un año sin partes"
  ) AS tipo
FROM
  policy_user AS us
  JOIN car_crash as cr ON us.polici_user_id NOT IN (
    SELECT
      pay_policy
    FROM
      car_crash
  )
  JOIN portfolio AS pt ON us.portfolio_id = pt.id
  JOIN seller_profile AS se ON pt.seller_id = se.seller_id
  JOIN user_profile AS up ON pt.user_dni = up.user_dni
  JOIN policy AS po ON us.policy_id = po.policy_id
GROUP BY
  nombre,
  fecha_fin
ORDER BY
  fecha_accidente ASC;