<?php 

    // Utilizaremos el modificar private para denegar el  acceso directo a las propiedades de la clase. Para poder asignar
    // valores a las propiedades privadas hay 2 formas:
    // 1. Por del constructor pasando parametros.
    // 2. Por un metodo con parametros de entrada.
    class Cliente
    {
        #region Constantes
        const PAIS ="España";
        #endregion

        #region Propiedades
        private $Nombre;
        private $Apellidos;
        private $FechaNacimiento;
        #endregion

        #region Constructor    

        function __construct($nombre="a", $apellidos="",$fechaNacimiento=""){
            $this->Nombre = $nombre;
            $this->Apellidos = $apellidos;
            $this->FechaNacimiento = $fechaNacimiento;
        }
        #endregion

        #region Funciones

        function ObtenerNombreCompleto(){
            return  $this->Nombre." ".$this->Apellidos." ".$this->FechaNacimiento;
        }

        #endregion
        
        #region Metodos

        function MostrarMensaje($msg){
            echo "Cliente -> MostrarMensaje: ".$msg.PHP_EOL;
        }

        function DatosPersonales(){
            
        }

        #endregion
    }

    #region Creamos instacias y asignamos valores a las propiedades
        $jose = new Cliente("Jose","Gonzalez",1977);
        

        $pablo= new Cliente("Pablo","Apellidos",1955);
        
    #endregion

    #region Visualizamos las propiedades de los objetos.
        echo "Objecto Jose".PHP_EOL;
        echo "------------".PHP_EOL;
        echo $jose->ObtenerNombreCompleto().PHP_EOL;
        echo $jose::PAIS.PHP_EOL;

        echo "".PHP_EOL;

        echo "Objecto Pablo".PHP_EOL;
        echo "-------------".PHP_EOL;
        echo $pablo->ObtenerNombreCompleto().PHP_EOL;
        echo $pablo::PAIS.PHP_EOL;
        
    #endregion

?>
