<?php 

$dbPwd = "";
$dbUser= "root";
$dbServer = "localhost";
$dbName = "test";

$connection = new mysqli($dbServer,$dbUser,$dbPwd, $dbName);
//print_r($connection);

if($connection->connect_errno){
    // Exit para la ejecución de los script de php y muestra el mensaje de error
    exit( "Database Connection Failed. Reason: " . $connection->connect_error);
}


$query="SELECT idCoches, Marca FROM coches WHERE idCoches = ? or idCoches = ? or idCoches = ?";
$result = $connection->prepare($query);

// https://www.php.net/manual/en/mysqli-stmt.bind-param.php
// bind_param("sdi", $stringVar, $floatVar, $intVar)
$result->bind_param('iii',$tempIdCoche,$tempIdCoche4,$tempIdCoche3);
$tempIdCoche = 1;
$tempIdCoche4 = 4;
$tempIdCoche3 = 3;

$result->execute();

$result->bind_result($idCoche, $marca);
$result->store_result();
 
if($result->num_rows> 0){
     while ($result->fetch()) 
    {
        echo "IdCoche:" .$idCoche.PHP_EOL;
        echo "Marca:". $marca.PHP_EOL;
        echo "------------------------------------".PHP_EOL;
    }
}

// Liberar  objecto declaracion
$result->close();
// Liberar conexión
$connection->close();
?>