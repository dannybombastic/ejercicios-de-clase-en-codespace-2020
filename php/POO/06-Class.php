<?php 

    // Utilizaremos el modificar protected para denegar el  acceso directo a las propiedades de la clase. Pero la clase que 
    // hereda de ella si puede tener acceso a la propiedad.
    class Cliente
    {
        #region Constantes
        const PAIS ="España";
        #endregion

        #region Propiedades
        public static $Nombre ='Jose';
        public static $Apellidos="Gonzalez";
        public static $FechaNacimiento=1977;
        #endregion

        #region Funciones
        static function DatosPersonales(){
            return self::$Nombre." ".self::$Apellidos." ".self::$FechaNacimiento.PHP_EOL;
        }

        #endregion
        
        #region Metodos

        function MostrarMensaje($msg){
            echo "Cliente -> MostrarMensaje: ".$msg.PHP_EOL;
        }

        #endregion
    }

    class SuperMercado extends Cliente{
        // No se puede acceder a traves de la instacia, pero si atraves de un metodo o función dentro de la clase
        protected $NombreSuper = "Aldi";
        
        // ESte metodo accede a una propiedad proctected de la clase
        public function ObtenerNombre(){
            return $this->NombreSuper."";
        }
       
        public function DatosEstaticoPadre(){
            return "parent--".parent::DatosPersonales();
        }
    }

    #region Creamos instacias y asignamos valores a las propiedades
        $jose = new SuperMercado();
        

        $pablo= new SuperMercado();
        
    #endregion

    #region Visualizamos las propiedades de los objetos.
        echo "Objecto Jose".PHP_EOL;
        echo "------------".PHP_EOL;
        //echo $jose->NombreSuper;
        echo $jose->ObtenerNombre().PHP_EOL;
        echo $jose->DatosPersonales().PHP_EOL;
        echo $jose->DatosEstaticoPadre().PHP_EOL;

        echo "".PHP_EOL;

        echo "Objecto Pablo".PHP_EOL;
        echo "-------------".PHP_EOL;
        echo $pablo->ObtenerNombre().PHP_EOL;
        echo $pablo->DatosPersonales().PHP_EOL;
        echo $pablo->DatosEstaticoPadre().PHP_EOL;

        
    #endregion

?>