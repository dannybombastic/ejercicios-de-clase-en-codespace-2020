<?php 

    // Utilizaremos el modificar protected para denegar el  acceso directo a las propiedades de la clase. Pero la clase que 
    // hereda de ella si puede tener acceso a la propiedad.
    class Cliente
    {
        #region Constantes
        const PAIS ="España";
        #endregion

        #region Propiedades
        protected $Nombre;
        protected $Apellidos;
        protected $FechaNacimiento;
        #endregion

        #region Constructor    
        
        // 3.3
        function __construct($nombre="a", $apellidos="",$fechaNacimiento=""){
            $this->Nombre = $nombre;
            $this->Apellidos = $apellidos;
            $this->FechaNacimiento = $fechaNacimiento;
        }
        #endregion

        #region Funciones

        function ObtenerNombreCompleto(){
            return  $this->Nombre." ".$this->Apellidos." ".$this->FechaNacimiento;
        }

        function DatosPersonales(){
            return $this->Nombre." ".$this->Apellidos." ".$this->FechaNacimiento.PHP_EOL;
        }

        #endregion
        
        #region Metodos

        protected function MostrarMensaje($msg){
            echo "Cliente -> MostrarMensaje: ".$msg.PHP_EOL;
        }

        #endregion
    }

    class SuperMercado extends Cliente{
        // No se puede acceder a traves de la instacia, pero si atraves de un metodo o función dentro de la clase
        protected $NombreSuper = "Aldi";
        
        // ESte metodo accede a una propiedad proctected de la clase
        public function ObtenerNombre(){
            return $this->NombreSuper."";
        } 
        
        public function MetodoProtected($msg){
            $this->MostrarMensaje($msg);
        }
    }

    #region Creamos instacias y asignamos valores a las propiedades
        $jose = new SuperMercado("Jose","Gonzalez",1977);
        

        $pablo= new SuperMercado("Pablo","Apellidos",1955);
        
    #endregion

    #region Visualizamos las propiedades de los objetos.
        echo "Objecto Jose".PHP_EOL;
        echo "------------".PHP_EOL;
        //echo $jose->NombreSuper;
        echo $jose->ObtenerNombre().PHP_EOL;
        echo $jose->DatosPersonales();
        echo $jose->MetodoProtected('hola mundo jose');

        echo "".PHP_EOL;

        echo "Objecto Pablo".PHP_EOL;
        echo "-------------".PHP_EOL;
        echo $pablo->ObtenerNombre().PHP_EOL;
        echo $pablo->DatosPersonales();
        echo $pablo->MetodoProtected('hola mundo pablo');

        
    #endregion

?>