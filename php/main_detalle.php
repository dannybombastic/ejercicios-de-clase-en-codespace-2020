<?php require_once('logica.php') ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <?php require_once('bootstrap.php'); ?>
</head>

<body>
   <?php Menu::getMenu("user"); ?>
    </script>
    <header>
        <?php require_once('navBar.php'); ?>
    </header>
    <?php require_once('detalle.php'); ?>

    <footer>
        <?php require_once('footer.php'); ?>
    </footer>
</body>

</html>