<?php 

/* 

Conceptos:
----------

    - OOP:          La programación orientada a objetos (Object Oriented Programming OOP) es un modelo de lenguaje de 
                    programación organizado por objetos constituidos por datos y funciones, 
                    entre los cuales se pueden crear relaciones como herencia, cohesión, abstracción, 
                    polimorfismo o encapsulamiento.


    - Clases:       Una clase es una estructura de datos que combina estados (campos) y 
                    acciones (métodos y otros miembros de función) en una sola unidad.
                    https://www.php.net/manual/es/language.oop5.basic.php

    - Extends:      Una clase puede heredar los métodos y propiedades de otra clase usando la palabra extends. 
                    No es posible extender a múltiples clases, sólo se puede heredar de una clase. Los métodos y 
                    propiedades heredados pueden ser sobreescritos declarándolos de nuevo con el mismo nombre 
                    que tienen en la clase padre.                     
    
    - Public:       Podrá usarse en cualquier parte del script
    - Private:      Podrá usarse en la clase a la que pertenece
    - Protected:    Podrá usar por la clase a la que pertenece y por sus descendientes.
    - Final:        No puede ser sobreescrito en clases descendientes.
    - Abstract:     No puede ser usado directamente, ha de ser heredado primero para usarse.

    - Operador de   "::" Permite acceder a elementos estáticos, constantes, y sobrescribir propiedades 
    Resolución de    o métodos de una clase.
    ambito:         

     - $this:       Usamos $this para hacer referencia al objeto (instancia) actual, y se 
                    utiliza self:: para referenciar a la clase actual.
     - self:        Cuando queramos acceder a una constante o método estático desde dentro de la clase.
     - parent:      Cuando queramos acceder a una constante o método de una clase padre.
Ficheros:

    - 02-Class.php: Creacion de clase con propiedades, metodos y funciones. NO CONTIENE constructor.
    - 03-Class.php: PUBLIC  Creacion de clase con propiedades, metodos y funciones. CONTIENE constructor.
    - 04-Class.php: PRIVATE Creacion de clase con propiedades, metodos y funciones. CONTIENE constructor.
    - 05-Class.php: PROTECTED Creacion de clase con propiedades, metodos y funciones. CONTIENE constructor.
    - 06-Class.php: Static Creacion de clase con propiedades, metodos y funciones. CONTIENE constructor.
*/

?>