<?php

declare(strict_types=1);

/**
 * clase Chasis de la cual los coche heredan
 */

abstract class Chasis
{

    public static string $numero_bastidor = "545";
    protected string $material;

    function __construct(string $material)
    {
        $this->material = $material;
    }

    public static function comoQuerais(): string
    {
        return self::$numero_bastidor;
    }
    /**
     * Get the value of material
     */
    public function getMaterial()
    {
        return $this->material;
    }

    /**
     * Set the value of material
     *
     * @return  self
     */
    protected function setMaterial($material)
    {
        $this->material = $material;

        return $this->material;
    }
}

class Coche extends Chasis
{

    public const RUEDAS = 4;
    public const VOLANTE = "Reondo";
    private string $marca = "";
    private string $matricula = "";
    private float $altura = 0;
    private float $ancho = 0;
    private float $velocidad = 85.50;
    private string $gasolina = "20L";
    private  float $kilometros = 20.000;


    public function __construct(string $marca, string $matricula, float $altura = 0, float $ancho = 0)
    {
        parent::__construct("Aluminio");
        $this->marca = $marca;
        $this->matricula = $matricula;
        $this->altura = $altura;
        $this->ancho = $ancho;
       
        echo $this->__toString();
    }

    public function frenar(self $var): void
    {
        echo sprintf("Frenando el coche %s", $var->marca . PHP_EOL);
    }
    public function arracar(self $var): void
    {
        echo sprintf("Arranco el coche %s", $var->marca . PHP_EOL);
    }
    /**
     * Get the value of marca
     */
    public function getMarca()
    {
        return $this->marca;
    }

    /**
     * Set the value of marca
     *
     * @return  self
     */
    public function setMarca($marca)
    {
        $this->marca = $marca;

        return $this;
    }

    /**
     * Get the value of matricula
     */
    public function getMatricula()
    {
        return $this->matricula;
    }

    /**
     * Set the value of matricula
     *
     * @return  self
     */
    public function setMatricula($matricula)
    {
        $this->matricula = $matricula;

        return $this;
    }

    /**
     * Get the value of altura
     */
    public function getAltura()
    {
        return $this->altura;
    }

    /**
     * Set the value of altura
     *
     * @return  self
     */
    public function setAltura($altura)
    {
        $this->altura = $altura;

        return $this;
    }

    /**
     * Get the value of ancho
     */
    public function getAncho()
    {
        return $this->ancho;
    }

    /**
     * Set the value of ancho
     *
     * @return  self
     */
    public function setAncho($ancho)
    {
        $this->ancho = $ancho;

        return $this;
    }

    /**
     * Get the value of velocidad
     */
    public function getVelocidad()
    {
        return $this->velocidad;
    }

    /**
     * Get the value of gasolina
     */
    public function getGasolina()
    {
        return $this->gasolina;
    }

    /**
     * Get the value of kilometros
     */
    public function getKilometros()
    {
        return $this->kilometros;
    }

    public function __toString()
    {
        $material = $this->getMaterial();
        return "Modelo $this->marca , Material $material, Matricula $this->matricula " . PHP_EOL;
    }
}

$coche_astra = new Coche("Opel Astra", "MA-5672-BL", 1.55, 2.55);
$coche_kia = new Coche("Kia", "MA-5672-BL", 1.55, 2.55);
$coche_audi = new Coche("Audi", "MA-5672-BL", 1.55, 2.55);
$coche_seat = new Coche("Seat", "MA-5672-BL", 1.55, 2.55);

$soy_unarray_de_objectos = [
    $coche_seat,
    $coche_kia,
    $coche_audi,
    $coche_astra
];



// foreach ($soy_unarray_de_objectos as $objeto) {

//     echo sprintf("hola soy un  nuevo %s y tengo esta matricula  %s y este es mi estado %s KmH %s KmT %s ", $objeto->marca, $objeto->matricula, $objeto->getVelocidad(), $objeto->getKilometros(), $objeto->getGasolina() . PHP_EOL);
// }
Chasis::$numero_bastidor = "hola que paza" . PHP_EOL;
echo Chasis::$numero_bastidor; // <--- prop
echo Coche::RUEDAS; //  <--- const
// $coche_astra->arracar($coche_astra);
// $coche_audi->arracar($coche_audi);
// $coche_astra->frenar($coche_astra);
// $coche_audi->frenar($coche_audi);
// echo sprintf("El coche %s", $coche_seat->marca.PHP_EOL);
// echo sprintf("El coche %s", $coche_audi->marca.PHP_EOL);
// echo sprintf("El coche %s", $coche_kia->marca.PHP_EOL);
