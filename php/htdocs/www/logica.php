<?php
$valido = 0;

function getModal()
{
   global $valido;

   return  $valido;
}

function getform()
{
   global $valido;
   $fname = null;
   $lname = null;
   $email = null;
   $subject = null;
   $obj = null;
   $obj  = null;

   if (isset($_GET["fname"])) {
      $fname = $_GET["fname"];
   }
   if (isset($_GET["lname"])) {
      $lname = $_GET["lname"];
   }
   if (isset($_GET["email"])) {
      $email = $_GET["email"];
   }
   if (isset($_GET["subject"])) {
      $subject = $_GET["subject"];
      $valido = 1;
   }

   $obj = [

      "fname" => $fname,
      "lname" => $lname,
      "email" => $email,
      "subject" => $subject,
   ];

   return $obj;
}

function getId()
{
   if (isset($_GET["id"])) {
      $id = $_GET["id"];
   }
   foreach (getInfo() as $value) {
      if ($value->getId() == $id) {
         return $value;
      }
   }
   return new Articulo(0, "", "", "", "");
}

function getIdlast($id)
{
   foreach (getInfo() as $value) {
      if ($value->getId() == $id) {
         return $value;
      }
   }
}

function getInfo()
{
   $content = array();

   for ($i = 0; $i < 20; $i++) {
      $obj = new Articulo($i, "https://www.iagua.es/sites/default/files/styles/thumbnail-830x455/public/faro_1_0.jpg?itok=dza0UYW9", "Titulo $i", "Description $i", $i);
      $content[] = $obj;
   };

   return $content;


   /**
    * Get the value of id
    */
}

class Articulo
{
   private string $id;
   private string $imagen;
   private string $titulo;
   private string $description;
   private string $valoracion;
   public function __construct(int $id, string $imagen, string $titulo, string $description, string $valoracion)
   {
      $this->id = $id;
      $this->imagen = $imagen;
      $this->titulo = $titulo;
      $this->description = $description;
      $this->valoracion = $valoracion;
   }
   public function getId()
   {
      return $this->id;
   }

   /**
    * Set the value of id
    *
    * @return  self
    */
   public function setId($id)
   {
      $this->id = $id;

      return $this;
   }
   /**
    * Get the value of imagen
    */
   public function getImagen()
   {
      return $this->imagen;
   }

   /**
    * Set the value of imagen
    *
    * @return  self
    */
   public function setImagen($imagen)
   {
      $this->imagen = $imagen;

      return $this;
   }

   /**
    * Get the value of titulo
    */
   public function getTitulo()
   {
      return $this->titulo;
   }

   /**
    * Set the value of titulo
    *
    * @return  self
    */
   public function setTitulo($titulo)
   {
      $this->titulo = $titulo;

      return $this;
   }

   /**
    * Get the value of description
    */
   public function getDescription()
   {
      return $this->description;
   }

   /**
    * Set the value of description
    *
    * @return  self
    */
   public function setDescription($description)
   {
      $this->description = $description;

      return $this;
   }

   /**
    * Get the value of valoracion
    */
   public function getValoracion()
   {
      return $this->valoracion;
   }

   /**
    * Set the value of valoracion
    *
    * @return  self
    */
   public function setValoracion($valoracion)
   {
      $this->valoracion = $valoracion;

      return $this;
   }
}

class Menu
{
   const USER = "user";
   const ADMIN = "admin";
   public static $menu = "";
   public string $name = "";
   public string $url = "";

   public function __construct(string $name = "", string $url = "")
   {
      $this->name = $name;
      $this->url = $url;
   }

   public static function getMenu(string $var)
   {
      switch ($var) {
         case self::ADMIN:
            self::menu_user();
            break;

         case self::USER:
            self::menu_admin();
            break;

         default:
            self::menu_user();
            break;
      }
   }

   public function menu_user()
   {
      self::$menu = [
         createMenu("Contacto", "./main_contact.php"),
         createMenu("Ultima", "./main_contact.php"),
         createMenu("Principal", "./index.php")

      ];
   }

   public function menu_admin()
   {
      self::$menu =  [
         createMenu("Contact", "./main_contact.php"),
         createMenu("Last", "./main_contact.php"),
         createMenu("Home", "./index.php")

      ];
   }
}

function createMenu($name, $url)
{
   $menu = new Menu($name, $url);

   return $menu;
}

class DbHandler
{

   public string $dbPwd = "";
   public string $dbUser = "root";
   public string $dbServer = "localhost";
   public string $dbName = "test";
   public $db = "";

   public function __construct($dbServer, $dbName, $dbUser, $dbPwd)
   {
      $this->dbPwd = $dbPwd;
      $this->dbUser = $dbUser;
      $this->dbServer = $dbServer;
      $this->dbName = $dbName;
      $this->db =  new PDO("mysql:host=$dbServer;dbname=$dbName", $dbUser, $dbPwd);
   }


   // sin parametros
   /* $query = "SELECT * FROM coches";
   $result = $connection->query($query);
   if($result->rowCount() > 0){
       foreach($result as $val){
           print_r($val);
           echo "IdCoche:" .$val["idCoches"].PHP_EOL;
           echo "Marca:". $val["Marca"].PHP_EOL;
           echo "------------------------------------".PHP_EOL;
           
       }
   } */


   public function executeQuery($params)
   {

     
      $query = "INSERT INTO noticias (";

      foreach ($params as $key => $value) {
         $query .= $key . ", ";
      }
      $query = substr($query, 0, -2);
      $query .= ") VALUES (";

      foreach ($params as $key => $value) {
         $query .= '"' . $value . '", ';
      }
      $query = substr($query, 0, -2);
      $query .= ");";

      echo $query; 
      $stm = $this->db->prepare($query);
      $stm->execute();
      $result = null;
      $connection = null;
   }




   public function generateDb()
   {
      

      for ($i = 0; $i < 25; $i++) {
      
         $this->executeQuery([
            "title" => "tocapelotingas",
            "description" => "desciption how's that default",
            "imagen" => "imagen churro",
            "valoration" => $i
         ]);
      }
   }
}

 
$db = new DbHandler("localhost" ,"noticias", "root", "");
 