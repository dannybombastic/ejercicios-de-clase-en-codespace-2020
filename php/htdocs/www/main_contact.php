<?php require_once('logica.php') ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <?php require_once('./Componentes/bootstrap.php'); ?>
    <link rel="stylesheet" href="./asset/CssContact.css">
    <?php require_once('./Componentes/bootstrap_js.php'); ?>
</head>

<body>
    <header>
        <?php Menu::getMenu("admin"); ?>
        <?php require_once('./Componentes/navBar.php'); ?>
    </header>
    <?php require_once('./Componentes/FormContact.php'); ?>

    <footer>
        <?php require_once('./Componentes/footer.php'); ?>
    </footer>
    <?php require_once('./Componentes/bootstrap_js.php'); ?>
</body>

</html>