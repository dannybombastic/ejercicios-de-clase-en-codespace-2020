<?php

// declare(strict_types=1);
// $numero = 12;
// $hola = "HOLA LUCAS";
// header('Content-Type: text/html; charset=utf-8');
// echo  "<h1> $hola " . "<h1>";
// echo "\n";
// $string = "{$numero} hola";
// echo $string;
// echo  gettype($hola);
// echo "\n";
// print("hola" . "hola");
// echo "\n";
// print_r(strtoupper("mañaná"));
// echo "\n";
// echo mb_detect_encoding("mañaná");
// echo "\n";
// $matirce = array();
// $matirceAnonima = ["cadena","cadena_2"];
// $matircenumerica = [1,2,3,4,5,6,7,8,7];

// $libro = [
//   "atributo"=>"hola mamona",
//   "indices" =>"estamos",
//   "andorra"
// ];


// function FunctionName()
// {
//    global $numero; 
// }

// echo array_key_exists(2,$libro), "hola";
// echo in_array("hola mamona",$libro); //no strict
// echo in_array("hola mamona",$libro, true); //strict
// array_push($libro, "mas lisbros");
// $libro["loco"] = "nuevos libros" ;
// print_r($libro);
// array_pop($libro); // elimina el ultimo

// unset($libro[1], $libro["indices"]);

// sort($libro);
// asort($libro);
// count($libro, COUNT_RECURSIVE);
// foreach($libro as $lib){

//   echo $lib;
// }

// foreach($libro as $key => $val){

//         echo $libro[$key] ," valores";

// } 
// echo <<<EOT
//    esto es texto loco MAÑANÁ
//  EOT;




// class Prueeba
// {
//   public ?string $cadena;
//   public  int $numero_interger;
//   public  float $numero_flotante;
//   public array $matri;

//   public function __construct()
//   {
//   }

//   public function setName(string $cadena = null)
//   {
//     $this->cadena = $cadena;
//   }


//   /**
//    * Get the value of numero_interger
//    */
//   public function getNumero_interger()
//   {
//     return $this->numero_interger;
//   }

//   /**
//    * Set the value of numero_interger
//    *
//    * @return  self
//    */
//   public function setNumero_interger($numero_interger)
//   {
//     $this->numero_interger = $numero_interger;

//     return $this;
//   }

//   /**
//    * Get the value of numero_flotante
//    */
//   public function getNumero_flotante()
//   {
//     return $this->numero_flotante;
//   }

//   /**
//    * Set the value of numero_flotante
//    *
//    * @return  self
//    */
//   public function setNumero_flotante($numero_flotante)
//   {
//     $this->numero_flotante = $numero_flotante;

//     return $this;
//   }
// }
// $matriz = array("primero_elemnto", "segundo_elemnto", "tercero_elemnto", "cuarto_elemnto");
// echo "\n";
// echo $matriz[0], " matriz posicion";
// echo "\n";
// $matriz_asociativa = [
//   "first_key" => "first_value",
//   "second_key" => "primer_value",
//   "third_key" =>  "third_nested",
// ];


// echo array_key_exists("second_key", $matriz_asociativa), " existe array asociativo";
// echo "\n";
// echo "existe array simple ", array_key_exists(7, $matriz);
// echo "\n";
// array_push($matriz, "quinto_value");

// $matriz[] = "sexto_elemento";
// echo "\n";
// print_r($matriz);

// echo "elemento bortrado ", array_pop($matriz);
// unset($matriz[0]);
// echo "\n";
// print_r($matriz_asociativa);
// //sort($matriz);
// echo "\n";
// print_r($matriz);
// asort($matriz);
// echo "\n";
// print_r($matriz);


// echo "longitud ", count($matriz_asociativa, COUNT_RECURSIVE);

// echo "existe ? ", in_array("first_value", $matriz_asociativa, true);

// foreach($matriz as $elemento){

//   echo "elementos ", $elemento;
//   echo "\n";
// }

// foreach($matriz_asociativa as $key => $value){

//     echo   "clave $key";
//     echo $matriz_asociativa[$key];
//     echo "\n";
// }


$matriz_asociativa_nested = [
  "first_key"  => "first_value",
  "second_key" => "second_value",
  "third_key"  =>  "third_value",
  "four_key"   => [
    "nested_first_array" => "nested_first",
    "nested_second_array" => "nested_second",
    "nested_third_array" => "nested_third",
    "nested_fourd_array" => [1, 2, 3, 4, 5, 6, 7, 8],
  ],
  "five_key"  =>  "fourd_value",
];

$simple_recursive = array(1, ["1-0", ["2-0"], ["3-2", "3-3"]], "1-1", "1-2", "1-3", [1, 3, 5, [2, 3, 4]]);

//$cnt =0;
/**
 *  Recursive Foreach 
 * 
 */
//global $cnt;
function getValuesRecursive($matriz, $cnt = 0)
{

 
  foreach ($matriz as $key => $value) {
    if (is_array($value)) {
      $cnt++;

      getValuesRecursive($value, $cnt);
    } else {
      echo "\n";
      echo getLevel($cnt) . "valor:  $value  key: $key contador:  $cnt";
    }
  }
}
getValuesRecursive($simple_recursive);

function getLevel($level)
{

  return str_repeat("\t", $level);
}

// function getValuesOneByOne($matriz)
//  /**
//   *  Mechanic Foreach 
//   */

// {
//   foreach ($matriz as $key => $value) {
//     if (is_array($value)) {
//       foreach ($value as $key_nth => $value_nth) {
//         if (is_array($value_nth)) {
//           foreach ($value_nth as $key_nth_nt => $value_nth_nt) {
//             echo "\n";
//             echo "valor:  $value_nth_nt  key: $key_nth_nt";
//           }
//         } else {
//           echo "\n";
//           echo "valor:  $value_nth  key: $key_nth";
//         }
//       }
//     } else {
//       echo "\n";
//       echo "valor:  $value  key: $key";
//     }
//   }
// }
// getValuesOneByOne($matriz_asociativa_nested);
