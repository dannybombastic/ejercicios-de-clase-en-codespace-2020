
<?php require_once('logica.php') ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <?php require_once('./Componentes/bootstrap.php'); ?>
</head>
<?php Menu::getMenu("user"); ?>
<body>
    <header>
        
        <?php require_once('./Componentes/navBar.php'); ?>
    </header>
    <?php require_once('./Componentes/main.php'); ?>

    <footer>
        <?php require_once('./Componentes/footer.php'); ?>
    </footer>
</body>

</html>