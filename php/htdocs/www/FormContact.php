 
<?php require_once('logica.php'); ?>

<div class="container contact">
	<div class="row">
		<div class="col-md-3">
			<div class="contact-info">
				<img src="https://image.ibb.co/kUASdV/contact-image.png" alt="image" />
				<h2>Contact Us</h2>
				<h4>We would love to hear from you !</h4>
			</div>
		</div>
		<div class="col-md-9">
			<form action="/www/FormContact.php" method="GET">
				<div class="contact-form">
					<div class="form-group">
						<label class="control-label col-sm-2" for="fname">First Name:</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" name="fname" id="fname" placeholder="Enter First Name" name="fname">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2" for="lname">Last Name:</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" name="lname" id="lname" placeholder="Enter Last Name" name="lname">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2" for="email">Email:</label>
						<div class="col-sm-10">
							<input type="email" class="form-control" name="email" id="email" placeholder="Enter email" name="email">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2" for="comment">Comment:</label>
						<div class="col-sm-10">
							<textarea class="form-control" rows="5" name="subject" id="comment"></textarea>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
							<button type="submit" class="btn btn-primary">
								Send
							</button>
						</div>
					</div>
				</div>
			</form>

			<!-- Button trigger modal -->

			<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<?php if (getform()) : ?>
							
								<h1><?php $datos = getform() ?></h1>
								<h1><?= $datos['fname'] ?></h1>
								<h1><?= $datos['lname'] ?></h1>
								<h1><?= $datos['email'] ?></h1>
								<h1><?= $datos['subject'] ?></h1>
							<?php else : ?>
								<h5>No info encotrada</h5>
							<?php endif; ?>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
							<button type="button" class="btn btn-primary">Save changes</button>
						</div>
					</div>
				</div>
			</div>
	
			<?php if ($valido == 1) : ?>
				<?= "<script type='text/javascript'>
                     $(document).ready(function(){
                     $('#exampleModal').modal('show');
					  });</script>"; ?>
			<?php endif; ?>

		</div>
	</div>
 