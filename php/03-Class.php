<?php 

    // Utilizaremos el modificar public para poder acceder a las propiedades de la clase. Las funciones por defecto
    // son public si no le indicamos un modificador
    class Cliente
    {
        #region Constantes
        const PAIS ="España";
        #endregion

        #region Propiedades
        public $Nombre;
        public $Apellidos;
        public $FechaNacimiento;
        #endregion
 
        #region Constructor    

        // 3.1
        /* function __construct(){
            echo "hola mundo soy constructor".PHP_EOL;
        } */
    
         
        // 3.2    
        /* function __construct($nombre, $apellidos,$fechaNacimiento){
            $this->Nombre = $nombre;
            $this->Apellidos = $Apellidos;
            $this->FechaNacimiento = $FechaNacimiento;
        } */
    
        // 3.3
        function __construct($nombre="a", $apellidos="",$fechaNacimiento=""){
            $this->Nombre = $nombre;
            $this->Apellidos = $apellidos;
            $this->FechaNacimiento = $fechaNacimiento;
        }
        #endregion

        #region Funciones

        function ObtenerNombreCompleto(){
            return  $this->Nombre." ".$this->Apellidos." ".$this->FechaNacimiento;
        }

        #endregion
        
        #region Metodos

        function MostrarMensaje($msg){
            echo "Cliente -> MostrarMensaje: ".$msg.PHP_EOL;
        }

        #endregion
    }

    #region Creamos instacias y asignamos valores a las propiedades
        $jose = new Cliente();
        /* $jose->Nombre = "Jose";
        $jose->Apellidos = "Gonzalez";
        $jose->FechaNacimiento = 1977; */

        $pablo= new Cliente();
        $pablo->Nombre = "Pablo";
        $pablo->Apellidos = "Apellidos";
        $pablo->FechaNacimiento = "1995";
    #endregion

    #region Visualizamos las propiedades de los objetos.
        echo "Objecto Jose".PHP_EOL;
        echo "------------".PHP_EOL;
        echo $jose->Nombre.PHP_EOL;
        echo $jose->Apellidos.PHP_EOL;
        echo $jose->FechaNacimiento.PHP_EOL;
        echo $jose->ObtenerNombreCompleto().PHP_EOL;
        echo $jose::PAIS.PHP_EOL;
        $jose->MostrarMensaje("hola ".$jose->Nombre);

        echo "".PHP_EOL;

        echo "Objecto Pablo".PHP_EOL;
        echo "-------------".PHP_EOL;
        echo $pablo->Nombre.PHP_EOL;
        echo $pablo->Apellidos.PHP_EOL;
        echo $pablo->FechaNacimiento.PHP_EOL;
        echo $pablo->ObtenerNombreCompleto().PHP_EOL;
        echo $pablo::PAIS.PHP_EOL;
        $pablo->MostrarMensaje("hola ".$pablo->Nombre);
    #endregion

?>