<?php require_once('logica.php') ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <?php require_once('bootstrap.php'); ?>
    <link rel="stylesheet" href="./CssContact.css">
    <?php require_once('bootstrap_js.php'); ?>
</head>

<body>
    <header>
        <?php Menu::getMenu("admin"); ?>
        <?php require_once('navBar.php'); ?>
    </header>
    <?php require_once('FormContact.php'); ?>

    <footer>
        <?php require_once('footer.php'); ?>
    </footer>
    <?php require_once('bootstrap_js.php'); ?>
</body>

</html>