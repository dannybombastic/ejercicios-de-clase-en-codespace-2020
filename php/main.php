 
<div class=container-fluid>
    <div class="row">
        <?php foreach (getInfo() as $value) : ?>
            <div data-id="<?= $value->getId() ?>" class="col-sm col-md-6 col-lg-4">
                <h5> <?= $value->getTitulo() ?></h5>
                <img src="<?= $value->getImagen() ?>" class="img-fluid" alt="">
                <p><?= $value->getDescription() ?></p>
                <a href="./main_detalle.php?id=<?= $value->getId() ?>" class="btn btn-primary btn-block"> Detalle </a>
            </div>
        <?php endforeach; ?>
    </div>
</div>