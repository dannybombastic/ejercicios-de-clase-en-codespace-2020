<?php

if (isset($_POST['action']) && !empty($_POST['action'])) {
    switch ($_POST['action']) {
        case 'register':
            $data = $_POST['data'][0];
            if (responseUser($data['user_dni'])) {
                $resp = [
                    "status" => 200,
                    "msg" => "Usuario Avalaible",
                    "user" => "existente"
                ];
                echo json_encode(array($resp));
            }        
            break;
        case 'login':
            # code...
            break;
        case 'get_categorie':
            # code...
            break;
        case 'get_articles':
            # code...
            break;
        case 'get_article':
            # code...
            break;
        case 'autocomplete':
            # code...
            break;
        case 'register':
            # code...
            break;

        default:
            # code...
            break;
    }
}


function get_db()
{
    $pdo = '';
    $host = 'localhost';
    $db   = 'dani_noticias';
    $db_port = '3306';
    $user = 'dani';
    $pass = '06Xzdktn29';
    $charset = 'utf8';

    $options = [
        \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
        \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
        \PDO::ATTR_EMULATE_PREPARES   => false,
        \PDO::FETCH_BOUND => true
    ];
    $dsn = "mysql:host=$host;dbname=$db;port=$db_port;charset=$charset";

    try {
        $pdo = new PDO($dsn, $user, $pass, $options);
    } catch (PDOException $e) {
        throw new PDOException($e->getMessage(), (int) $e->getCode());
    }
    return $pdo;
}


/**
 * 
 * Obtiene todos los articulos que se muestran en el Home + Condicion de filtrado si la hay
 *  */
function getAllArticle($condicion = null)
{

    $where = "WHERE name = '$condicion'";

    $query = <<<EOD
      SELECT 
          DISTINCT ca.name as name,
          mu.src,
          ar.art_title,
          ar.id,
          ar.art_price,
          ar.art_desc,
          ar.art_evalu,
          ar.create_at,
          ar.is_active
      FROM
          articles AS ar
              JOIN
          multimedia AS mu ON ar.id = mu.id_art
              JOIN
          categories AS ca ON ar.id_cat = ca.id 
            
    EOD;


    if (null !== $condicion) {
        $query .= $where;
    }
    $db = get_db();
    $stm = $db->prepare($query);
    $stm->execute();
    $data = $stm->fetchAll();
    return $data;
}

function fetchSigleUser($dni)
{
    /**
     * Definir la clase con un include o require en este mismo archivo ej require_once('models/usuario.php');
     * Obtener directamente un objeto tipo usuario, se puede obtener una lista  de objetos tipo clase 
     * los atributos de clase deben llamarse igual que los attriburos de la tabla  
     */
    $query = "SELECT * FROM user_profile WHERE user_dni = ?";
    $db = get_db();
    $stm = $db->prepare($query);
    $stm->execute([$dni]);
    return $stm->fetch();
}


function responseUser($user_dni)
{

    if (count(fetchSigleUser($user_dni)) > 1) {
        $resp = [
            "status" => 302,
            "msg" => "Usuario Ya existe",
            "user" => $user_dni
        ];
        echo json_encode(array($resp));
       return false;
    }
    return true;
}
