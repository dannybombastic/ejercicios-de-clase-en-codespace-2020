<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
    <title>dani.codespace.online &mdash; Ready to go</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="description" content="This is a default index page for a new domain." />

 
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style type="text/css">
        body {
            font-size: 50px;
            color: #777777;
            font-family: arial;
            text-align: center;
            width: 100%;
        }

        h1 {
            font-size: 64px;
            color: #555555;
            margin: 70px 0 50px 0;
        }

        p {
            width: 320px;
            text-align: center;
            margin-left: auto;
            margin-right: auto;
            margin-top: 30px
        }

        #tienda {
            float: right;
            clear: both;
            display: inline-block;
            background-color: #343A40;
            text-align: center;
            line-height: 100vh;
            width: 49%;
            height: 100vh;
            text-align: center;
            margin-left: auto;
            margin-right: auto;

        }

        #periodico {
            display: inline-block;
            background-color: #f1f1f1;
            color: white;
            text-align: center;
            line-height: 100vh;
            width: 49%;
            height: 100vh;
            text-align: center;
            margin-left: auto;
            margin-right: auto;

        }

        #periodico a {
            writing-mode: vertical-rl;
            text-orientation: upright;
            color: red;
            font-size: 5rem;
            border-radius: 50px;
            background: #f1f1f1;
            box-shadow: 19px 19px 38px #bebebe,
                -19px -19px 38px #ffffff;
        }

        #periodico .btn {
            writing-mode: vertical-rl;
            text-orientation: upright;
            color: red;
            font-size: 5rem;
            border-radius: 50px;
            background: #f1f1f1;
            box-shadow: 13px 13px 26px #ececec,
                -13px -13px 26px #f6f6f6;
        }

        #tienda .btn {

            writing-mode: vertical-rl;
            text-orientation: upright;
            color: orange;
            font-size: 5rem;
            border-radius: 50px;
            background: #343A40;
            box-shadow: 20px 20px 60px #2e3439,
                -20px -20px 60px #3a4047;
        }

        #tienda a {

            writing-mode: vertical-rl;
            text-orientation: upright;
            color: orange;
            font-size: 5rem;
            border-radius: 50px;
            background: #343A40;
            box-shadow: 20px 20px 60px #2b3035,
                -20px -20px 60px #3d444b;
        }

        a:link {
            color: white;
        }

        a:visited {
            color: white;
        }

        a:active {
            color: white;
        }

        a:hover {
            color: white;
        }
    </style>
</head>

<body>

    <div id="periodico">
        <div class="btn p-3">
            <div class="btn p-3">
                <div class="btn p-3">
                    <div class="btn p-3">
                        <a class="btn p-3" href="http://dani.codespace.online/noticias/">Noticias</a>
                    </div>
                </div>
            </div>
        </div>


    </div>
    <div id="tienda">
        <div class="btn p-3">
            <div class="btn p-3">
                <div class="btn p-3">
                    <div class="btn p-3">
                        <a class="btn p-3" href="http://dani.codespace.online/code/">Tienda</a>
                    </div>
                </div>
            </div>
        </div>


    </div>
</body>

</html>