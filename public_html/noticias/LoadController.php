<?php

require_once('autoload.php');
require_once('./Config/Conf.php');
 

$showError = true;
$controllerName = "HomeController";
$action = "home";


 

if(isset($_GET['q']) && !empty($_GET['q'])) Conf::$QUERY = $_GET['q'];

// Si $_GET["controller"] es vacio, instroducimos uno por defecto. Esto solo ocurre para cargar la home
$controllerDefine = isset($_GET["controller"]);
if ($controllerDefine) $controllerName = $_GET['controller'] . 'Controller';

// Si $_GET["action"] es vacio, instroducimos uno por defecto.
$actionDefine = isset($_GET["action"]);
if ($actionDefine) $action = $_GET["action"];

if (class_exists($controllerName)) {
	$controller = new $controllerName();
	$exists = method_exists($controller, $action);
	$showHome = !$exists;

	if ($exists) $controller->$action();
}

if ($showHome) {
	$home = new HomeController();
	$home->home();
}
