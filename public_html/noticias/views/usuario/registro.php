<div class="row justify-content-center">
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="alert alert-<?= $success ?> alert-dismissible fade show" role="alert">
            <strong>Holy guacamole!</strong> You should check in on some of those fields below.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
    <div class="col-md-8 order-md-1 text-center">
        <h4 class="mb-3">User Profile</h4>
        <form class="needs-validation pb-5" action="/noticias/User/register/" method="POST">
            <div class="row text-left">
                <div class="col-md-6 mb-3">
                    <label for="lastName">DNI</label>
                    <input type="text" class="form-control" id="lastName" placeholder="DNI..." value="" name="user_dni" required>
                    <div class="invalid-feedback">
                        Valid last name is required.
                    </div>
                </div>
                <div class="col-md-6 mb-3">
                    <label for="username">Username</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">@</span>
                        </div>
                        <input type="text" class="form-control" id="username" placeholder="Username" name="user_name" required="">
                        <div class="invalid-feedback" style="width: 100%;">
                            Your username is required.
                        </div>
                    </div>
                </div>
                <div class="col-md-6 mb-3">
                    <label for="email">Email </label>
                    <input type="email" class="form-control" id="email" name="user_email" placeholder="you@example.com">
                    <div class="invalid-feedback">
                        Please enter a valid email address for shipping updates.
                    </div>
                </div>
                <div class="col-md-6 mb-3">
                    <label for="address">Address</label>
                    <input type="text" class="form-control" name="user_addr" id="address" placeholder="1234 Main St" required="">
                    <div class="invalid-feedback">
                        Please enter your shipping address.
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 mb-3">
                    <label for="address2">Password</label>
                    <input type="password" class="form-control" name="user_password" id="password" placeholder="Password">
                </div>
                <div class="col-md-6 mb-3">
                    <label for="address2">Repite your Password </label>
                    <input type="password" class="form-control" id="" placeholder="Password">
                </div>
                <div class="col-md-6 mb-3">
                    <label for="country">Active <span class="text-muted">(Optional)</span></label>
                    <select class="custom-select d-block w-100" name="is_active" id="role" required="">
                        <option value="1">Active</option>
                        <option value="0">Disable</option>
                    </select>
                    <div class="invalid-feedback">
                        Please select a valid country.
                    </div>
                </div>
                <div class="col-md-6 mb-3">
                    <label for="country">Gender</label>
                    <select class="custom-select d-block w-100" name="user_gender" id="gender" required="">
                        <option value="M">Men</option>
                        <option value="F">Women</option>
                    </select>
                    <div class="invalid-feedback">
                        Please select a valid country.
                    </div>
                </div>
                <div class="col-md-12 mb-3">
                    <label for="country">Roles</label>
                    <select class="custom-select d-block w-100" name="user_role" id="gender" required="">
                        <option value="1">User</option>
                        <option value="2">Admin</option>
                    </select>
                    <div class="invalid-feedback">
                        Please select a valid country.
                    </div>
                </div>
            </div>
            <hr class="mb-4">
            <button class="btn btn-lg btn-block" type="submit">Continue to checkout</button>
        </form>
    </div>
</div>