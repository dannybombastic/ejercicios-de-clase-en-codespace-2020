<div class="parallax row justify-content-lg-center pt-5 pb-5">
    <div class="text-center text-white  col-sm-12 col-md-12 col-lg-12">
        <h1 class="bg-dark">Detail <?= $pro['title'] ?> ID <?= Conf::$QUERY ?></h1>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
        <div class="card">
            <img class="" src="static/img/<?=$pro['src'] ?>" alt="Card image cap" height="300" >
            <div class="card-body">
                <h5 class="card-title"><?= $pro['title'] ?></h5>
                <p class="card-text"><?= $pro['art_desc'] ?></p>
            </div>
            <div class="card-footer">
                <small class="text-muted"><?= $pro['create_at'] ?></small>
                Last Time View: <small class="text-info"><?= $pro['visited_at'] ?> </small>
                Mny Times View: <small class="text-info"><?= $pro['visited_cnt'] ?> </small>
                Categorie <small class="text-warning"><?= $pro['name'] ?> €</small>
            </div>
        </div>
    </div>
</div>