<?php require_once('models/usuario.php'); ?>
<div class="container-fluid testimonial-group">

    <div class="row w-100" id="main-row">
        <span id="left">
        <i class="fas fa-angle-left"></i>
        </span>
        <span id="right">
        <i class="fas fa-angle-right"></i>
        </span>
    </div>
</div>
<div class="container-fluid testimonial-group" id="main-pseudo">
    <div class="row w-100" id="main-bottom">
        <span id="left-bottom">
        <i class="fas fa-angle-left"></i>
        </span>
        <span id="right-bottom">
        <i class="fas fa-angle-right"></i>
        </span>
    </div>
    <script>
        <?php include_once('./static/js/bind.js'); ?>
        getData(<?php echo json_encode($articles, JSON_HEX_TAG) ?>, <?php echo json_encode($condition, JSON_HEX_TAG) ?>);
    </script>
</div>