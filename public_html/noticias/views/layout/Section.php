<?php
require_once('./controllers/HomeController.php');
$categories = new HomeController();
$categories = $categories->getCategorie();
?>


<div class="container-fluid">
    <div class="text-white justify-content-center text-center pt-3">
        <div class=" col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <div class="row justify-content-center">
       
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                <i class="fas text-danger h3 fa-sort"></i> <h3 class="text-danger d-inline">Order</h3>
                    <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-12 col-lg-4">
                            <button class="btn m-2 text-nowrap " onclick="order_by_date()">By Date</button>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-12 col-lg-4">
                            <button class="btn m-2 text-nowrap " onclick="order_by_cate()">By Categorie</button>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-12 col-lg-4">
                            <button class="btn m-2 text-nowrap" onclick="order_by_visited()">By Most visited</button>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <label for="auto-complete">
                <i class="fas text-danger h3 fa-search"></i>  <h3 class="text-danger d-inline">Search</h3>
                    </label>
                    <form action="/noticias/" class="form pl-4 text-white" method="POST">
                        <input class="form-control mr-sm-2" type="search" placeholder="Search" name="condition" id="auto-complete" aria-label="Search">
                        <button class="btn btn mt-2 btn-block" type="submit">Search</button>
                    </form>
                </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                    <label for="exampleFormControlSelect1">
                        <h3 class="text-danger">Categories</h3>
                    </label>
                    <form action="/noticias/" class="form my-2 my-lg-0" method="POST">
                        <select class="form-control" name="condition" id="exampleFormControlSelect1">
                            <?php foreach ($categories as $cat) : ?>
                                <option><?= $cat['name'] ?></option>
                            <?php endforeach ?>
                            <option>All</option>

                        </select>
                        <button class="btn m-2 pt-2 btn-block" type="submit">Search</button>
                    </form>
                </div>
            </div>


        </div>



    </div>
    <hr>
</div>