<header>
    <nav class="navbar border-bottom border-dark navbar-expand-lg navbar-light bg-light p-4">
        <a class="navbar-brand" href="/noticias/"><img id="logo" src="static/img/logo.png" alt="" height="75"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <?php if ($isAdmin) : ?>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle text-danger" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Actions Admin Forms
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="Producto/putForm/">Add Article</a>
                            <a class="dropdown-item" href="Producto/updateForm/">Update Article</a>

                            <div class="dropdown-divider"></div>

                            <a class="dropdown-item" href="Categorie/putForm/">Add Categorie</a>
                            <a class="dropdown-item" href="Categorie/updateForm/">Update Categorie</a>

                            <div class="dropdown-divider"></div>

                            <a class="dropdown-item" href="MultiMedia/typePut/">Add Multimedia Type </a>
                            <a class="dropdown-item" href="MultiMedia/typeUpdate/">Update Multimedia Type </a>
                            <div class="dropdown-divider"></div>

                            <a class="dropdown-item" href="MultiMedia/mediaPut/">Add Multimedia </a>
                            <a class="dropdown-item" href="MultiMedia/mediaUpdate/">Update Multimedia </a>

                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="User/register/">Add User </a>
                            <a class="dropdown-item" href="MultiMedia/mediaPut/">Update user</a>
                        </div>
                    </li>
                <?php endif; ?>
                <li class="nav-item">
                    <a class="nav-link active" href="#">Disabled</a>
                </li>
                <li>

                </li>
            </ul>

            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                <my-list name="">

                </my-list>
            </div>



            <button type="button" id="filters" class="btn m-2 btn-default">
                <i class="fas text-danger fa-search"></i> Filters
            </button>
            <div class="dropdown">
                <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    User Actions
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">

                    <?php if (!is_null($user)) : ?>
                        <span class="p-2"> <?= $user->user_name ?></span>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item text-danger" href="User/logout/">Log out</a>
                        <a class="dropdown-item text-danger" href="User/udate/">Update Profile</a>
                    <?php else : ?>
                        <a class="dropdown-item text-danger" href="User/login/"><i class="fas text-warning fa-sign-in-alt"></i> Login </a>
                        <a class="dropdown-item text-danger" href="User/register/"><i class="fas text-dark fa-user-alt"></i> Register </a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </nav>
</header>