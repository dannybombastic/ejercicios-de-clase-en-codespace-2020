<div class="row justify-content-center parallax p-5">
  <div class="p-2 col-xs-12 col-sm-12 col-md-12 col-lg-6">
    <div class="alert alert-<?= $success ?> alert-dismissible fade show" role="alert">
      <strong>Holy guacamole!</strong> You should check in on some of those fields below.
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <?php if (!empty(Conf::$QUERY)) : ?>
      <div class="text-center text-white col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <h3>Udapte Media</h3>
            </div>
      <form action="/noticias/MultiMedia/typeUpdate/<?= Conf::$QUERY ?>" method="POST">
        <div class="form-group">
          <label for="media_type">Media Type</label>
          <input type="text" name="media_type" id="media_type" class="form-control" value="<?= $media['m_type'] ?>" placeholder="Media type" aria-describedby="helpId" required>
          <small id="helpId" class="text-muted">media</small>
        </div>
        <button type="submit" class="btn btn-lg btn-block">Block level button</button>
      </form>
    <?php endif; ?>
    <?php if (empty(Conf::$QUERY)) : ?>
      <div class="text-center col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <h3>Create Media</h3>
            </div>
      <form action="/noticias/MultiMedia/typePut/" method="POST">
        <div class="form-group">
          <label for="media_type">Media Type</label>
          <input type="text" name="media_type" id="media_type" class="form-control" placeholder="Media type" aria-describedby="helpId" required>
          <small id="helpId" class="text-muted">media</small>
        </div>
        <button type="submit" class="btn btn-lg btn-block">Block level button</button>
      </form>
    <?php endif; ?>
  </div>
</div>