<div class="row  justify-content-center parallax p-5 text-danger">

    <div class="p-2 col-xs-12 col-sm-12 col-md-12 col-lg-6">

        <div class="alert alert-<?= $success ?> alert-dismissible fade show" role="alert">
            <strong>Holy guacamole!</strong> You should check in on some of those fields below.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php if (empty(Conf::$QUERY)) : ?>
 
            <div class="text-center bg-white col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <h3>Create Media</h3>
            </div>
            <form action="/noticias/MultiMedia/mediaPut/" method="POST" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="id_art">Media type</label>
                    <select class="form-control" name="id_art" value="" id="categories" required>
                        <?php foreach ($article as $value) : ?>
                            <option value="<?= $value['id'] ?>"><?= $value['art_title'] ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="src"> Soruce Path </label>
                    <input type="file" name="src" id="src" class="form-control" placeholder="Media type" value="<?= $media['src'] ?>" onchange="var path = 
                    document.getElementById('src').textContent = this.files.length > 0 ? this.files[0].name : 'Select a file'; 
                    document.getElementById('src').setAttribute('value', path);" aria-describedby="helpSrc">


                    <small id="helpSrc" class="text-muted">Help text</small>
                </div>
                <div class="form-group">
                    <label for="categories">Media type</label>
                    <select class="form-control" name="media_type" value="<?= $media['id_tipo'] ?>" id="categories" required>
                        <?php foreach ($media_type as $value) : ?>
                            <option value="<?= $value->id ?>"  <?php if ($media['id_tipo'] == $value->id) : ?> <?= 'selected' ?> <?php endif; ?>><?= $value->m_type ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                
                
                
                <div class="form-check form-check-inline mt-3" id="active">
                    <input class="form-check-input" type="radio" name="active" id="label_yes" value="1">
                    <label class="form-check-label" for="label_yes"> Active</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="active" id="label_no" value="0">
                    <label class="form-check-label" for="label_no"> No Active</label>
                </div>
                <button type="submit" class="btn btn-lg mt-4 btn-block">Block level button</button>


            </form>
        <?php endif; ?>
        <?php if (!empty(Conf::$QUERY)) : ?>
            <div class="text-center bg-white col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <h3>Udapte Media</h3>
            </div>
    
            <form action="/noticias/MultiMedia/mediaUpdate/<?= Conf::$QUERY ?>" method="POST" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="id_art">Media </label>
                    <select class="form-control" name="id_art" value="" id="categories" required>
                        <?php foreach ($article as $value) : ?>
                            <?php if ($value['id'] == $media['id_art']) : ?>
                                <option value="<?= $value['id'] ?>"  <?php if ($media['id_art'] == $value['id']) : ?> <?= 'selected' ?> <?php endif; ?> ><?= $value['art_title'] ?> </option>
                            <?php endif; ?>
                            <option value="<?= $value['id'] ?>"><?= $value['art_title'] ?> </option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="src"> Soruce Path:  </label>
                    <input type="file" name="src" id="src" class="form-control" placeholder="Media type" value="<?= $media['src'] ?>" onchange="var path = 
                    document.getElementById('src').textContent = this.files.length > 0 ? this.files[0].name : 'Select a file'; 
                    document.getElementById('src').setAttribute('value', path);" aria-describedby="helpSrc">

                    <small id="helpSrc" class="text-danger">Source path: <?=$media['src']?> </small>
                </div>
                <div class="form-group">
                    <label for="media_type">Media type</label>
                    <select class="form-control" name="media_type" value="<?= $media['id_tipo'] ?>" id="media_type" required>
                        <?php foreach ($media_type as $value) : ?>
                            <option value="<?= $value->id ?>"  <?php if ($media['id_tipo'] == $value->id) : ?> <?= 'selected' ?> <?php endif; ?>> <?= $value->m_type ?> </option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-check form-check-inline" id="active">
                    <input class="form-check-input" type="radio" name="active" id="label_yes" value="1"  <?php if ($media['is_active'] == 1) : ?> <?= 'checked' ?> <?php endif; ?>>
                    <label class="form-check-label" for="label_yes"> Active</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="active" id="label_no" value="0"  <?php if ($media['is_active'] == 0) : ?> <?= 'checked' ?> <?php endif; ?>>
                    <label class="form-check-label" for="label_no"> No Active</label>
                </div>
                <button type="submit" class="btn btn-lg btn-block">Block level button</button>

            </form>
        <?php endif; ?>

    </div>
</div>