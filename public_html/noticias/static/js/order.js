$(document).ready(function () {




});

var asc_date = true;
function order_by_date() {
    asc_date = !asc_date;
    $( "#main-row > .custom-item" ).hide( "highlight" , 800, function(){ $( ".custom-item" ).show( "drop");});
    let list = $('#main-row .custom-item').get();
   
    list.sort(function (a, b) {
        if (asc_date) {
            return moment($(a).data("date"), 'YYYY-MM-DD:h:mm:ss') < moment($(b).data("date"), 'YYYY-MM-DD:h:mm:ss') ? 1 : -1;
        } else {
            return moment($(a).data("date"), 'YYYY-MM-DD:h:mm:ss') > moment($(b).data("date"), 'YYYY-MM-DD:h:mm:ss') ? 1 : -1;
        }
    });
    
    $(list).appendTo("#main-row");

}



var asc_money = true;
function order_by_cate() {
        $( "#main-row > .custom-item" ).hide( "fade" , 800, function(){ $( ".custom-item" ).show( "fade");});
    asc_money = !asc_money;

    let list = $('#main-row .custom-item').get();
    console.log("hola", list );
    list.sort(function (a, b) {
        if (asc_money) {
            return $(a).data("cate") < $(b).data("cate") ? 1 : -1;
        } else {
            return $(a).data("cate") > $(b).data("cate") ? 1 : -1;
        }
    });

    $(list).appendTo("#main-row");

}


var asc_visited = true;
function order_by_visited() {
        $( "#main-row > .custom-item" ).hide( "fade" , 800, function(){ $( ".custom-item" ).show( "fade");});
        asc_visited = !asc_visited;

    let list = $('#main-row .custom-item').get();
    console.log("hola", list );
    list.sort(function (a, b) {
        if (asc_visited) {
            return $(a).data("visited") < $(b).data("visited") ? 1 : -1;
        } else {
            return $(a).data("visited") > $(b).data("visited") ? 1 : -1;
        }
    });

    $(list).appendTo("#main-row");

}