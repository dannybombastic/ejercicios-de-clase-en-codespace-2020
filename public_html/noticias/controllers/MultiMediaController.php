<?php

declare(strict_types=1);
require_once('models/usuario.php');
require_once('Base/BaseMultimediaController.php');
require_once('utils/upload.php');

class MultiMediaController extends BaseMultimediaController
{


    public function typeUpdate()
    {
        $success = 'success invisible';
        $media = [
            "id" => 0,
            "m_type" => 'vacio'
        ];
        if (Conf::$QUERY !== '') {
            $media = $this->getMultimediaType((int) Conf::$QUERY);
            if (!empty($media)) {
                $media = $media[0];
            }
            if (count($_POST) > 0) {
                $success = $this->updateTypeFrom();
                $media = $this->getMultimediaType((int) Conf::$QUERY);
                $media = $media[0];
            }
        }
        require_once('./views/Multimedia/MediaType.php');
    }
    public function typePut()
    {
        $success = 'success invisible';
        if (Conf::$QUERY === '') {
            if (count($_POST) > 0) {
                $success = $this->serializeMediaTypeForm();
            }
        }

        require_once('./views/Multimedia/MediaType.php');
    }

    public function mediaUpdate()
    {
        $media_type = $this->getAllMultimediaType();
        $article = $this->getAlltoPutArticle();
        $success = 'success invisible';
        $media = [
            "id" => 0,
            "src" => 'vacio',
            "id_tipo" => 'vacio',
            "is_active" => '0'

        ];
        
        if (Conf::$QUERY !== '') {

            if (count($this->getMultimedia((int) Conf::$QUERY))  > 0) {
                $media = $this->getMultimedia((int) Conf::$QUERY)[0];
 
            }
            if (count($_POST) > 0) {
                $success = $this->updateMediaForm();
                $media = $this->getMultimedia((int) Conf::$QUERY)[0];
              
            }
        }

        require_once('./views/Multimedia/MultiMedia.php');
    }

    /**
     * 
     * MultiMedia/mediaPut/{id}
     */
    public function mediaPut()
    {
        $media = [
            "id" => 0,
            "src" => 'vacio',
            "id_tipo" => 'vacio',
            "is_active" => '0'

        ];
        $media_type = $this->getAllMultimediaType();
        $article = $this->getAlltoPutArticle();
        $success = 'success invisible';
        if (Conf::$QUERY === '') {
            if (count($_POST) > 0) {

                $success = $this->serializeMediaForm();
                if (count($this->getMultimedia((int) Conf::$QUERY)) > 0) {
                    $media = $media[0];
                }
            }
        }

        require_once('./views/Multimedia/MultiMedia.php');
    }






    /**
     * 
     * Update MediaType
     * 
     */
    public function updateTypeFrom(): string
    {
        if (count($_POST) > 0 && isset($_POST['media_type'])) {

            $this->updateMediatype($_POST['media_type'], (int) Conf::$QUERY);
            return 'success visible';
        } else {
            return 'danger visible';
        }
    }

    /**
     * 
     * Serialize MediaType
     * 
     */
    public function serializeMediaTypeForm(): string
    {
        if (count($_POST) > 0 && isset($_POST['media_type'])) {

            $this->setMultimediaType($_POST['media_type']);
            return 'success visible';
        } else {
            return 'danger visible';
        }
    }


    /**
     * 
     * Serialize Media Files
     * 
     */
    public function serializeMediaForm(): string
    {
        if (count($_POST) > 0 && isset($_POST['id_art']) && isset($_FILES['src']) && isset($_POST['media_type'])) {
            $upload = new Upload();
            $msg = $upload->uploadFile($_POST['media_type']);
            $this->setMultimedia([(int) $_POST['id_art'], $_FILES['src']['name'], $_POST['media_type'], $_POST['active']]);
            return 'success visible';
        } else {
            return 'danger visible';
        }
    }

    /**
     * 
     * Update Media Files
     * 
     */
    public function updateMediaForm(): string
    {
        if (count($_POST) > 0 && isset($_POST['id_art']) && isset($_POST['src']) && isset($_POST['active'])) {

            $this->updateMedia([$_POST['id_art'], $_POST['src'], $_POST['media_type'], $_POST['active'], Conf::$QUERY]);
            return 'success visible';
        } else {
            return 'danger visible';
        }
    }
}
