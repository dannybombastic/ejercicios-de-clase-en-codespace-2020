<?php
require_once 'models/producto.php';
require_once('Base/BaseProductController.php');

class productoController extends BaseProductController
{

	public function mostPopular()
	{
		require_once 'views/producto/MostPopular.php';
	}

	/**
	 * /Producto/put/{id}
	 * 
	 */
	public function putForm()
	{
		$success = 'success invisible';
		$categories = $this->getCategorie();
		$pro = null;
		
		if (Conf::$QUERY === '') {
			if (count($_POST) > 0) {
				$success = $this->serializerProduct();
			}
		}

		require_once 'views/producto/Put.php';
	}

	public function updateForm()
	{
		$success = 'success invisible';
		$categories = $this->getCategorie();
		$pro = null;
		
		if (Conf::$QUERY !== '') {
			$product = $this->getProducto(Conf::$QUERY);
			if (count($product) > 0) {
				$pro = $product[0];
			}
			if (count($_POST) > 0) {
				$success = 	$this->updateRequest();
				$product = $this->getProducto(Conf::$QUERY);
				$pro = $product[0];
			}
		}

		require_once 'views/producto/Put.php';
	}
 

	/**
	 * 
	 * save into db
	 */
	public function serializerProduct()
	{
		if (count($_POST) > 0 && isset($_POST['title']) && isset($_POST['active'])) {
			$fecha = date_create();
			$fecha = $fecha->format('Y-m-d H:i:s');
			$this->setProducto($_POST['title'], $_POST['description'], $_POST['star'], $_POST['categorie'], $fecha, (int) $_POST['active']);
			return 'success visible';
		} else {
			return 'danger visible';
		}
	}
	/***
	 * update 
	 */
	public function updateRequest()
	{
		if (count($_POST) > 0 && isset($_POST['title']) && isset($_POST['active'])) {
			$fecha = date_create();
			$fecha = $fecha->format('Y-m-d H:i:s');
			$this->updateProducto($_POST['title'], $_POST['description'], $_POST['star'], $_POST['categorie'], $fecha, (int) $_POST['active'], Conf::$QUERY);
			return 'success visible';
		} else {
			return 'danger visible';
		}
	}
}
