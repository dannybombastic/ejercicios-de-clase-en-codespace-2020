<?php

declare(strict_types=1);

require_once('models/usuario.php');
require_once('models/MediaType.php');
abstract class BaseController
{
    protected $pdo;
    private string $host = 'db';
    private string $db   = 'journal';
    private string $db_port = '3306';
    private string $user = 'dannybombastic';
    private string $pass = '606197854';
    private string $charset = 'utf8';

    private array $options = [
        \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
        \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
        \PDO::ATTR_EMULATE_PREPARES   => false,
        \PDO::FETCH_BOUND => true
    ];

    function __construct()
    {
        $dsn = "mysql:host=$this->host;dbname=$this->db;port=$this->db_port;charset=$this->charset";

        try {
            $this->pdo = new PDO($dsn, $this->user, $this->pass, $this->options);
        } catch (PDOException $e) {
            throw new PDOException($e->getMessage(), (int) $e->getCode());
        }
    }

    /**
     * Inserta cualquier query con parametros
     */
    protected function insert(string $query, array $values)
    {
        $stm = $this->pdo->prepare($query);
        $stm->execute($values);
        return  $stm;
    }

    /**
     * Actualiza cualquier query con parametros siendo el id el ultimo parametro
     */
    protected function update(string $query, array $values)
    {    // ex.  $sql = "UPDATE users SET name=?, surname=?, sex=? WHERE id=?";
        $stm = $this->pdo->prepare($query)->execute($values);
        return  $stm;
    }

    /**
     * Obtenemos los datos de un usuario por su dni primary key para mi
     */
    public function fetchSigleUser($dni)
    {
        /**
         * Definir la clase con un include o require en este mismo archivo ej require_once('models/usuario.php');
         * Obtener directamente un objeto tipo usuario, se puede obtener una lista  de objetos tipo clase 
         * los atributos de clase deben llamarse igual que los attriburos de la tabla  
         */
        $query = "SELECT * FROM user_profile WHERE user_dni = ?";
        $stm = $this->pdo->prepare($query);
        $stm->setFetchMode(PDO::FETCH_CLASS, 'Usuario');
        $stm->execute([$dni]);
        return $stm->fetch();
    }

    /**
     * Obtenemos todos los tipos multimedia para armar los menus 
     */

    public function getAllMultimediaType()
    {
        $query = "SELECT * FROM multimedia_type";
        $stm = $this->pdo->prepare($query);
        $stm->setFetchMode(PDO::FETCH_CLASS, 'MediaType');
        $stm->execute();
        $data = $stm->fetchAll();
        return $data;
    }

    /**
     * Devuelve la conexion a la base de datos con su instacia
     */
    protected function getDb(): self
    {
        return $this;
    }


    /**
     * Creo las tabls en bases de datos necesarias
     */
    public function createTables()
    {

        $query = "CREATE DATABASE IF NOT EXISTS journal CHARACTER SET = 'utf8' COLLATE = 'utf8_general_ci';";
        $stm = $this->pdo->prepare($query)->execute();
        $query = "use journal;";
        $stm = $this->pdo->prepare($query)->execute();
        
        $query = <<<EOD
            CREATE TABLE IF NOT EXISTS user_roles (
              id INT NOT NULL AUTO_INCREMENT,         
              user_role VARCHAR(25) NOT NULL,
              PRIMARY KEY(id)                           
            );
            EOD;
        $stm = $this->pdo->prepare($query)->execute();
        $query = <<<EOD
            CREATE TABLE IF NOT EXISTS user_profile (
              user_dni VARCHAR(24) NOT NULL,
              user_name VARCHAR(24) NOT NULL,
              user_addr VARCHAR(60) NOT NULL,
              user_email VARCHAR(60) NOT NULL,
              user_password VARCHAR(150) NOT NULL,
              gender ENUM ('M', 'F') NOT NULL,
              user_role INT NOT NULL DEFAULT 0,
              create_at DATETIME NOT NULL,
              is_active TINYINT NOT NULL DEFAULT 0,
              KEY(user_role),
              KEY(user_dni),
              PRIMARY KEY(user_dni)            
            );
            EOD;

        $stm = $this->pdo->prepare($query)->execute();
        $query = <<<EOD
            CREATE TABLE IF NOT EXISTS articles (
              id INT NOT NULL AUTO_INCREMENT,
              art_title VARCHAR(80) NOT NULL,
              art_desc VARCHAR(400) NOT NULL,
              art_evalu CHAR(5) NOT NULL,
              id_cat INT NOT NULL DEFAULT 0,
              visited_cnt int NOT NULL DEFAULT 0,
              visited_at DATETIME NOT NULL,
              create_at DATETIME NOT NULL,
              is_active TINYINT NOT NULL DEFAULT 0,
              KEY(id_cat),
              PRIMARY KEY(id)  
            );
            EOD;
        $stm = $this->pdo->prepare($query)->execute();
        $query = <<<EOD
            CREATE TABLE IF NOT EXISTS categories (
              id INT NOT NULL AUTO_INCREMENT,
              name VARCHAR(80) NOT NULL,
              image VARCHAR(80) NOT NULL,
              is_active TINYINT NOT NULL DEFAULT 0,
              PRIMARY KEY (id)  
            );
            EOD;
        $stm = $this->pdo->prepare($query)->execute();
        $query = <<<EOD
            CREATE TABLE IF NOT EXISTS comments (
              id INT NOT NULL AUTO_INCREMENT,
              art_comment VARCHAR(250) NOT NULL,
              id_art INT NOT NULL,
              id_user VARCHAR(18) NOT NULL,
              is_active TINYINT NOT NULL DEFAULT 0,
              KEY(id_art),
              KEY(id_user),
              PRIMARY KEY(id),
              CONSTRAINT comments_relation FOREIGN KEY (id_user) REFERENCES user_profile(user_dni)
            );
            EOD;
        $stm = $this->pdo->prepare($query)->execute();
        $query = <<<EOD
          CREATE TABLE IF NOT EXISTS multimedia_type (
            id INT NOT NULL AUTO_INCREMENT,
            m_type VARCHAR(80),
            PRIMARY KEY(id)
        );
       EOD;
        $stm = $this->pdo->prepare($query)->execute();
        $query = <<<EOD
        CREATE TABLE IF NOT EXISTS multimedia (
            id INT NOT NULL AUTO_INCREMENT,
            id_art INT NOT NULL,
            src VARCHAR(2000) NOT NULL,
            home TINYINT NOT NULL DEFAULT 0, 
            id_tipo INT NOT NULL,
            is_active TINYINT NOT NULL DEFAULT 0,
            PRIMARY KEY(id),
            KEY(id_art),
            KEY(id_tipo),
            CONSTRAINT multi_relation FOREIGN KEY (id_tipo) REFERENCES multimedia_type(id)
        );
       EOD;
        $stm = $this->pdo->prepare($query)->execute();
        $query = <<<EOD
       CREATE TABLE IF NOT EXISTS menu (
           id INT NOT NULL AUTO_INCREMENT,
           id_menu INT NOT NULL,
           href VARCHAR(255) NOT NULL,
           PRIMARY KEY(id)
       );
      EOD;
        $stm = $this->pdo->prepare($query)->execute();
        // $query = "CREATE INDEX art_id ON trolleybascket(id_art);";
        // $stm = $this->pdo->prepare($query)->execute();

        $query = "ALTER TABLE user_profile ADD CONSTRAINT rol_relation FOREIGN KEY (user_role) REFERENCES user_roles(id) ON DELETE CASCADE ON UPDATE CASCADE;";
        $stm = $this->pdo->prepare($query)->execute();
        $query = "ALTER TABLE comments ADD CONSTRAINT art_relation_coments FOREIGN KEY (id_art) REFERENCES articles(id) ON DELETE CASCADE ON UPDATE CASCADE;";
        $stm = $this->pdo->prepare($query)->execute();
        $query = "ALTER TABLE articles ADD CONSTRAINT art_relation FOREIGN KEY (id_cat) REFERENCES categories (id) ON DELETE CASCADE ON UPDATE CASCADE;";
        $stm = $this->pdo->prepare($query)->execute();
        $query = "ALTER TABLE trolleybascket ADD CONSTRAINT troll_relation_art FOREIGN KEY (id_art) REFERENCES articles(id) ON DELETE CASCADE ON UPDATE CASCADE;";
        $stm = $this->pdo->prepare($query)->execute();
    }
}
