<?php

declare(strict_types=1);


require_once('BaseController.php');

abstract class BaseProductController extends BaseController
{
    /**
     * /Product/put/ produtc
     */
    public function setProducto(string $art_title, string $art_desc, string $art_evalu, string $id_cat, string $create_at, int $is_active)
    {
        $query = "INSERT INTO articles (art_title, art_desc, art_evalu, id_cat, create_at, visited_at, is_active  ) VALUES (?,?,?,?,?,?,?)";
        return $this->pdo->prepare($query)->execute([$art_title, $art_desc, $art_evalu, $id_cat, $create_at, $create_at, $is_active]);
    }

    /**
     * /Product/put/ produtc/{id}
     */
    public function updateProducto(string $art_title, string $art_desc, int $art_evalu, int $id_cat, string $create_at, int $is_active, int $id)
    {
        $query = "UPDATE articles SET art_title = ?, art_desc = ?, art_evalu = ?, id_cat = ?, visited_at = ? , create_at = ?, is_active = ? WHERE id = ? ";
        return $this->update($query, [$art_title, $art_desc, $art_evalu, $id_cat, $create_at, $create_at, $is_active, $id]);
        //return $this->pdo->prepare($query)->execute([$art_title, $art_desc, $art_price, $art_evalu, $art_sku, $id_cat, $create_at, $is_active, $id]);
    }
    /**
     * /Product/detail/ produtc
     */
    public function getProducto(int $id)
    {
        $query = "SELECT * FROM articles WHERE id = ?";
        $stm = $this->pdo->prepare($query);
        $stm->execute([$id]);
        $data = $stm->fetchAll();
        return $data;
    }




    public function __destruct()
    {
        $this->pdo =  null;
    }
}
