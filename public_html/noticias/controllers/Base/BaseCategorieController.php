<?php

declare(strict_types=1);

require_once('BaseController.php');


abstract class BaseCategorietController extends BaseController
{   
    /**
     * inserta una categoria
     */
    
    public function setCategorie(string $name, string $file, int $isActive)
    {
        $query = "INSERT INTO categories (name, image, is_active) VALUES (?,?,?)";
        $this->pdo->prepare($query)->execute([$name, $file, $isActive]);
    }

 
    public function getOneCategorie(int $id)
    {
        $query = "SELECT name, is_active FROM categories WHERE id = ?";
        $stm = $this->pdo->prepare($query);
        $stm->execute([$id]);
        $data = $stm->fetchAll();
        return $data;
    }
    public function updateCategorie(string $name, string $file, int $is_active, int $id)
    {
        $query = "UPDATE categories SET name = ?, image = ?, is_active = ? WHERE id = ? ";
        //return $this->update($query, [$name, $is_active, $id]);
        return $this->pdo->prepare($query)->execute([$name, $file, $is_active, $id]);
    }
    public function __destruct()
    {
        $this->pdo =  null;
    }
}
