<?php

declare(strict_types=1);

class MediaType
{
    public int $id;
    public string $m_type;
}
