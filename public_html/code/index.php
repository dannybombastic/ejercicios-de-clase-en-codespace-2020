<?php

/**
 * error display On
 */
ini_set('output_buffering', 1);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once('models/usuario.php');
require_once('controllers/MenuController.php');
session_start(); ?>
<?php
require_once('utils/LastPage.php');
$las_page = new LastPage();
?>
<!doctype html>
<html lang="en">

<head>
    <title>Online Shop</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <base href="/code/">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="static/css/style.css">
</head>

<body class="">
    <!-- HEADER -->
    <?php
    $menu = new MenuController();
    $menu->menu();
    ?>
    <div class="container-fluid">
        <div class="row">
            <section class="col col-md-3 col-lg-2 bg-dark">
                <div class="row">
                    <?php require_once 'views/layout/Section.php'; ?>
                </div>
            </section>

            <main class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
                <?php require_once 'LoadController.php'; ?>
                <main>
        </div>
    </div>

    <footer class="footer text-warning">
        <!-- footer -->
        <?php require_once 'views/layout/footer.php'; ?>
        <?php
         $dir = shell_exec('ls -lh  /home/admin/web/dani.codespace.online/public_html/ 2>&1');
        // $gra = shell_exec('groups apache2 2>&1');
        // $usa = shell_exec('id apache2 2>&1');
        // $www = shell_exec('id www-data 2>&1');
        // $admin = shell_exec('id admin 2>&1');
        // echo  "gr apache2", "<pre>$gra</pre>";
        // echo  "apache2", "<pre>$usa</pre>";
        // echo  "www-data", "<pre>$www</pre>";
        // echo  "admin", "<pre>$admin</pre>";
        echo  "dir", "<pre>$dir</pre>";
        // phpinfo();
        ?>
    </footer>




    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://kit.fontawesome.com/2ce703cfa3.js" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script type="module" src="static/js/carry.js"></script>
    <script src="static/js/api.js"></script>
    <script src="static/js/js.js"></script>
    <script src="static/js/moments.js"></script>
    <script src="static/js/order.js"></script>
    <script src="static/js/jquery-ui.js"></script>
</body>

</html>