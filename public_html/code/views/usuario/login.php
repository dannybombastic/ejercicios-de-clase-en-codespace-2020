<div class="container fluid text-warning">
    
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="alert alert-<?= $success ?> alert-dismissible fade show" role="alert">
            <strong>Holy guacamole!</strong> You should check in on some of those fields below.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
    
    <div class="row justify-content-center p-3">
       <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
       <form class="form-signin pt-5 text-center" action="User/login/" method="POST">
            <img class="mb-2" src="https://getbootstrap.com/docs/4.0/assets/brand/bootstrap-solid.svg" alt="" width="72" height="72">
            <h1 class="h3 mb-3 font-weight-normal">Sign in Form</h1>
            <label for="inputEmail" class="sr-only p-2">Email address</label>
            <input type="email" name="email" id="inputEmail" class="form-control" placeholder="@Email address" required autofocus>
            <br>
            <label for="inputPassword" class="sr-only p-2">Password</label>
            <input type="password"  name="password" id="inputPassword" class="form-control" placeholder="Password" required>
            <div class="checkbox mb-3">
                <label>
                    <input type="checkbox" value="remember-me"> Remember me
                </label>
            </div>
            <button class="btn btn-lg btn-block" type="submit">Sign in</button>
            <p class="mt-5 mb-3 text-center text-muted">© 2017-2018</p>
        </form>
       </div>
    </div>
</div>