<?php
require_once('./controllers/HomeController.php');
$categories = new HomeController();
$categories = $categories->getCategorie();
?>
<div class="container-fluid">
    <div class="justify-content-center text-center">
        <div class=" col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="row section">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <a href="/code/" class="btn btn-lg mb-4"> Back</a>
            </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <form action="/code/" class="form my-2 pl-4" method="POST">
                        <input class="form-control mr-sm-2" type="search" name="condition" placeholder="Search" id="auto-complete" aria-label="Search">
                        <button class="btn btn mt-5" type="submit"> <i class="fas fa-search"></i> Search</button>
                    </form>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 pt-5 pb-5">
                    <h3><i class="fas text-warning fa-sort"></i> Order </h3>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="form-check mt-3 form-check-inline">
                            <button class="btn m-2" onclick="order_by_date()">By date</button>
                            <button class="btn m-2" onclick="order_by_price()">By price</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  pt-2 pb-5">
                    <label for="exampleFormControlSelect1">
                        <h3>Categories</h3>
                    </label>
                    <form action="/code/" class="form my-2 my-lg-0" method="POST">
                        <select class="form-control mt-3" name="condition" id="exampleFormControlSelect1">
                            <?php foreach ($categories as $cat) : ?>
                                <option><?= $cat['name'] ?></option>
                            <?php endforeach ?>
                            <option>All</option>
                        </select>
                        <button class="btn mt-5 pt-2" type="submit">Search</button>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>