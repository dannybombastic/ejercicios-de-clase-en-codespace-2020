<header>
    <nav class="navbar border-bottom border-dark text-warning navbar-expand-lg navbar-light p-4">
        <a class="navbar-brand" href="http://dani.codespace.online/code/"><img id="logo" src="static/img/logo.png" alt="" height="75"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <?php if ($isAdmin) : ?>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle text-danger" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Actions Admin Forms
                        </a>
                        <div class="dropdown-menu bg-dark" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item text-warning" href="Producto/putForm/">Add Product</a>
                            <a class="dropdown-item text-warning" href="Producto/updateForm/">Update Product</a>

                            <div class="dropdown-divider"></div>

                            <a class="dropdown-item text-warning" href="Categorie/putForm/">Add Categorie</a>
                            <a class="dropdown-item text-warning" href="Categorie/updateForm/">Update Categorie</a>

                            <div class="dropdown-divider"></div>

                            <a class="dropdown-item text-warning" href="MultiMedia/typePut/">Add Multimedia Type </a>
                            <a class="dropdown-item text-warning" href="MultiMedia/typeUpdate/">Update Multimedia Type </a>
                            <div class="dropdown-divider"></div>

                            <a class="dropdown-item text-warning" href="MultiMedia/mediaPut/">Add Multimedia </a>
                            <a class="dropdown-item text-warning" href="MultiMedia/mediaUpdate/">Update Multimedia </a>

                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item text-warning" href="User/register/">Add User </a>
                            <a class="dropdown-item text-warning" href="MultiMedia/mediaPut/">Update user</a>
                        </div>
                    </li>
                <?php endif; ?>
                <li class="nav-item text-warning">
                    <h3>Shop Online</h3>
                </li>
                <li>
                </li>
            </ul>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3">
             <my-list class="mt-3 pt-3"></my-list>
            </div>



            <button type="button" id="filters" class="btn m-2 btn-default">
                <i class="fas fa-search"></i> Filters
            </button>
            <div class="dropdown">
                <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    User Actions
                </button>
                <div class="dropdown-menu bg-dark" aria-labelledby="dropdownMenuButton">
                    <?php if (!is_null($user)) : ?>
                        <span class="p-2 text-warning text-capitalize"> <?= $user->user_name ?></span>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item text-warning" href="User/logout/">Log out</a>
                        <a class="dropdown-item text-warning" href="User/register/">Update Profile</a>
                    <?php else : ?>
                        <a class="dropdown-item text-warning" href="User/login/"><i class="fas text-warning fa-sign-in-alt"></i> Login </a>
                        <a class="dropdown-item text-warning" href="User/register/"><i class="fas text-warning fa-user-alt"></i> Registe </a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </nav>
</header>