 <div class="parallax row justify-content-lg-center p-3 text-warning">

     <div class="col-xs-4 col-sm-4 col-md-4 col-lg-6">
     
        <div class="alert alert-<?= $success ?> alert-dismissible fade show" role="alert">
             <strong>Congrats!</strong> You Product have been saved.
             <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                 <span aria-hidden="true">&times;</span>
             </button>
         </div>
        
         <?php if(empty(Conf::$QUERY)): ?>
            <div class="text-center col-xs-12 col-sm-12 col-md-12 col-lg-12">
             <h3>Create Product</h3>
         </div>
        <form action="Producto/putForm/" method="POST">
             <div class="form-group">
                 <label for="title">Title</label>
                 <input type="text" class="form-control" id="title" name="title" placeholder="..." required>
             </div>
             <div class="form-group">
                 <label for="description">Description</label>
                 <input type="text" class="form-control" id="description" name="description" placeholder="..." required>
             </div>
             <div class="form-group form-check-inline">
                 <label class="pr-3" for="price">Price</label>
                 <input type="number" class="form-control" id="price" name="price" placeholder="25.20" required>
             </div>
             <div class="form-group form-check-inline">
                 <label class="pr-3" for="star">Star</label>
                 <input type="number" class="form-control" id="star" maxlength="2" max="10" name="star" placeholder="1-10" required>
             </div>
             <div class="form-group form-check-inline">
                 <label class="pr-3" for="sku">Sku</label>
                 <input type="number" class="form-control" id="sku" name="sku" max="999" maxlength="3" placeholder="99" required>
             </div>
             <div class="form-group">
                 <label for="categories">Categories</label>
                 <select class="form-control" name="categorie" id="categories" required>
                     <?php foreach ($categories as $value) : ?>
                         <option value="<?= $value['id'] ?>"><?= $value['name'] ?></option>
                     <?php endforeach; ?>
                 </select>
             </div>
             <div class="p-2 col-xs-12 col-sm-12 col-md-12 col-lg-12">
                 <div class="form-check form-check-inline">
                     <input class="form-check-input" type="radio" name="active" id="inlineRadio1" value="1">
                     <label class="form-check-label" for="inlineRadio1"> Active</label>
                 </div>
                 <div class="form-check form-check-inline">
                     <input class="form-check-input" type="radio" name="active" id="inlineRadio2" value="0">
                     <label class="form-check-label" for="inlineRadio2"> No Active</label>
                 </div>
             </div>
             <button type="submit" class="btn btn-block">Save prosuct</button>
         </form>
       <?php endif; ?>
       <?php if(!empty(Conf::$QUERY)):  ?>
        <div class="text-center col-xs-12 col-sm-12 col-md-12 col-lg-12">
             <h3>Udapte Product ID <?= Conf::$QUERY ?></h3>
         </div>
        <form action="Producto/updateForm/<?= Conf::$QUERY ?>" method="POST">
             <div class="form-group">
                 <label for="title">Title</label>
                 <input type="text" class="form-control" id="title" value="<?= $pro['art_title'] ?>" name="title" placeholder="..." required>
             </div>
             <div class="form-group">
                 <label for="description">Description</label>
                 <input type="text" class="form-control" id="description" value="<?= $pro['art_desc'] ?>" name="description" placeholder="..." required>
             </div>
             <div class="form-group form-check-inline">
                 <label class="pr-3" for="price">Price</label>
                 <input type="number" class="form-control" id="price" value="<?= $pro['art_price'] ?>" name="price" placeholder="25.20" required>
             </div>
             <div class="form-group form-check-inline">
                 <label class="pr-3" for="star">Star</label>
                 <input type="number" class="form-control" id="star" maxlength="2" max="10" value="<?= $pro['art_evalu'] ?>" name="star" placeholder="1-10" required>
             </div>
             <div class="form-group form-check-inline">
                 <label class="pr-3" for="sku">Sku</label>
                 <input type="number" class="form-control" id="sku" name="sku" max="999" maxlength="3" value="<?= $pro['art_sku'] ?>" placeholder="99" required>
             </div>

             <div class="form-group">
                 <label for="categories">Categories</label>
                 <select class="form-control" name="categorie" value="" id="categories" required>
                     <?php foreach ($categories as $value) : ?>
                         <option value="<?= $value['id'] ?>"
                         <?php if ($pro['id_cat'] == $value['id']) : ?> <?= 'selected' ?> <?php endif; ?> >
                          <?= $value['name']?>  </option>
                     <?php endforeach; ?>
                 </select>
             </div>
             <div class="p-2 col-xs-12 col-sm-12 col-md-12 col-lg-12">
                 <div class="form-check form-check-inline">
                     <input class="form-check-input" type="radio" name="active" id="inlineRadio1" value="1"  <?php if ($pro['is_active'] == 1) : ?> <?= 'checked' ?> <?php endif; ?>>
                     <label class="form-check-label" for="inlineRadio1"> Active</label>
                 </div>
                 <div class="form-check form-check-inline">
                     <input class="form-check-input" type="radio" name="active" id="inlineRadio2" value="0"  <?php if ($pro['is_active'] == 0) : ?> <?= 'checked' ?> <?php endif; ?>>
                     <label class="form-check-label" for="inlineRadio2">  No Active</label>
                 </div>
             </div>
             <button type="submit" class="btn btn-block">Update Product</button>
         </form>
         <?php endif; ?>
        
     </div>
 </div>