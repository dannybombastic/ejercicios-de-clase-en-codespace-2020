<div class="parallax row justify-content-lg-center pt-5 pb-5">
    <div class="text-center text-warning col-sm-12 col-md-12 col-lg-12">
        <h1>Detail <?= $pro['title'] ?> ID <?= Conf::$QUERY ?></h1>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
        <div class="card w-100 text-warning">
            <img class="" src="static/img/<?=$pro['src'] ?>" alt="Card image cap" height="300" >
            <div class="card-body">
                <h5 class="card-title"><?= $pro['title'] ?></h5>
                <p class="card-text"><?= $pro['art_desc'] ?></p>
            </div>
            <div class="card-footer">
                Date: <small class="text-muted"><?= $pro['create_at'] ?></small>
                Price: <small class="text-info"><?= $pro['price'] ?> €</small>
                Categorie: <small class="text-info"><?= $pro['name'] ?> </small>
            </div>
        </div>
    </div>
</div>