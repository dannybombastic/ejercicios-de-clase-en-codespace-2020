<?php if (is_null($cat)) : ?>
    <div class="parallax row justify-content-lg-center p-5">
        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-6">
            <div class="text-center col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <h1>Udapte Categorie ID <?= Conf::$QUERY ?></h1>
            </div>

            <form action="/Categorie/detail/<?= Conf::$QUERY ?>" method="POST">
                <div class="form-group">
                    <label for="title">Name</label>
                    <input type="text" class="form-control" name="name" id="name" placeholder="..." required>
                </div>

                <div class="p-2 col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="active" id="inlineRadio1" value="1">
                        <label class="form-check-label" for="inlineRadio1"> Active</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="active" id="inlineRadio2" value="0">
                        <label class="form-check-label" for="inlineRadio2"> No Active</label>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary btn-block">Submit</button>
            </form>
        </div>
    </div>
<?php endif; ?>

<?php if (!is_null($cat)) : ?>
    <div class="parallax row justify-content-lg-center p-5">
        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-6">
            <div class="text-center col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <h1>Udapte Categorie ID <?= Conf::$QUERY ?></h1>
            </div>

            <form action="/Categorie/detail/<?= Conf::$QUERY ?>" method="POST">
                <div class="form-group">
                    <label for="title">Name</label>
                    <input type="text" class="form-control" value="<?= $cat['name'] ?>" name="name" id="name" placeholder="..." required>
                </div>

                <div class="p-2 col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="active" id="inlineRadio1" value="<?= $cat['is_active'] ?>">
                        <label class="form-check-label" for="inlineRadio1"> Active</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="active" id="inlineRadio2" value="<?= $cat['is_active'] ?>">
                        <label class="form-check-label" for="inlineRadio2"> No Active</label>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary btn-block">Submit</button>
            </form>
        </div>
    </div>
    <?php endif; ?>