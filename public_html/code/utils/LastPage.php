<?php

declare(strict_types=1);

class LastPage
{
    protected string $last_page = '';
    function __construct()
    {
        if (isset($_SERVER['HTTP_REFERER'])) {
            if ($this->containX((string) $_SERVER['HTTP_REFERER'] ?? '/', "http://dani.codespace.online/code/User/login/")) {

                $_SESSION['last_page'] = $_SERVER['HTTP_REFERER'] ?? '/';

                $this->last_page = $_SESSION['last_page'] ?? "http://dani.codespace.online/code/";
            }
        }
    }

    private function containX(string $url = ' ', string $regx = ' ')
    {
        return strcasecmp($url, $regx) !== 0;
    }

    /**
     * Get the value of last_page
     */
    public function getLast_page()
    {
        return $this->last_page;
    }

    /**
     * Set the value of last_page
     *
     * @return  self
     */
    public function setLast_page($last_page)
    {
        $this->last_page = $last_page;
        return $this;
    }
}
