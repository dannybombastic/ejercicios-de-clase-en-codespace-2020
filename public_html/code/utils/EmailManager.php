<?php

declare(strict_types=1);

include_once('models/usuario.php');

class EmailManager
{
    const SUBJECT = 'Wellcome to online shop';
    const FROM = 'admin@vps806808.ovh.net';
    const REPLYTO = 'noreply@noreply.com';
    private string $user = '';
    private string $email = '';
    public string $token = '';
    private string $headers = '';
    private string $message = '';

    public function __construct(string $user, string $email, string $token)
    {
        $this->token = $token;
        $this->user = $user;
        $this->email = $email;
    }



    public function setHeaders()
    {
        $this->headers = "To: $this->user, <$this->email>" . "\r\n";
        $this->headers .= "From: <" . self::FROM . ">\r\n";
        $this->headers .= "CC: dani@example.com" . "\r\n";
        $this->headers .= 'MIME-Version: 1.0' . "\r\n";
        $this->headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
        return $this->headers;
    }

    public function getMessage()
    {
        $this->message = <<<EOD
           <!DOCTYPE html>
           <html>
           <body>
           <head>
           <meta charset="utf-8">
           <title>Recordatorio de cumpleaños para Agosto</title>
           </head>
           
           <table rules="all" align="center" style="border-color: #666;" cellpadding="10">
             <tbody style="padding:15px;">
               <tr><td><img src="dani.codespace.online/code/static/img/logo.png" height="50" width="50" alt="Website logo" /> </td></tr>
               <tr style='background: #eee;'><td><strong>Name:</strong> </td></tr>
               <tr><td><strong>Hi $this->user </strong> </td><td></td></tr>
               <tr><td><strong>You have to activae your account</strong> </td></tr>
               <tr><td><strong>URL To Activate your Account :</strong> </td></tr>
               <tr><td><a href="http://dani.codespace.online/code/User/activateUserByEmail/$this->token"> Click to activate</a></td></tr>
             </tbody>
           </table>
           </body>
           </html>
          EOD;
        return $this->message;
    }


    public function sendMessage()
    {
        $success = mail($this->email, self::SUBJECT, $this->getMessage(), $this->setHeaders());
        if (!$success) {
            return $errorMessage = error_get_last()['message'];
        }

        
        return $this->email;
    }
}
