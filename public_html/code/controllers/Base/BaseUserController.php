<?php

declare(strict_types=1);
require_once('models/usuario.php');
require_once('BaseController.php');
require_once('utils/EmailManager.php');

class BaseUserController extends BaseController
{

    private EmailManager $emailManager;
    /**
     * Recupera un usuario con toda la info
     */
    public function geCredential(string $email)
    {
        $query = "SELECT * FROM user_profile WHERE user_email = ?";
        $stm = $this->pdo->prepare($query);
        $stm->execute([$email]);

        if ($stm) {
            return $stm->fetchAll();
        }

        return null;
    }


    /**
     * Funcion para registrar un usuario
     */
    public function registerUser(array $form)
    {
        $query = <<<EOD
        INSERT INTO `user_profile`
         (`user_dni`,
         `user_name`,
         `user_addr`,
         `user_email`,
         `user_password`,
         `gender`,
         `user_role`,
         `create_at`,
         `is_active`)
         VALUES
         (?,?,?,?,?,?,?,?,?);
      EOD;
        $stm = $this->pdo->prepare($query);
        $stm->execute($form);
        if ($stm) {
            $this->emailManager = new EmailManager($form[0], $form[3], $this->createToken());
            $this->activateUser([$form[0], $this->emailManager->token]);
            $this->emailManager->sendMessage();
            return true;
        }
        return false;
    }

    public function activateUser(array $user_data)
    {
        $query = <<<EOD
        INSERT INTO `activate_accounts`
         (`user_dni`,
          `token`)
          VALUES
          (?,?);
      EOD;
        $stm = $this->pdo->prepare($query);
        $stm->execute($user_data);
        if ($stm) {
            return true;
        }
        return false;
    }

    public function createToken()
    {
        $token = openssl_random_pseudo_bytes(16);
        return  bin2hex($token);
    }


    public function activateAfterSubbmit($values)
    {
        $values = $this->getIdFromToken($values);
       
        if ($values) {
            $query = "UPDATE user_profile SET is_active = 1 WHERE user_dni = ?";
            $stm = $this->pdo->prepare($query)->execute([$values['user_dni']]);
            return $stm;
        }
        return false;
    }

    public function getIdFromToken($var)
    {
        $query = "SELECT user_dni FROM activate_accounts WHERE token = ?";
        $stm = $this->pdo->prepare($query);
        $stm->execute([$var]);

        if ($stm) {
            return $stm->fetch();
        }

        return false;
    }
}
