<?php
require_once('Base/BaseCategorieController.php');
require_once('Config/Conf.php');


class CategorieController extends BaseCategorietController
{
    /**
     * Accion put para las categorias
     *  
     */
    public function putForm()
    {
        $success = 'success invisible';
        $cat = [
            "name" => "No Name",
            "is_active" => false
        ];


        if (Conf::$QUERY === '') {
            if (count($_POST) > 0) {
                $success =   $this->serializerCategorie();
            }
        }

        require_once 'views/Categorie/put.php';
    }

    /**
     * Accion update para las categorias
     * si $QUERY viene con algun id recupero los datos para un update
     */
    public function updateForm()
    {
        $success = 'success invisible';
        $cat = [
            "name" => "No Name",
            "is_active" => false
        ];

        if (Conf::$QUERY !== '') {
            $categorie = $this->getOneCategorie((int) Conf::$QUERY);
            if (count($categorie) > 0) {
                $cat = $categorie[0];
            }
            if (count($_POST) > 0) {
                $success =  $this->updateRequest();
                $categorie = $this->getOneCategorie((int) Conf::$QUERY);
                $cat = $categorie[0];
            }
        }

        require_once 'views/Categorie/put.php';
    }

    /**
     * Guardamos la categoria en base de datos
     * 
     */
    private function serializerCategorie()
    {
        if (count($_POST) > 0 && isset($_POST['name']) && isset($_POST['active'])) {
            $this->setCategorie($_POST['name'], (int) $_POST['active']);
            return 'success visible';
        } else {
            return 'danger visible';
        }
    }


    /**
     * Actualizamos una categoria en base de datos
     */
    public function updateRequest()
    {
        if (count($_POST) > 0 && isset($_POST['name']) && isset($_POST['active'])) {

            $this->updateCategorie($_POST['name'], (int) $_POST['active'], (int) Conf::$QUERY);
            return 'success visible';
        } else {
            return 'danger visible';
        }
    }
}
