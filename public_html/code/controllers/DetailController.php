<?php

declare(strict_types=1);

require_once('Base/BaseDetailController.php');

class DetailController extends BaseDetailController
{
    

    public function product()
    {
        $pro = $this->fetchOneProduct((int) Conf::$QUERY ?? [0]);
        require_once('views/producto/detail.php');
    }
}



