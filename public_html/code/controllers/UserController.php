<?php

declare(strict_types=1);

require_once('Security/Encoder.php');
require_once('models/usuario.php');
require_once('utils/LastPage.php');

require_once('Base/BaseUserController.php');

class UserController extends BaseUserController
{
    /**
     * Funcion del controlador para la acion de login /User/login/
     * 
     */
    public function login()
    {
        $success = 'success invisible';
        if (isset($_POST['email']) && isset($_POST['password'])) {

            $success =  $this->LoginUser((string) $_POST['email'], (string) $_POST['password']);
        }

        require_once('views/usuario/login.php');
    }
    /**
     * Funcion del controlador Para la accion logout /user/logout/ 
     * 
     */
    public function logout()
    {
        $referer = 'http://dani.codespace.online/code/';
        if (isset($_SESSION['user'])) {
            unset($_SESSION['user']);
            $this->Redirect($referer);
        }

        // require_once('views/usuario/login.php');
    }
    /**
     * Funcion del controlador para la accion /User/register/
     */
    public function register()
    {
        $success = 'success invisible';
        if (isset($_POST['user_name']) && isset($_POST['user_email']) && isset($_POST['user_password'])) {

            $fecha = date_create();
            $fecha = $fecha->format('Y-m-d H:i:s');
            $success = $this->singInUser(
                [
                    $_POST['user_dni'],
                    $_POST['user_name'],
                    $_POST['user_addr'],
                    $_POST['user_email'],
                    Encoder::encooder($_POST['user_password']),
                    $_POST['user_gender'],
                    $_POST['user_role'],
                    $fecha,
                    0
                ]
            );
        }


        require_once('views/usuario/registro.php');
    }



    /**
     * Funcion para logear a un usuario y redirigirlo a la pagina donde estaba
     * 
     */
    public function LoginUser($email, $password)
    {
        $verify = $this->geCredential($email);
        if (!is_null($verify[0])) {
            if (Encoder::verifyUserPass($password, $verify[0]['user_password'])) {

                if (isset($_SESSION['last_page'])) {

                    $referer = $_SESSION['last_page'];
                    $user = $this->fetchSigleUser($verify[0]['user_dni']);

                    $_SESSION['user'] = $user;

                    $this->Redirect($referer);

                    return 'success visible';
                }
            }
        }
        return 'danger visible';
    }


    /**
     * Funcion para registrar a un usuario
     */
    public function singInUser(array $data)
    {
        if ($this->registerUser($data)) {


            return 'success visible';
        }
        return 'danger visible';
    }

    /**
     * Header redirect como a mi no me funciona ponga donde lo ponga yo uso este truqillo
     */

    function Redirect($url)
    {
        echo "<script> location.href = '$url'; </script>";
        //header("Location: $url");
        exit;
    }



    function activateUserByEmail()
    {
        if (isset($_GET['q']) && !empty($_GET['q'])) {
             echo $_GET['q'];
             print_r($this->activateAfterSubbmit($_GET['q']));
        }
       
        
    }
}
