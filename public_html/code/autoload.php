<?php

function controllers_autoload($classname){
	$fileName = 'controllers/' . $classname . '.php';
	if (is_file($fileName)) include_once $fileName;	
}

spl_autoload_register('controllers_autoload');