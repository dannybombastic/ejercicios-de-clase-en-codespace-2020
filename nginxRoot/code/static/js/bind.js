

function getData(main_list, condition_list) {
    const data = main_list;
    const condition = condition_list;
    var container = document.getElementById('main-row');
    var container_botton = document.getElementById('main-bottom');

    if (data.length > 0) {
        data.forEach(element => {
            let elemento = `<div class="custom-item col-sm-12 col-md-6 col-lg-4" data-id="${element.id}" data-date="${element.create_at}"  data-title="${element.art_title}" data-cate="${element.name}" data-price="${element.art_price}">
    <div class="card-deck p-3">
        <div class="card">
            <img class="card-img-top" src="/img/${element.src}" alt="Card image cap">
            <div class="card-body">
                <span class="h5 w-50 card-title">${element.art_title}</span>
                <button  class="h5 p-2 btn btn-outline-info card-title text-right float-right">${element.art_price}</button>
                <div class="text-card w-100">
                    <p class="card-text">${element.art_desc}</p>
                </div>
                <div class="row">
                  <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 p-1">
                    <button   data-id="${element.id}" data-src="${element.src}"  data-title="${element.art_title}" data-price="${element.art_price}" class="btn-block text-white btn bg-dark" onclick="buyIt(this)" >Buy it</button>
                  </div>
                  <div class="col-xs-3 col-sm-3 col-md-3 col-lg-4 p-1">
                    <a href="/Detail/product/${element.id}" class="text-white btn btn-block bg-dark">See detail</a>
                  </div>
                </div>
               
            </div>
            <div class="card-footer">
                <small class="text-muted">Last updated ${element.create_at} Categorie:</small>  <small id="helpSrc" class="text-info"> ${element.name} </small>
            </div>
        </div>
    </div>
</div>`;

            container.innerHTML += elemento;

        });


    } else {
        container.innerHTML += '<H1>No data to show</H1>';
    }


    if (condition.length > 0) {
        condition.forEach(element => {
            let elemento = `<div class="custom-item col-sm-12 col-md-6 col-lg-4" data-id="${element.id}" data-date="${element.create_at}"  data-title="${element.art_title}" data-cate="${element.name}" data-price="${element.art_price}">
    <div class="card-deck p-3">
        <div class="card">
            <img class="card-img-top" src="/img/${element.src}" alt="Card image cap">
            <div class="card-body">
                <span class="h5 w-50 card-title">${element.art_title}</span>
                <button class="h5 p-2 btn btn-outline-info card-title text-right float-right">${element.art_price}</button>
                <div class="text-card w-100">
                    <p class="card-text">${element.art_desc}</p>
                </div>
                <div class="row">
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 p-1">
                        <a href="#" class="btn-block text-white btn bg-dark">Buy it</a>
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-4 p-1">
                    <a href="#" class="text-white btn btn-block bg-dark">See detail</a>
                    </div>
                </div>
               
            </div>
            <div class="card-footer">
                <small class="text-muted">Last updated ${element.create_at} Categorie:</small>  <small id="helpSrc" class="text-info"> ${element.name} </small>
            </div>
        </div>
    </div>
</div>`;

            container_botton.innerHTML += elemento;

        });
    } else {
        container.innerHTML += '<H1>No data to show</H1>';
    }



}




function buyIt(el) {
 

    let obj = {
         id: Number(el.dataset.id),
         img: el.dataset.src,
         sht_name: el.dataset.title,
         quantity: 1,
         total: Number(el.dataset.price)
     };
     var list = document.getElementsByTagName("my-list")[0];
     
     list.art = obj;

}