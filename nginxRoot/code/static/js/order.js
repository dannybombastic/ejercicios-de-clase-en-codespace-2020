$(document).ready(function () {




});

var asc_date = true;
function order_by_date() {
    asc_date = !asc_date;
    $( "#main-row > .custom-item" ).hide( "highlight" , 800, function(){ $( ".custom-item" ).show( "drop");});
    let list = $('#main-row .custom-item').get();
   
    list.sort(function (a, b) {
        if (asc_date) {
            return moment($(a).data("date"), 'YYYY-MM-DD:h:mm:ss') < moment($(b).data("date"), 'YYYY-MM-DD:h:mm:ss') ? 1 : -1;
        } else {
            return moment($(a).data("date"), 'YYYY-MM-DD:h:mm:ss') > moment($(b).data("date"), 'YYYY-MM-DD:h:mm:ss') ? 1 : -1;
        }
    });
    
    $(list).appendTo("#main-row");

}



var asc_money = true;
function order_by_price() {
        $( "#main-row > .custom-item" ).hide( "fade" , 800, function(){ $( ".custom-item" ).show( "fade");});
    asc_money = !asc_money;

    let list = $('#main-row .custom-item').get();
    console.log("hola", list );
    list.sort(function (a, b) {
        if (asc_money) {
            return parseFloat($(a).data("price")) < parseFloat($(b).data("price")) ? 1 : -1;
        } else {
            return parseFloat($(a).data("price")) > parseFloat($(b).data("price")) ? 1 : -1;
        }
    });

    $(list).appendTo("#main-row");

}