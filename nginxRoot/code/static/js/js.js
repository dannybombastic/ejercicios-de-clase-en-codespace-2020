$(document).ready(function () {

    var item = document.getElementById('main-row');
    var item_bottom = document.getElementById('main-pseudo');
  
    $( "#logo" ).hide( "explode" , 800, function(){ $( "#logo" ).show( "explode");});
    if (item_bottom) {
        horizAnim();
        item_bottom = item_bottom.firstElementChild;
        $('#left-bottom').click(function (e) {
            e.preventDefault();
            $('#main-pseudo .row').animate({
                scrollLeft: '-=500'
            }, 'slow');
        });
        $('#right-bottom').click(function (e) {
            e.preventDefault();
            $('#main-pseudo .row').animate({
                scrollLeft: '+=500'
            }, 'slow');;
        });
    }
    if (item) {
        $('#left').click(function (e) {
            e.preventDefault();
            $('#main-row').animate({
                scrollLeft: '-=500'
            }, 'slow');
        });
        $('#right').click(function (e) {
            e.preventDefault();
            $('#main-row').animate({
                scrollLeft: '+=500'
            }, 'slow');
        });
    }



    $("#filters").click(function (e) {
        e.preventDefault();
        changeSize();
    });

    initSize();
});

/**
 * localstorage  filter menu state permanence
 */
function setSideMenu(state = false) {
    let option = { hide: state };
    localStorage.setItem('hide', JSON.stringify(option));
}

/**
 * 
 * get state of filter menu
 * 
 */
function getSideMenu() {
    let option = JSON.parse(localStorage.getItem('hide'));
    if (option !== null) {
        return option.hide;
    }
    return false;
}

/**
 * 
 * chage column size according to the filter menu state
 * 
 */

function changeSize() {

    if ($('section').is(':visible')) {
        $( "section" ).toggle( "fold" );
         
        $("main").removeClass('col-lg-10');
        $('main').addClass('col-lg-12');
        
    } else {
       
        $( "section" ).toggle( "fold" );
        $("main").removeClass('col-lg-12');
        $('main').addClass('col-lg-10');

    }
    setSideMenu(!getSideMenu());

}

/**
 * 
 * initialize the filter menu localstorage nd state
 * 
 */

function initSize() {

    if (JSON.parse(localStorage.getItem('hide')).hide == true) {

        if (!$('section').is(':visible')) {
            $( "section" ).toggle( "fold" );
            $("main").removeClass('col-lg-10');
            $('main').addClass('col-lg-12');
        }
    } else {
        if ($('section').is(':visible')) {
            $( "section" ).toggle( "fold" );
            $("main").removeClass('col-lg-10');
            $('main').addClass('col-lg-12');
        }
    }
}

function horizAnim() {

    $('#main-row').animate({
        scrollLeft: '+=500'
    }, 'slow');
    $('#main-row').animate({
        scrollLeft: '-=500'
    }, 'slow');
    $('#main-pseudo .row').animate({
        scrollLeft: '+=300'
    }, 'slow');
    $('#main-pseudo .row').animate({
        scrollLeft: '-=300'
    }, 'slow');
}



