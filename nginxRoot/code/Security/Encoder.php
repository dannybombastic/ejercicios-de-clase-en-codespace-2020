<?php

declare(strict_types=1);


class Encoder
{
  const OPTIONS = [
    'cost' => 12,
  ];

  /**
   * codificamos el pass del user y lo devolbemos para serializar
   */
  public static function encooder($password)
  {
    return password_hash($password, PASSWORD_BCRYPT, self::OPTIONS);
  }

  /**
   * Verificamos que sean iguales
   */
  public static function verifyUserPass($password, $hash)
  {
    return password_verify($password, $hash);
  }
}
