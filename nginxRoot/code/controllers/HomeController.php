<?php

declare(strict_types=1);

require_once('Base/BaseHomeController.php');

class HomeController extends BaseHomeController
{
     /**
      * Controlador del Home de la pagina que es solo una pequeña seccion de la pagina al completo
      */
     public function home()
     {
          /**
           * Recuperamos todo y lo paso en dos variables ($articles $condition) al contexto del templta Home.php
           */
          $data = $this->getAll();
          $articles = $data['base_list'];
          $condition = $data['condition_list'];
          require_once('./views/layout/Home.php');
     }
}
