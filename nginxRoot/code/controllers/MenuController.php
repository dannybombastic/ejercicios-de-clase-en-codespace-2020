<?php

declare(strict_types=1);
require_once('models/usuario.php');
require_once('Base/BaseController.php');

class MenuController extends BaseController
{

    public function menu()
    {
        /**
         * Si viene un user lo agragamos al contxto del header.php vista
         */
        $user = null;
        $isAdmin = false;
        if (isset($_SESSION['user'])) {

            $user = $this->fetchSigleUser($_SESSION['user']->user_dni);
          
            if ($user->user_role == '2') {
                $isAdmin = true;
            }
        }
        /**
         * Si viene un user y es admin cambiamos cosas en el header.php vista
         */
    
        require_once('views/layout/header.php');
    }
}
