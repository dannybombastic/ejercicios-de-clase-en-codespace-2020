<?php

declare(strict_types=1);

require_once('BaseController.php');


class BaseDetailController extends BaseController
{

    public function fetchOneProduct(int $id)
    {
        $query = <<<EOD
        SELECT 
        ca.name as name,
        mu.src as src,
        ar.art_title as title,
        ar.id,
        ar.art_price as price,
        ar.art_desc,
        ar.art_evalu,
        ar.create_at,
        ar.is_active
        FROM
        articles AS ar
            JOIN
        multimedia AS mu ON ar.id = mu.id_art
            JOIN
        categories AS ca ON ar.id_cat = ca.id 
        WHERE ar.id = ?;
        EOD;
        $stm = $this->pdo->prepare($query);
        $stm->execute([$id]);
        $data = $stm->fetch();
        return $data;
    }
}
