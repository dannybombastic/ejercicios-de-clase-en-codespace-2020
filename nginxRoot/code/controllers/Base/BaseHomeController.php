<?php

declare(strict_types=1);

require_once('BaseController.php');


abstract class BaseHomeController extends BaseController
{
  /**
   *  Obtenemos todos los productos para enseñar en el home
   */
  public function getAll()
  {
    $condition = null;
    if (isset($_POST['condition']) && $_POST['condition'] != 'All') {

      $condition = $_POST['condition'];
    }
    return [
      "base_list" => $this->getAllArticle(),
      "condition_list" => $this->getAllArticle($condition),
    ];
  }
}
