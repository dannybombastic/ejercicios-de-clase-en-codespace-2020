<?php

declare(strict_types=1);
require_once('models/usuario.php');
require_once('BaseController.php');

class BaseUserController extends BaseController
{

    
    /**
     * Recupera un usuario con toda la info
     */ 
    public function geCredential(string $email)
    {    
        $query = "SELECT * FROM user_profile WHERE user_email = ?";
        $stm = $this->pdo->prepare($query);
        $stm->execute([$email]);

        if ($stm) {
             return $stm->fetchAll();
        }

        return null;
    }


     /**
      * Funcion para registrar un usuario
      */
   public function registerUser(array $form)
   {
     $query = <<<EOD
        INSERT INTO `shoponline`.`user_profile`
         (`user_dni`,
         `user_name`,
         `user_addr`,
         `user_email`,
         `user_password`,
         `gender`,
         `user_role`,
         `create_at`,
         `is_active`)
         VALUES
         (?,?,?,?,?,?,?,?,?);
      EOD;
      $stm = $this->pdo->prepare($query);
      $stm->execute($form);
      if($stm){
          return true;
      }
      return false;
   }

 

}


 
