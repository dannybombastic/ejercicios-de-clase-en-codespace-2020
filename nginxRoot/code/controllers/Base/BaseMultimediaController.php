<?php

declare(strict_types=1);


require_once('BaseController.php');

abstract class BaseMultimediaController extends BaseController
{

     /**
      * Insertamos un recurso multimedia
      */
    protected function setMultimedia(array $values)
    {

        $query = "INSERT INTO multimedia (id_art, src, id_tipo , is_active) VALUES (?,?,?,?)";
        $stm = $this->insert($query, $values);
    }

    /**
     * Recuperamos un recurso multimedia
     */
    protected function getMultimedia(int $id)
    {

        $query = "SELECT * FROM shoponline.multimedia WHERE id = ?";
        $stm = $this->pdo->prepare($query);
        $stm->execute([$id]);
        $data = $stm->fetchAll();
        return $data;
    }

    /**
     * Actualizar un recurso multimedia
     */

    protected function updateMediatype(string $media_type, int $id)
    {
        $query = "UPDATE multimedia_type SET m_type = ? WHERE id = ? ";
        return $this->update($query, [$media_type, $id]);
        //return $this->pdo->prepare($query)->execute([$art_title, $art_desc, $art_price, $art_evalu, $art_sku, $id_cat, $create_at, $is_active, $id]);
    }

    /**
     * Inserta un tipo multimedia
     */
    protected function setMultimediaType(string $values)
    {

        $query = "INSERT INTO multimedia_type (m_type) VALUES (?)";
        $this->insert($query, [$values]);
    }

    /**
     * Actualiza un recurso multimedia
     */
    protected function updateMedia(array $values)
    {
        $query = "UPDATE multimedia SET id_art = ?, src = ?, id_tipo = ?, is_active = ? WHERE id = ? ";
        return $this->update($query, $values);
        //return $this->pdo->prepare($query)->execute([$art_title, $art_desc, $art_price, $art_evalu, $art_sku, $id_cat, $create_at, $is_active, $id]);
    }

    /**
     *  Obtiene un recurso de tipo multimedia
     */
    protected function getMultimediaType(int $id)
    {
        $query = "SELECT * FROM multimedia_type  WHERE id = ?";
        $stm = $this->pdo->prepare($query);
        $stm->execute([$id]);
        $data = $stm->fetchAll();
        return $data;
    }
    /**
     * Obtengo todos los aticulos para asociale una imagen o recurso multimedia 
     */
    protected function getAlltoPutArticle()
    {
        $query = <<<EOD
          SELECT 
              art_title,
              id,
              art_price,
              art_desc,
              art_evalu,
              create_at,
              is_active
          FROM
              articles; 
        EOD;
        $stm = $this->pdo->prepare($query);
        $stm->execute();
        $data = $stm->fetchAll();
        return $data;
    }
    /**
     * sobreescribo por si la moscas :) 
     */
    public function __destruct()
    {
        $this->pdo =  null;
    }

}

