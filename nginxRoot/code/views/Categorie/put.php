<div class="parallax row justify-content-lg-center p-5">
    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-6">
        <div class="alert alert-<?= $success ?> alert-dismissible fade show" role="alert">
            <strong>Congrats!</strong> You ategorie have been saved.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>


        <?php if (!empty(Conf::$QUERY)) : ?>
            <div class="text-center col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <h1>Udapte Categorie ID <?= Conf::$QUERY ?></h1>
            </div>
            <form action="/Categorie/updateForm/" method="POST">
                <div class="form-group">
                    <label for="title">Name</label>
                    <input type="text" class="form-control" name="name" id="name" placeholder="..." required>
                </div>

                <div class="p-2 col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="active" id="inlineRadio1" value="1">
                        <label class="form-check-label" for="inlineRadio1"> Active</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="active" id="inlineRadio2" value="0">
                        <label class="form-check-label" for="inlineRadio2"> No Active</label>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary btn-block">Submit</button>
            </form>
        <?php endif; ?>
        <?php if (empty(Conf::$QUERY)) : ?>
            <div class="text-center col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <h1>Create Categorie </h1>
            </div>
            <form action="/Categorie/putForm/<?= Conf::$QUERY ?>" method="POST">
                <div class="form-group">
                    <label for="title">Name</label>
                    <input type="text" class="form-control" value="<?= $cat['name'] ?>" name="name" id="name" placeholder="..." required>
                </div>

                <div class="p-2 col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-check form-check-inline">

                        <input class="form-check-input" type="radio" name="active" id="inlineRadio1" value="1" <?php if ($cat['is_active'] == 1) : ?> <?= 'checked' ?> <?php endif; ?>>
                        <label class="form-check-label" for="inlineRadio1"> Active</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="active" id="inlineRadio2" value="0" <?php if ($cat['is_active'] == 0) : ?> <?= 'checked' ?> <?php endif; ?>>
                        <label class="form-check-label" for="inlineRadio2"> No Active</label>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary btn-block">Submit</button>
            </form>
        <?php endif; ?>
    </div>
</div>