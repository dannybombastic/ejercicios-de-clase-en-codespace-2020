<?php
require_once('models/usuario.php');
// $user = unserialize(file_get_contents('user'));
// if ($user instanceof Usuario) {
//     echo $user->nombre . PHP_EOL;
//     echo $user->email;
// }

?>
<div class="container-fluid testimonial-group">

    <div class="row w-100" id="main-row">
        <span id="left">
            <ion-icon name="caret-back-circle-sharp"></ion-icon>
        </span>
        <span id="right">
            <ion-icon name="caret-forward-circle-sharp"></ion-icon>
        </span>


    </div>
</div>
<div class="container-fluid testimonial-group" id="main-pseudo">
    <div class="row w-100" id="main-bottom">
        <span id="left-bottom">
            <ion-icon name="caret-back-circle-sharp"></ion-icon>
        </span>
        <span id="right-bottom">
            <ion-icon name="caret-forward-circle-sharp"></ion-icon>
        </span>
    </div>
    <script>
        <?php include_once('./static/js/bind.js'); ?>
        getData(<?php echo json_encode($articles, JSON_HEX_TAG) ?>, <?php echo json_encode($condition, JSON_HEX_TAG) ?>);
    </script>
</div>