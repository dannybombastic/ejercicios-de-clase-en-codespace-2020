<?php
require_once('./controllers/HomeController.php');
$categories = new HomeController();
$categories = $categories->getCategorie();
?>


<div class="container-fluid">
    <div class="text-white justify-content-center text-center">
        <div class=" col-xs-12 col-sm-12 col-md-12 col-lg-12 pt-5">
            
            <div class="row">
            <div class="bg-warning border col-xs-12 col-sm-12 col-md-12 col-lg-12 pt-5 pb-5">
                <h3>Filters</h3>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-check form-check-inline">
                        <button class="btn btn-outline-light m-2 m-sm-0" onclick="order_by_date()">By date</button>
                        <button class="btn btn-outline-light m-2 m-sm-0" onclick="order_by_price()">By price</button>

                    </div>
                </div>

            </div>
            </div>
            
     

            <div class="row">
                <div class="bg-info border col-xs-12 col-sm-12 col-md-12 col-lg-12  pt-5 pb-5">
                    <label for="exampleFormControlSelect1">
                        <h3>Categories</h3>
                    </label>
                    <form action="/" class="form my-2 my-lg-0" method="POST">
                        <select class="form-control" name="condition" id="exampleFormControlSelect1">
                            <?php foreach ($categories as $cat) : ?>
                                <option><?= $cat['name'] ?></option>
                            <?php endforeach ?>
                            <option>All</option>

                        </select>
                        <button class="btn btn-outline-light m-2 pt-2" type="submit">Search</button>
                    </form>
                </div>
            </div>


        </div>



    </div>
</div>