<div class="parallax row justify-content-lg-center p-5">
    <div class="text-center col-sm-12 col-md-12 col-lg-12">
        <h1>Detail <?= $pro['title'] ?> ID <?= Conf::$QUERY ?></h1>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 p-5">
        <div class="card">
            <img class="" src="/img/<?=$pro['src'] ?>" alt="Card image cap" height="300" >
            <div class="card-body">
                <h5 class="card-title"><?= $pro['title'] ?></h5>
                <p class="card-text"><?= $pro['art_desc'] ?></p>
            </div>
            <div class="card-footer">
                <small class="text-muted"><?= $pro['create_at'] ?></small>
                Price: <small class="text-info"><?= $pro['price'] ?> €</small>
                Categorie <small class="text-warning"><?= $pro['name'] ?> €</small>
            </div>
        </div>
    </div>
</div>