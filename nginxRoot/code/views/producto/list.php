<div class="parallax row justify-content-lg-center p-5">
    <div class="text-center col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <h1>Detail of Product ID <?= Conf::$QUERY ?></h1>
    </div>
    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-6">
        <div class="card">
            <img class="img-fluid" src="https://img.freepik.com/vector-gratis/fondo-figuras-geometricas-3d_23-2148064982.jpg?size=338&ext=jpg" alt="Card image cap">
            <div class="card-body">
                <h5 class="card-title"><?= $pro['art_title'] ?></h5>
                <p class="card-text"><?= $pro['art_desc'] ?></p>
            </div>
            <div class="card-footer">
                <small class="text-muted"><?= $pro['create_at'] ?></small>
            </div>
        </div>
    </div>
</div>