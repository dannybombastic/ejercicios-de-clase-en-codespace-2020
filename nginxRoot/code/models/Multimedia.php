<?php

declare(strict_types=1);


class Multimedia
{
    public int $id;
    public int $id_art;
    public string $src;
    public int $id_tipo;
    public int $is_active;
}
