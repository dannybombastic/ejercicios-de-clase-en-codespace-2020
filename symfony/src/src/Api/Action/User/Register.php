<?php

declare(strict_types=1);

namespace App\Api\Action\User;

use App\Api\Action\RequestTransformer;
use App\Entity\User;
use App\Exception\User\UserAllreadyExistsException;
use App\Repository\UserRepository;
use Exception;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;

class Register
{
    private UserRepository $userRepository;
    private $jWTTokenManager;
    private $encoderFactory;
    public function __construct(UserRepository $userRepository, JWTTokenManagerInterface $jWTTokenManager, EncoderFactoryInterface $encoderFactory)
    {
        $this->userRepository = $userRepository;
        $this->jWTTokenManager = $jWTTokenManager;
        $this->encoderFactory = $encoderFactory;
    }


    /**
     * 
     * @Route("/users/register", methods={"POST"})
     * 
     * @throws Exception
     * 
     */
    public function __invoke(Request $request): JsonResponse
    {
        $name = RequestTransformer::getRquireField($request, 'name');
        $email = RequestTransformer::getRquireField($request, 'email');
        $password = RequestTransformer::getRquireField($request, 'password');
        $existingUser = $this->userRepository->findOneByEmail($email);

        if (null !== $existingUser) {
            throw UserAllreadyExistsException::fromUserEmail($email);
        }

        $user = new User($name, $email);

        $encoder = $this->encoderFactory->getEncoder($user);

        $user->setPassword($encoder->encodePassword($password, null));

        $this->userRepository->save($user);

        $jwt = $this->jWTTokenManager->create($user);

        return new JsonResponse(["token" => $jwt]);
    }
}
