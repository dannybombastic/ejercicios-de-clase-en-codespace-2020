<?php

declare(strict_types=1);

namespace App\Api\Listener;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

class JsonExecptionTransformerListener
{
    public function onKernelException(ExceptionEvent $event): void
    {
        $exception = $event->getThrowable();

        if ($exception instanceof HttpExceptionInterface) {
            $data = [

                'class' => get_class($exception),
                'code' => $exception->getStatusCode(),
                'message' => $exception->getMessage(),
            ];
            $response = $this->pepareResponse($data, $data['code']);
            $event->setResponse($response);
        }
    }
    private function pepareResponse(array $data, int $statusCode): JsonResponse
    {
        $response = new JsonResponse($data , $statusCode);
        $response->headers->set('Server-Time', time());
        $response->headers->set('X-Error-code' ,$statusCode);

        return $response;
    }
}
