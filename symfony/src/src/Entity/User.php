<?php

declare(strict_types=1);

namespace App\Entity;

use App\Security\Role;
use DateTime;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\ORM\Mapping as ORM;



/**
 * @ORM\Entity
 */
class User implements UserInterface
{

    /** 
     * @var \Ramsey\Uuid\Uuid
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true) 
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
     * */
    protected ?string $id;
    /** @ORM\Column(type="string") */
    protected string $name;
    /** @ORM\Column(type="string") */
    protected string $email;
    /** @ORM\Column(type="string") */
    protected string $password;
    /** @ORM\Column(type="array") */
    protected array $roles;
    /** @ORM\Column(type="datetime", name="create_at") */
    protected \DateTime $createdAt;
    /** @ORM\Column(type="datetime", name="update_at") */
    protected \DateTime $updateAt;


    /**
     * @throws \Exception
     * 
     */
    public function __construct(string $name, string $email)
    {
        $this->id = Uuid::uuid4()->toString();
        $this->name = $name;
        $this->email = $email;
        $this->roles[] = Role::ROLE_USER;
        $this->createdAt = new DateTime();
        $this->markAsUpdted();
    }

    /**
     * Get the value of email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set the value of email
     *
     * @return  self
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }

    /**
     * Get the value of id
     */
    public function getId()
    {
        return $this->id;
    }

 

    /**
     * Get the value of roles
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * Set the value of roles
     *
     * @return  self
     */
    public function setRoles($roles)
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * Get the value of createdAt
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Get the value of updateAt
     */
    public function getUpdateAt()
    {
        return $this->updateAt;
    }

    public function markAsUpdted(): void
    {
        $this->updateAt = new DateTime();
    }

    public function getSalt(): void
    {
    }


    public function getUsername()
    {
        return $this->email;
    }
    public function eraseCredentials(): void
    {
    }

    /**
     * Get the value of password
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set the value of password
     *
     * @return  self
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }
}
