<?php

declare(strict_types=1);

namespace App\Repository;

use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Persistence\ObjectRepository;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\QueryBuilder;

abstract class BaseRepository
{
    private ManagerRegistry $managerRegistry;
    protected ObjectRepository $objectRepository;
    private Connection $connection;
    public function __construct(ManagerRegistry $managerRegistry, Connection $connection)
    {
        $this->managerRegistry = $managerRegistry;
        $this->objectRepository = $this->getEntityManager()->getRepository($this->entityClass());
        $this->connection = $connection;
    }

    abstract protected static function entityClass(): string; 
  
    protected function saveEntity($entity):void 
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
    }
    protected function removentity($entity):void 
    {
        $this->getEntityManager()->remove($entity);
        $this->getEntityManager()->flush();
    }

    protected function createQueryBuilder(): QueryBuilder
    {
        return $this->getEntityManager()->createQueryBuilder();
    }
 
    /**
     * @throws DBALException
     */
    protected function executeFetchQuery(string $query, array $params = []): array
    {
        return $this->connection->executeQuery($query, $params)->fetchAll();
    }

    /**
     * @throws DBALException
     */
    protected function executeInsertQuery(string $query, array $params = []): void
    {
       $this->connection->executeQuery($query, $params)->fetchAll();
    }
    
    protected function Entity($entity):void 
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
    }


    private function getEntityManager(): ObjectManager
    {
        $entityMnager = $this->managerRegistry->getManager();
        if($entityMnager->isOpen()){

            return $entityMnager;
        }

        return $this->managerRegistry->resetManager();
    }
}